'use strict';

angular.module('IQApp.version', [
  'IQApp.version.interpolate-filter',
  'IQApp.version.version-directive'
])

.value('version', '0.1');
