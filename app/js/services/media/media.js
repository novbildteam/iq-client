angular.module('mediaService',['restApi.media','userAuthService']).
	factory('MediaService',['RestApiMedia','UserAuthService',
	function(RestApiMedia, UserAuthService)
	{
		var auth = UserAuthService;
		var api = RestApiMedia;
		var service = {
			/*
			�������� ����������� �� ������
			���������� URL �����������
			*/
			uploadImage: function(file, success, fail)
			{
				if(!file)
					return;
				api.uploadImage(auth.getUserToken(), file).$promise.then(
				function(data)
				{
					if(!data.error_code)
						success(data);
					else
						fail(data)
				},
				function(error)
				{
					fail(error);
				});
			},
			/*
			��������� ������ ����������� �� ���������� ������������
			*/
			getImagesList: function(success, fail)
			{
				//TODO: fix count, make as param
				api.getImagesList(auth.getUserToken(), 0, 100).$promise.then(
				function(data)
				{
					if(!data.error_code)
						success(data.results);
					else
						fail(data);
				},
				function(error)
				{
					fail(error);
				});
			},
		};
		return service;
	}]);