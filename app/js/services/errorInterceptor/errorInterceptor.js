'use strict'
angular.module('errorInterceptor', []).
	factory('ErrorInterceptor', 
		function()
		{
			var errorList = []; 
			/*
				type - ��� ������
					server - �� �������
					net - ������ ����
			*/
			var currentError;
			var maxLength = 20;
			
			var ERR_TYPE = 
			{
				SERVER: 'server',
				NET: 'net',
			}
			
			var errInterceptor =
			{
				currentError: currentError,
				errorList: errorList,
				pushError: function(error, type)
				{
					var errorObject = {type: type, error: error};
					currentError = errorObject;
					if(errorList.length+1 > maxLength)
						errorList.slice(1);
					errorList.push(errorObject);
					if(this.onError) this.onError(errorObject);
				},
				onError: function(errorObject)
				{
				},
				ERR_TYPE: ERR_TYPE,
			};
						
			errInterceptor.__defineGetter__('currentError', function()
			{
				return currentError;
			});
			errInterceptor.__defineGetter__('errorList', function()
			{
				return errorList;
			});
				
			return errInterceptor;
		}
	);