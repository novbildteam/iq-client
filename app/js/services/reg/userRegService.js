﻿/**
@namespace userRegService
@desc модуль реализующий сервис регистрации пользователя в системе
*/
angular.module('userRegService', ['restApi.accounts','userAuthService'])
	.factory('UserRegService', ['RestApiAccounts', 'UserAuthService',
	/**
	@class userRegService.UserRegService
	@desc сервис регистрации пользователя
	*/
	function(RestApiAccounts, UserAuthService)
	{
		var api = RestApiAccounts;
		return(
		{
			/** @name userRegService.UserRegService.username логин пользователя */
			username: null,
			/** @name userRegService.UserRegService.password пароль пользователя */
			password: null,
			/** @name userRegService.UserRegService.firstName имя пользователя */
			firstName: null,
			/** @name userRegService.UserRegService.secondName фамилия пользователя*/
			secondName: null,
			/**
			@function userRegService.UserRegService.register
			@desc регистрация пользователя
		 	@param object {Object} данные для регистрации
			@param success {Function} callback успешного выполнения регистрации
			@param fail {Function} callback неуспешной регистрации
			*/
			register: function(object, success, fail)
			{
				api.signUp(object.username, object.password).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						//do something
						success();
					}
					else
					{
						fail(data);
					}
				},
				function(error)
				{
					fail(error);
				}
				);
			},
			registerByPromo: function(object,success, fail)
			{
				api.signUpByPromo(object.username, object.password, object.firstName, object.secondName, object.promo).$promise.then(
				function(data)	
				{
					if(!data.error_code)
					{
						if(success) success();
					}
					else
					{
						if(fail) fail(data);
					}
				},
				function(error)
				{
					if (fail) fail(error)
				}
				);
			},
			resetPassword: function(username, success, fail)
			{
				api.resetPassword(username).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						if (success) success();
					}
					else
					{
						if(fail) fail(data);
					}
				},
				function(error)
				{
					if(fail) fail(error);
				});
			}

		}
		);
	}]);