angular.module('deviceService',[]).
	factory('DeviceService',[
		function()
		{
			var localStorage = window.localStorage;
			if(!localStorage['device.deviceId'])
				localStorage['device.deviceId'] = Number(new Date());
			var service =
			{
				deviceId: localStorage['device.deviceId'],
				tabId: Number(new Date()), //tab_id - идентификатор вкладки браузера (для корректной обработки событий)
			};
			return service;
		}
	]);