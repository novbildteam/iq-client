angular.module('tests.adminTest', ['restApi.tests', 'userAuthService', 'structures']).
	factory('AdminTest', ['RestApiTests', 'UserAuthService', 'LinkedList',
		function(RestApiTests, UserAuthService, LinkedList)
		{
			var apiTest = RestApiTests;
			var auth = UserAuthService;
			var list; //linked list �����
			var groups = null; //���-�������...�� group_id
			var questions = null; //���-�������...�� question_id
			var answers = null; //����������...�� answer_id
			var dataSaving = 
			{
				//������ ��������� ���������
				deleted:
				{
					groups: [],
					questions: [],
					answers: []
				},
				//���������/��������� �������� (��������� ����������  group)
				edited: []
			}
			/*
				Group, question � answer ������ �������������� ����:
				changed - �������
				deleted - ������
				created - ������
			*/
			var result =
			{
				//��. api/tests/getInfo
				test: 
				{
						test_id: null,
						owner_id: null,
						name: null,
						type: null,
						questions: null,
						timer: null,
						creation_time: null,
						editing_time: null
				},
				saved: true,
				initTest: function(data)
				{
					this.test.test_id = data.test_id;
					this.test.owner_id = data.owner_id;
					this.test.name = data.name;
					this.test.type = data.type;
					this.test.questions = data.questions;
					this.test.timer = data.timer;
					this.test.creation_time = data.creation_time;
					this.test.editing_time = data.editing_time;
					groups = [];
					questions = [];
					answers = [];
				},
				openTest: function(success, fail)
				{
					var self = this;
					if(!this.test.test_id)
						throw new Error('���� �� ���������');
					apiTest.getFullTest(auth.getUserToken(), this.test.test_id).$promise.then(
					function(data)
					{
						if(!data.error)
						{
							list = new LinkedList();
							var arr = [];
							for(i in data.groups) //���������� � ������������� �������
							{
								groups[data.groups[i].group_id] = data.groups[i];
								arr.push(data.groups[i]);
								var q = data.groups[i].questions
								for(j in q)
								{
									questions[q[j].question_id] = q[j];
									var a = q[j].answers;
									for(k in a)
										answers[a[k].answer_id] = a[k];
								}
							}
							//sort(arr)
							for(i=0; i<arr.length; i++)
								list.push(arr[i]); //���������� �������� ������ �����
							success();
						}
						else
							fail(data.error);
					},
					function(error)
					{
						fail(error);
					}
					);
				},
				createGroup: function(position)
				{
					var id;
					//��������� ���������� ��������������, ����� ���������� ����� ������� �� ��������
					do
					{
						id = 'tmp'+Math.round(Math.random()*1000);
					}while(groups[id])
					var group = 
					{
						group_id: id,
						position: position,
						questions: [],
						created: true
					};
					if(!position)
						list.push(group);
					else
						list.addTo(group, position);
					groups[id] = group;
					return group;
				},
				deleteGroup: function(id)
				{
					if(!groups[id])
						return null;
					list.remove(groups[id]);
					//���� id ���������, �� ������ �������
					if(groups[id].created)
						groups[id] = null;
					else //�����, ������� �� ������ list � �������� ������ �� ��������
					{
						groups[id].deleted = true;
						dataSaving.deleted.groups.push(groups[id]); //�������� � ������ ���������
					}
				},
				moveGroupTo: function(id, position)
				{
					if(!id || !position)
						return null;
					var item = list.remove(groups[id]);
					list.addTo(position, item);
					item.edited = true;
				},
				createQuestion: function(groupId)
				{
					if(!groups[groupId])
						return;
					var questionId;
					do
					{
						questionId = 'tmp'+Math.round(Math.random()*1000);
					}while(questions[questionId])
					var question = 
					{
						question_id: questionId,
						answers: [],
						created: true
					};
					questions[questionId] = question;
					groups[groupId].questions.push(question);
					return question;
				},
				deleteQuestion: function(id)
				{
					if(!questions[id])
						return;
					questions[id].deleted = true;
					if(!questions[id].created)
						dataSaving.deleted.questions.push(quesions[id]);
				},
				createAnswer: function(questionId)
				{
					if(!questions[questionId])
						return;
					var answerId;
					do
					{
						answerId = 'tmp'+Math.round(Math.random()*1000);
					}while(answers[answerId])
					var answer =
					{
						answer_id: answerId,
						text: null,
						correct: null,
						created: true
					};
					answers[answerId] = answer;
					questions[questionId].answers.push(answer);
					return answer;
				},
				deleteAnswer: function(id)
				{
					if(!answers[id])
						return;
					answers[id].deleted = true;
					if(!answers[id].created)
						dataSaving.deleted.answers.push(answers[id]);
				},
				save: function(success, fail)
				{
					if(!success)
						throw new Error('success callback not defined');
					if(!fail)
						throw new Error('fail callback not defined');
					//������������� position ����� � ����������� � �� �������� � list
					var group, newGroup;
					var question, newQuestion;
					var answer, newAnswer;
					var position = 0;
					while(group = list.next())
					{
						group.position = position;
						position++;
					}
					var deleted = dataSaving.deleted;
					var edited = dataSaving.edited;
					
					if(!edited || edited.length == 0)
						for(var i in groups)
						{
							group = groups[i];
							if(group.deleted)
								continue;
							var newGroup = 
							{
								group_id: group.group_id,
								questions: []
							};
							if(group.created || group.edited)
							{
								newGroup.position = group.position;
								if(group.created) newGroup.created = group.created;
								if(group.edited) newGroup.edited = group.edited;
							}
							edited.push(newGroup);
							for(var j in group.questions)
							{
								question = group.questions[j];
								if(question.deleted)
								{
									delete group.questions[j];
									continue;
								}
								newQuestion = 
								{
									question_id: question.question_id,
									answers: []
								};
								if(question.created || question.edited)
								{
									newQuestion.text = question.text;
									newQuestion.type = question.type;
									if(question.created) newQuestion.created = question.created;
									if(question.edited) newQuestion.edited = question.edited;
								}
									newGroup.questions.push(newQuestion);
									for(var k in question.answers)
									{
										answer = question.answers[k];
										if(answer.deleted)
										{
											delete question.answers[k];
											continue;
										}
										newAnswer = 
										{
											answer_id: answer.answer_id,
										};
										if(answer.created || answer.edited)
										{
											newAnswer.text = answer.text;
											newAnswer.correct = answer.correct;
											if(answer.created) newAnswer.created = answer.created;
											if(answer.edited) newAnswer.edited = answer.edited;
										}
										newQuestion.answers.push(newAnswer);
									}
							}
						}
						
					console.log(edited);
					console.log(deleted);
					this.saved = false;
					var self = this;
					apiTest.sendChanges(auth.getUserToken(), deleted, edited).$promise.then(
					function(data)
					{
						if(data.error)
						{
							fail(data.error);
							return;
						}
						self.saved = true;
						var group, question, answer;
						for(var i in data.groups)
						{
							group = data.groups[i];
							groups[group.oldId].group_id = group.newId;
							groups[group.newId] = groups[group.oldId];
							delete groups[group.newId].created;
							delete groups[group.newId].edited;
							delete groups[group.oldId];
						}
						for(var i in data.questions)
						{
							question = data.questions[i];
							questions[question.oldId].question_id = question.newId;
							questions[question.newId] = questions[question.oldId];
							delete questions[question.newId].created;
							delete questions[question.newId].edited;
							delete questions[question.oldId];
						}
						for(var i in data.answers)
						{
							answer = data.answers[i];
							answers[answer.oldId].answer_id = answer.newId;
							answers[answer.newId] = answers[answer.oldId];
							delete answers[answer.newId].created;
							delete answers[answer.newId].edited;
							delete answers[answer.oldId];
						}
						
						//������� ������ ���������� deleted (������, �������, ������)
						for(var i in group = deleted.groups)
							delete groups[group.group_id];
						for(var i in question = deleted.questions)
							delete questions[question.question_id];
						for(var i in answer = deleted.answers)
							delete answers[answer.answer_id];
						//�������� ������ ��������� � ���������� ��-��
						dataSaving.deleted.groups = [];
						dataSaving.deleted.questions = [];
						dataSaving.deleted.answers = [];
						dataSaving.edited = [];
						success();
					},
					function(error)
					{
						fail(error);
					}
					);
				},
				getList: function()
				{
					return list.toArray();
				},
				dataChanged: function()
				{
					this.saved = false;
					dataSaving.edited = [];
				},
			};
			return result;
		}]);