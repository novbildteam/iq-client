angular.module('tests.adminTest', ['restApi.tests', 'userAuthService', 'structures']).
	factory('AdminTest', ['RestApiTests', 'UserAuthService', 'LinkedList', 'TreeItem',
		function(RestApiTests, UserAuthService, LinkedList, TreeItem)
		{
			var apiTest = RestApiTests;
			var auth = UserAuthService;
			var root; //������ ��������� ��������-�������
			var groups = null; //���-�������...�� group_id
			var questions = null; //���-�������...�� question_id
			var answers = null; //����������...�� answer_id
			var dataSaving = //������, ������� �������� ���������� � ��������� ��������
			{
				//������ ��������� ���������
				deleted:
				{
					groups: [],
					questions: [],
					answers: []
				},
				//���������/��������� �������� (���-�������)
				edited:
				{
					groups: {},
					questions: {},
					answers: {}
				}
			}
			/*
				Group, question � answer ������ �������������� ����:
				edited - ������ ���������
					text - ������������ �������/������
					type - ��� �������/������
					childs - ������� ����
				created - ������
			*/
			
			var ERR_CODE = 
			{
				NO_QUESTIONS: 'no questions', 
				NO_ANSWERS: 'no answers',
				NO_ANSWER_TEXT: 'no answer text',
				NO_QUESTION_TEXT: 'no question text',
				NO_CORRECT_ANSWERS: 'no correct answers',
				NO_GROUPS: 'no groups',
				NOT_ENOUGH_ANSWERS: 'not enough answers',
				MORE_THAN_ONE_CORRECT: 'more than one correct',
				NOT_ENOUGH_CORRECT_ANSWERS: 'not enough correct answers',
			}
			
			var result =
			{
				//��. api/tests/getInfo
				test: 
				{
						test_id: null,
						owner_id: null,
						name: null,
						type: null,
						questions: null,
						timer: null,
						random: null,
						creation_time: null,
						editing_time: null,
						edited: null //������ ���������
				},
				list: null,
				saved: true,
				initTest: function(data, success, fail)
				{
					var self = this;
					if(!data) //������������� "������� �����"
					{
						groups = {};
						questions = {};
						answers = {};
						if(success) success();
							return;
					}
					else
					if(typeof(data) == 'string' || typeof(data) == 'number') //���������� test_id
					{
						apiTest.getInfo(auth.getUserToken(), data).$promise.then(
						function(response)
						{
							self.test.test_id = data;
							init(response);
							if(success) success();
						},
						function()
						{
							if(fail) fail();
						}
						);
					}
					else
					if(typeof(data) == 'object') //�������� ��������� ��� ������������� �����
					{
						init(data);
					}
					function init(data)
					{
						
						console.log(self.test.test_id);
						self.test.owner_id = data.owner_id;
						self.test.name = data.name;
						self.test.type = data.type;
						self.test.questions = data.questions;
						self.test.timer = data.timer;
						self.test.random = data.random;
						self.test.creation_time = data.creation_time;
						self.test.editing_time = data.editing_time;
						self.test.edited = {};
						self.saved = true;
						self.list = null;
						groups = {};
						questions = {};
						answers = {};
						console.log(self.test);
					}
					
				},
				openTest: function(success, fail)
				{
					var self = this;
					var questionItem, groupItem, answerItem;
					if(!this.test.test_id)
					{
						if (fail) fail({msg: 'no test_id'});
						return;
					}
					apiTest.getFullTest(auth.getUserToken(), this.test.test_id).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{					
							var group, question, answer;
							root = new TreeItem({groups: []});
							self.list = root.item;
							for(i in data.groups) //���������� � ������������� �������
							{
								group = root.addChild(data.groups[i]);
								groups[data.groups[i].group_id] = group;
								var q = data.groups[i].questions;
								for(j in q)
								{
									question = group.addChild(q[j]);
									questions[q[j].question_id] = question;
									var a = q[j].answers;
									for(k in a)
									{
										answer = question.addChild(a[k]);
										answers[a[k].answer_id] = answer;
									}
									question.item.answers = question.childsToArray();
								}
								group.item.questions = group.childsToArray();
							}
							root.item.groups = root.childsToArray();
							if(success) success();
						}
						else
							if(fail) fail(data);
					},
					function(error)
					{
						if(fail) fail(error);
					}
					);
				},
				createGroup: function()
				{
					
					var id;
					//��������� ���������� ��������������, ����� ���������� ����� ������� �� ��������
					do
					{
						id = 'tmp'+Math.round(Math.random()*1000);
					}while(groups[id])
					var group = 
					{
						group_id: id,
						position: root.item.groups.length + 1, //��������� � 1
						questions: [],
						created: true
					};
					groups[id] = root.addChild(group);
					root.item.groups = root.childsToArray();
					this.saved = false;
					this.createQuestion(id);
					return group;
				},
				copyGroup: function(groupId)
				{
					var id;
					if(!groups[groupId])
						return;
					var originalGroup = groups[groupId].item;
					do
					{
						id = 'tmp'+Math.round(Math.random()*1000);
					}while(groups[id])
					var group = 
					{
						group_id: id,
						position: root.item.groups.length + 1, //��������� � 1
						questions: [],
						created: true
					};
					groups[id] = root.addChild(group);
					root.item.groups = root.childsToArray();
					for(var i in originalGroup.questions)
					{
						this.copyQuestion(id, originalGroup.questions[i].question_id)
					}
					this.saved = false;
					return group;
				},
				deleteGroup: function(id)
				{
					if(!groups[id])
						return null;
					if(!groups[id].item.created)
						dataSaving.deleted.groups.push(id);
					var delPos = groups[id].item.position;
					groups[id].remove();
					root.childs.flush();
					while(next = root.childs.next())
						if(next.item && next.item.item.position > delPos)
							next.item.item.position--;
					root.item.groups = root.childsToArray();
					this.saved = false;
				},
				moveGroupTo: function(id, position)
				{
				//����� �������������� (��������!)
					if(!id || position == null)
						return null;
					if(!groups[id])
						return null;
					var finded = 0, i = 1;				
					var next;
					var arr = []; //������ ���������, ������� ������� ���� ��������
					root.childs.flush();
					while(finded < 2)
					{
						next = root.childs.next();
						if(!next)
							break;
						if(next.item == groups[id])
						{
							if(i == position)
								return;
							finded++;
							next.item.item.position = position;
							if(!next.item.item.edited)
								next.item.item.edited = {};
							next.item.item.edited.position = true;
							continue;
						}
						if(i == position)
						{
							finded++;
							if(finded > 1)
								continue;
						}
						if(finded > 0)
						{
							if(next.item && !next.item.item.edited)
								next.item.item.edited = {};
							next.item.item.edited.position = true;
							arr.push(next.item.item);
						}
						i++;
					}
					for(var j in arr)
					{
						console.log(arr[j].group_id);
						if(position < i)
							arr[j].position++;
						else
							arr[j].position--;
						console.log(arr[j].position);
					}
					var item = root.childs.remove(groups[id]);
					root.childs.addTo(position-1, item);
					root.item.groups = root.childsToArray();
					this.saved = false;
					console.log('arr');
					console.log(arr);
				},
				groupChanged: function(id, type)
				{ //���� ����� �� ����� �������
					if(!groups[id].item.edited)
						groups[id].item.edited = {};
					if(type == 'position')
						groups[id].item.edited.position = true;
					this.saved = false;
				},
				createQuestion: function(groupId)
				{
					if(!groups[groupId])
						return;
					var questionId;
					do
					{
						questionId = 'tmp'+Math.round(Math.random()*1000);
					}while(questions[questionId])
					var question = 
					{
						type: 0,
						question_id: questionId,
						answers: [],
						created: true,
						text: '',
					};
					questions[questionId] = groups[groupId].addChild(question);
					groups[groupId].item.questions = groups[groupId].childsToArray();
					this.saved = false;
					this.createAnswer(questionId);
					this.createAnswer(questionId);
					return question;
				},
				copyQuestion: function(groupId, originalQuestionId)
				{
					if(!groups[groupId])
						return;
					if(!questions[originalQuestionId])
						return;
					var originalQuestion = questions[originalQuestionId].item;
					var questionId;
					do
					{
						questionId = 'tmp'+Math.round(Math.random()*1000);
					}while(questions[questionId])
					var question = 
					{
						type: originalQuestion.type,
						question_id: questionId,
						answers: [],
						created: true,
						text: originalQuestion.text,
					};
					questions[questionId] = groups[groupId].addChild(question);
					groups[groupId].item.questions = groups[groupId].childsToArray();
					for(var i in originalQuestion.answers)
					{
						var answer = this.createAnswer(questionId);
						answer.text = originalQuestion.answers[i].text;
						if(originalQuestion.answers[i].correct)
							answer.correct = originalQuestion.answers[i].correct;
					}
					this.saved = false;
					return question;
				},
				deleteQuestion: function(id)
				{
					if(!questions[id])
						return null;
					if(!questions[id].item.created)
						dataSaving.deleted.questions.push(id);
					var group = questions[id].parent;
					questions[id].remove();
					group.item.questions = group.childsToArray();
					this.saved = false;
				},
				questionChanged: function(id, type)
				{
					if(!questions[id].item.edited)
						questions[id].item.edited = {};
					if(type == 'type')
					{
						questions[id].item.edited.type = true;
						//если вопрос закрытого типа, то добавить доп. поля для пустых ответов
						if(questions[id].item.type == 0 || questions[id].item.type == 1)
						{
							if(questions[id].item.answers.length < 2)
								this.createAnswer(questions[id].item.question_id);
						}
						//если вопрос открытого типа, то удалить лишние пустые поля ответов
						//только для теста
						if(questions[id].item.type == 2 && this.test.type == 0)
						{
							var ans = questions[id].childs.next(); //начиная со второго
							while (questions[id].item.answers.length > 1)
							{
								ans = questions[id].childs.next();
								if(!ans)
									break;
								if (!ans.item.item.text)
									this.deleteAnswer(ans.item.item.answer_id);
							}
						}
					}
					if(type == 'text')
						questions[id].item.edited.text = true;
					this.saved = false;
				},
				createAnswer: function(questionId)
				{
					if(!questions[questionId])
						return;
					var answerId;
					do
					{
						answerId = 'tmp'+Math.round(Math.random()*1000);
					}while(answers[answerId])
					var answer =
					{
						answer_id: answerId,
						text: '',
						correct: false,
						created: true
					};
					answers[answerId] = questions[questionId].addChild(answer);
					questions[questionId].item.answers = questions[questionId].childsToArray();
					this.saved = false;
					return answer;
				},
				deleteAnswer: function(id)
				{
					if(!answers[id])
						return null;
					if(!answers[id].item.created)
						dataSaving.deleted.answers.push(id);
					var question = answers[id].parent;
					answers[id].remove();
					question.item.answers = question.childsToArray();
					this.saved = false;
				},
				answerChanged: function(id, type)
				{
					if(!answers[id].item.edited)
						answers[id].item.edited = {};
					if(type == 'text')
						answers[id].item.edited.text = true;
					if(type == 'correct')
						answers[id].item.edited.correct = true;
					this.saved = false;
				},
				testInfoChanged: function(type)
				{
					this.saved = false;
					this.test.edited[type] = true;
				},
				save: function(success, fail)
				{
					if(!success)
						throw new Error('success callback not defined');
					if(!fail)
						throw new Error('fail callback not defined');
					if(this.saved)
						return;
					
					/*
						�������� ������ � �����
					*/
					var errorObject = checkError(this.list);
					if(errorObject)
					{
						fail(errorObject);
						return;
					}
					
					dataSaving.edited.groups = {};
					dataSaving.edited.questions = {};
					dataSaving.edited.answers = {};			
					
					var group, question, answer;
					var groupItem, questionItem, answerItem;
					var groupArray = [];
					//���������� ������ ���������
					while(group = root.childs.next())
					{
						console.log(group);
						if(!group.item.item)
							continue;
						groupItem = group.item.item;
						var questionArray = [];
						while(question = group.item.childs.next())
						{
							questionItem = question.item.item;
							var answerArray = [];
							var correctAnswers = 0;
							while(answer = question.item.childs.next())
							{
								answerItem = answer.item.item;
								if(answerItem.correct) correctAnswers++;
								if(answerItem.edited || answerItem.created)
								{
									var newAnswer = dataSaving.edited.answers[answerItem.answer_id];
									if(!newAnswer) {newAnswer = {}; answerArray.push(newAnswer); dataSaving.edited.answers[answerItem.answer_id] = newAnswer;}
									newAnswer.answer_id = answerItem.answer_id;
									if(answerItem.created)
									{
										newAnswer.created = true;
										newAnswer.text = answerItem.text;
										newAnswer.correct = answerItem.correct;
									}
									if(answerItem.edited)
									{
										if(answerItem.edited.text) newAnswer.text = answerItem.text;
										if(answerItem.edited.correct) newAnswer.correct = answerItem.correct;
									}									
								}
							}
							/*if(correctAnswers > 1 && questionItem.type == 0)
							{
								if(!questionItem.edited)
									questionItem.edited = {};
								questionItem.edited.type = true;
								questionItem.type = 1;
							}
							else
							if(correctAnswers == 1 && questionItem.type == 1)
							{
								if(!questionItem.edited)
									questionItem.edited = {};
								questionItem.edited.type = true;
								questionItem.type = 0
							}*/
							if(questionItem.edited || questionItem.created || answerArray.length != 0)
							{
								var newQuestion = dataSaving.edited.questions[questionItem.question_id];
								if(!newQuestion) {newQuestion = {}; questionArray.push(newQuestion); dataSaving.edited.questions[questionItem.question_id] = newQuestion;}
								newQuestion.question_id = questionItem.question_id;
								if(questionItem.created)
								{
									newQuestion.created = true;
									newQuestion.text = questionItem.text;
									newQuestion.type = questionItem.type;
								}
								if(questionItem.edited)
								{
									if(questionItem.edited.text) newQuestion.text = questionItem.text;
									if(questionItem.edited.type) newQuestion.type = questionItem.type;
								}
								if(answerArray.length != 0) newQuestion.answers = answerArray;
							}
						}
						if(groupItem.edited || groupItem.created || questionArray.length != 0)
						{
							var newGroup = dataSaving.edited.groups[groupItem.group_id];
							if(!newGroup) {newGroup = {}; groupArray.push(newGroup); dataSaving.edited.groups[groupItem.group_id] = newGroup;}
							newGroup.group_id = groupItem.group_id;
							if(groupItem.created)
							{
								newGroup.created = true;
								newGroup.position = groupItem.position;
							}
							if(groupItem.edited)
								if(groupItem.edited.position) newGroup.position = groupItem.position;
							if(questionArray.length != 0) newGroup.questions = questionArray;
						}
					}
					//������ � ����������� ��� �����
					var testInfo = null;
					for(var i in this.test.edited)
					{
						if(!testInfo)
							testInfo = {};
						if(i == 'timer')
							testInfo[i] = Number(this.test[i]);
						else
							testInfo[i] = this.test[i];
						
					}
					//������ �� �������� �������
					var edited = {};
					if(testInfo != {}) edited.testInfo = testInfo;
					if(groups.length != 0) edited.groups = groupArray;
					console.log('deleted: ');
					console.log(dataSaving.deleted.groups);
					console.log(dataSaving.deleted.questions);
					console.log(dataSaving.deleted.answers);
					console.log('edited: ');
					console.log(dataSaving.edited.groups);
					console.log(dataSaving.edited.questions);
					console.log(dataSaving.edited.answers);
					console.log('result: ');
					console.log(edited);
					var self = this;
					//���������, � ������ �������� ��������� � ���������� id
					console.log(testInfo);
					if(
						dataSaving.deleted.groups.length == 0 &&
 						dataSaving.deleted.questions.length == 0 &&
						dataSaving.deleted.answers.length == 0 &&
						!testInfo && groupArray.length == 0)
					{
						this.saved = true;
						success();
						return;
					}
					apiTest.sendChanges(auth.getUserToken(), self.test.test_id, dataSaving.deleted, edited).$promise.then(
					function(data)
					{
						console.log('send');
						if(data.error_code)
						{
							fail(data);
							return;
						}
						if(self.test.test_id != data.test_id)
							self.test.test_id = data.test_id;
						//������� ������ ���������� deleted (������, �������, ������)
						for(var i in group = dataSaving.deleted.groups)
							delete groups[group[i].group_id];
						for(var i in question = dataSaving.deleted.questions)
							delete questions[question[i].question_id];
						for(var i in answer = dataSaving.deleted.answers)
							delete answers[answer[i].answer_id];
						//������� ������� �� ���������� (������, �������, ������)
						for(var i in group = dataSaving.edited.groups)
							delete groups[group[i].group_id].item.edited;
						for(var i in question = dataSaving.edited.questions)
							delete questions[question[i].question_id].item.edited;
						for(var i in answer = dataSaving.edited.answers)
							delete answers[answer[i].answer_id].item.edited;
						//�������� ������ ��������� � ���������� ��-��
						dataSaving.deleted.groups = [];
						dataSaving.deleted.questions = [];
						dataSaving.deleted.answers = [];
						self.test.edited = {};
						self.saved = true;
						if(data.test_id) self.test.test_id = data.test_id;
						var group, question, answer;
						for(var i in data.groups)
						{
							group = data.groups[i];
							if(group.oldId == group.newId)
								continue;
							console.log(group);
							groups[group.oldId].item.group_id = group.newId;
							groups[group.newId] = groups[group.oldId];
							delete groups[group.newId].item.created;
							delete groups[group.newId].item.edited;
							delete groups[group.oldId];
						}
						for(var i in data.questions)
						{
							question = data.questions[i];
							if(question.oldId == question.newId)
								continue;
							questions[question.oldId].item.question_id = question.newId;
							questions[question.newId] = questions[question.oldId];
							delete questions[question.newId].item.created;
							delete questions[question.newId].item.edited;
							delete questions[question.oldId];
						}
						for(var i in data.answers)
						{
							answer = data.answers[i];
							if(answer.oldId == answer.newId)
								continue;
							answers[answer.oldId].item.answer_id = answer.newId;
							answers[answer.newId] = answers[answer.oldId];
							delete answers[answer.newId].item.created;
							delete answers[answer.newId].item.edited;
							delete answers[answer.oldId];
						}
						
						success();
					},
					function(error)
					{
						fail(error);
					}
					);
				},
				checkError: function()
				{
					return checkError(this.list);
				},
				ERR_CODE: ERR_CODE,
			};
			
			function checkError(list)
			{
					var questions, answers;
					if(list.groups.length == 0)
						return({error_code: ERR_CODE.NO_GROUPS});
					for(var i in list.groups)
					{
						questions = list.groups[i].questions;
						if(questions.length == 0)
							return({error_code: ERR_CODE.NO_QUESTIONS,
								data:
								{
									group: list.groups[i],
								}
							});
						for(var j in questions)
						{
							if(!questions[j].text || questions[j].text == '')
								return({error_code: ERR_CODE.NO_QUESTION_TEXT,
									data:
									{
										group: list.groups[i],
										question: questions[j]
									}
								});
							answers = questions[j].answers;
							if(answers.length == 0 && (result.test.type == 0 || result.test.type == 1 && questions[j].type != 2))
								return({error_code: ERR_CODE.NO_ANSWERS,
									data:
									{
										group: list.groups[i],
										question: questions[j]
									}
								});
							else
							if(answers.length < 2 && questions[j].type != 2)
									return({error_code: ERR_CODE.NOT_ENOUGH_ANSWERS,
										data:
										{
											group: list.groups[i],
											question: questions[j]
										}
									});
							var correctAnswers = 0
							if(!(result.test.type == 1 && questions[j].type == 2))
								for(var k in answers)
								{
									if(!answers[k].text || answers[k].text == '')
										return({error_code: ERR_CODE.NO_ANSWER_TEXT,
											data:
											{
												group: list.groups[i],
												question: questions[j],
												answer: answers[k],
											}
										});
									if(answers[k].correct)
										correctAnswers++;
								}
							if(result.test.type == 0)
								if(correctAnswers > 1 && questions[j].type == 0)
									return({error_code: ERR_CODE.MORE_THAN_ONE_CORRECT,
										data:
										{
											group: list.groups[i],
											question: questions[j]
										}
									});
								else
								if(correctAnswers == 1 && questions[j].type == 1)
									return({error_code: ERR_CODE.NOT_ENOUGH_CORRECT_ANSWERS,
										data:
										{
											group: list.groups[i],
											question: questions[j]
										}
									});
								else
								if(correctAnswers == 0 && (questions[j].type == 0 || questions[j].type == 1))
									return({error_code: ERR_CODE.NO_CORRECT_ANSWERS,
										data:
										{
											group: list.groups[i].group,
											question: questions[j].question,
										}
									});
						}
						
					}
					return null;
			}
			
			return result;
		}]);