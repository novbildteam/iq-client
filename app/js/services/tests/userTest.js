/**
@namespace "tests.userTest"
@desc Модуль реализующий сервис прохождения студентом (пользователем) теста
*/
angular.module('tests.userTest',['restApi.tests','userAuthService']).
	factory('UserTest',['RestApiTests','UserAuthService', 'RestApiRoom',
	/**
	@class "tests.userTest".UserTest
	@desc Сервис реализующий прохождение студентом теста
	*/
	function(RestApiTests, UserAuthService, RestApiRoom)
	{

		var self = this;
		
		var api = RestApiTests;
		var apiRoom = RestApiRoom;
		var auth = UserAuthService;
		var localStorage = window.localStorage;
		var LS = 
		{
			sessionId : "userTest.sessionId",
			current : "userTest.current",
			count : "userTest.count",
			state : "userTest.state",
			timer : "userTest.timer",
			answerTimer : "userTest.answerTimer",
			timeLimit : "userTest.timeLimit",
			question : "userTest.question",
			name : "userTest.name",
			type : "userTest.type",
		};
		var intervalId; //ссылка на таймер отсчитывающий время пройденное с начала тестирования
		var intervalAId; //таймер отсчитывающий время потраченное на вопрос
		
		var state = 
		{
			_sessionId: null,
			_current: null,
			_count: null, //количество вопросов в тесте
			_state: null, /*	состояние тестирования:
							ready - ожидание пользовательских действий
							active - тестирование уже идет
							finished - тестирование окончено
							cancelled - отменен преподавателем
							dropped - пользователь прервал тестирование*/
			_timer: null,
			_timeLimit: null,
			_answerTimer: null,
			_name: null,
			_type: null,
			_paceMode: null, /*режим прохождения теста/опроса
							1 - audience-paced
							2 - presenter-paced
							*/
			_showResults: null, //показывать ли результат ответа (для pace_mode == 1)
		}
		
		
		//функция регистрация геттера и сеттера для name
		function regGetterSetters(realName, name)
		{
			if(name == 'current' || name == 'count' || name == 'timer' || name == 'answerTimer')
				state.__defineGetter__(name, function()
				{
					return state[realName] == null  ? null : parseInt(state[realName]);
				});
			else
				state.__defineGetter__(name, function()
				{
					return state[realName];
				});
			state.__defineSetter__(name, function(val)
			{
				state[realName] = val;
				if(name == 'answerTimer' || name == 'sessionId')
					localStorage[LS[name]] = val;
			});
		}
		
		//Регистрация геттеров и сеттеров
		for(var i in state)
		{
			if(i.charAt(0) == '_')
			{
				var name = i.substr(1,i.length-1);
				regGetterSetters(i, name);
			}
		}
		
		/*Этот объект чисто описательный, на самом деле он
			подменяется новым при полученни очередного вопроса
			*/
		var question =
		{
			question_id: null, //
			text: null, //формулировка вопроса
			type: null, /*тип вопроса:
							0 - закрытый с одним вариантом
							1 - закрытый с несколькими вариантами
							2 - открытый */
			answers:
			[
				{
					//в зависимости от типа вопроса, используется либо answer_id, либо text
					//answer_id: null, //id ответа
					//text: null //ответ
				}
			]
		}		
		
		var questions = [];
				
		var resetState = function()
		{
			for(i in LS)
				localStorage.removeItem(LS[i])
		}
		var restoreState = function()
		{
			state._sessionId = localStorage[LS.sessionId];
			/*state._current = localStorage[LS.current] == 'null' ? null : parseInt(localStorage[LS.current]);
			state._count = localStorage[LS.count] == 'null' ? null : parseInt(localStorage[LS.count]);
			state._state = localStorage[LS.state];
			state._timer = localStorage[LS.timer] == 'null' ? null : parseInt(localStorage[LS.timer]); //защита от ручного удаления из LS*/
			state._answerTimer = localStorage[LS.answerTimer] == 'null' ? null : parseInt(localStorage[LS.answerTimer]);
			/*state._timeLimit = localStorage[LS.timeLimit] == 'null' ? null : parseInt(localStorage[LS.timeLimit]);
			state._name = localStorage[LS.name];
			state._type = localStorage[LS.type] == 'null' ? null : parseInt(localStorage[LS.type]);*/
			//question = JSON.parse(localStorage[LS.question]);
		}
	
		var getAll = function(sessionId,success,fail)
		{
			apiRoom.getAll(auth.getUserToken(), sessionId).$promise.then
			(
				function(data)
				{
					questions = data.questions;
					success();
				},
				function(error)
				{
					fail(error);
				}
			);
		}
		
		var getNext = function(sesId, success, fail)
		{
			apiRoom.getNext(auth.getUserToken(), sesId).$promise.then(success, fail);
		}
		
		function killTimer()
		{
			if(intervalId)
			{
				clearInterval(intervalId);
				clearInterval(intervalAId);
				intervalId = null;
			}
		}
		
		var service =
		{
			/**
			@name "tests.userTest".UserTest.state 
			@desc Объект состояния теста
				@prop {Number} sessionId идентификатор сессии тестирования
				@prop {Number} current номер текущего вопроса
				@prop {Number} count количество вопросов в тесте
				@prop {String} state состояние теста:
					ready - тест получен, можно начинать
					active - тест в процессе прохождения
					finished - тест окончен
				@prop {Number} timer время прошедшее с начала тестирования
				@prop {Number} timeLimit ограничение по времени прохождения теста (если нет, то null)
				@prop {Object} question текущий вопрос
				@prop {String} name название теста
				@prop {String} type тип теста:
					0 - тест, 1 - опрос
			*/
			state: state,
			/**
			@name "tests.userTest".UserTest.answers
			@desc Массив ответов
			*/
			answers : [], //массив ответов пользовтаеля на тест
			/**
			@function "tests.userTest".UserTest.nextQuestion
			@desc Переход к следующему вопросу. Производится отправка текущих ответов массива {@link answers}.
			В случае успешной отправки ответа, производится переход к следующему вопросу и вызывается callback success.
			Иначе - callback fail. 
			Если вопросы закончились, то в аргумент success передается объект с сообщением об окончании (msg)
			@param {Function} success callback успешного перехода к следующему вопросу. Аргументов нет,
			кроме случая, когда тест завершен (объект с полем msg: 'finished')
			@param {Function} fail callback при возникновении ошибки
			*/
			submitAnswer: function(success, fail)
			{
				var answerTimer = state.answerTimer;
				var self = this;
				//объект с запросом
				var obj = {
					questionId: self.question.question_id,
					answers: self.answers,
					answerTime: answerTimer,
				};
				clearInterval(intervalAId);
				clearInterval(intervalId);
				apiRoom.sendAnswer(auth.getUserToken(), state.sessionId, obj).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						if(data.finished)
						{
							state.state = 'finish';
							killTimer();
							//if(success) success({msg: 'finish'});
						}
						else
						{
							//сохранение следующего вопроса в LS (в случае, если произойдет перезагрузка)
							localStorage[LS.current]++;
							//localStorage[LS.question] = JSON.stringify(questions[localStorage[LS.current]]);
							//сброс в LS answerTimer
							localStorage[LS.answerTimer] = 0;
						}
						if(success) success(data);
					}
					else
					{
						if(fail) fail(data);
					}
				},
				function(error)
				{
					if(fail) fail(error)
				}
				);
			},
			nextQuestion: function(success, fail)
			{
				var self = this;
				this.answers = [];
				if(state.state == 'finished' || state.state == 'cancel')
				{
					if(fail) fail('test finished!');
					return false;
				}
				getNext(state.sessionId,
				function(data)
				{
					if(!data.error_code)
					{
						self.question = data;
						//if(!state.current  || state.current < data.question_index) //0 или null, то первый вопрос
						if(state.current != data.question_index)
						{
							state.current = data.question_index;
							state.answerTimer = 0;
							//intervalAId = setInterval(function(){state.answerTimer++}, 100);
						}
						else
							state.timer += state.answerTimer;//10;
						self.startTest();
						success();
					}
					else
					if(fail) fail(data);
				},
				function(error)
				{
					if(fail) fail(error);
				});				
			},
			/**
			@func "tests.userTest".UserTest.init
			@desc Инициализация теста
			@param {Number} sesId идентификатор сессии
			@param {Function} success callback при успешной инициализации
			@param {Function} fail callback при ошибке
			*/
			
			//ПЕРЕПИСАТЬ!
			init: function(data, type)
			{	
				if(data.session_id == null)
				{
					if(fail) fail('no session id!')
					return;
				}
				var self = this;
				this.answers = [];
				var session = localStorage.getItem(LS.sessionId);
				state.name = data.name;
				state.type = data.type;
				state.timeLimit = data.timer;
				state.count = data.questions;
				state.paceMode = data.pace_mode;
				state.showResults = data.show_results;
				if(data.student_state && data.student_state.question_index)
					state.current = data.student_state.question_index;
				if(type != 'new')
				{
					state.state = data.test_state;
					if(data.student_state.time)
						state.timer = data.student_state.time;
				}
				else
				if(type == 'new')
				{
					state.state = 'active';
					state.current = 0;
					state.timer = 0;
				}
				if(state.state == 'active')
				{
					if(!session || session != data.session_id)
					{
						state.sessionId = data.session_id;
						state.answerTimer = 0;
						state.state = 'ready';
					}
					else
					{
						state.sessionId = data.session_id;
						restoreState();
					}
				}
			},
			
			/* init: function(sesId, success, fail)
			{	
				if(sesId == null)
				{
					if(fail) fail('no session id!')
					return;
				}
				var self = this;
				this.answers = [];
				var session = localStorage.getItem(LS.sessionId);
				if(state.state == 'active')
				{
					if(!session || session != sesId)
					{
						state.sessionId = sesId;
						state.answerTimer = 0;
						state.state = 'ready';
						if(success) success();
					}
					else
					getNext(sesId,
						function(data)
						{
							if(!data.error_code)
							{
								if(!session || session != sesId)
								{
									state.sessionId = sesId;
									state.answerTimer = 0;
									state.state = 'ready';
									self.question = data;
								}
								else
								{
									state.sessionId = sesId;
									restoreState();
									state.timer += state.answerTimer;//10;
									self.question = data;
									self.startTest();
								}
								if(success) success();
							}
							else
							if(fail) fail(data);
						},
						function(error)
						{
							if(fail) fail(error);
						});
				}
				else
				if(success) success();
			}, */
			/**
			@function "tests.userTest".UserTest.closeTest
			@desc Метод закрытия теста, в случае когда он еще не завершен
			(но, например, закрылась вкладка или был осуществлен переход на другую страницу)
			*/
			closeTest: function()
			{
				killTimer();
			},
			/**
			@function "tests.userTest".UserTest.quitTest
			@desc Выход из теста
			*/
			quitTest: function()
			{
				//выход из теста
				resetState();
				killTimer();
				
			},
			/**
			@function "tests.userTest".UserTest.startTest
			@desc Запуск/продолжение тестирования
			*/
			startTest: function()
			{
				var self = this;
				state.state = 'active';
				/*
				заглушка, требуется исправление на сервере
				*/
				/*if(state.timeLimit)
					if(state.timer >= state.timeLimit)
					{
						killTimer();
						state.state = 'finish';
						return;
					}*/
				if(intervalId)
					clearInterval(intervalId);
				intervalId = setInterval(inc,10);
				
				function inc()
				{
					state.answerTimer++;
					state.timer++;
					if(state.timer % 10 == 0)
						self.event.timerTick();
					if(state.timeLimit)
						if(state.timer >= state.timeLimit*100)
						{
							killTimer();
							//state.state = 'finish';
							self.event.timeIsUp();
						}
				}
				/*if(intervalAId)
					clearInterval(intervalAId);
				intervalAId = setInterval(function(){state.answerTimer++}, 100);*/
			},
			/**
			@function "tests.userTest".UserTest.waitClosure
			@desc Callback собятия закрытия теста
			*/
			event:
			{
				waitClosure: function()
				{
				},
				/**
				@function "tests.userTest".UserTest.timeIsUp
				@desc Callback события окончания времени отведенного под тест
				*/
				timeIsUp: function()
				{
				},
				/**
				@function "tests.userTest".UserTest.timerTick
				@desc Callback события счета таймера
				*/
				timerTick: function()
				{
				}
			},
		};
		
		service.__defineGetter__('question', function()
		{
			return question;
		});
		service.__defineSetter__('question', function(val)
		{
			question = val;
			console.log('this');
			//localStorage[LS.question] = JSON.stringify(val);
		});
		service.__defineGetter__('questions', function()
		{
			return questions;
		});
				
		return(service);
	}
	]);
