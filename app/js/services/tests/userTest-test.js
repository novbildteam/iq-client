describe('UserTest module', function () {
    var objectToTest;
    var $httpBackend;
	var Settings;

    beforeEach(function () {
	module('ngResource');
	module('settings');       
	//module('restApi.tests');
	module('tests.userTest');
	module('settings');
    inject(function ($injector) {
            objectToTest = $injector.get('UserTest');
			$httpBackend = $injector.get('$httpBackend');
			Settings = $injector.get('Settings');
            $rootScope = $injector.get('$rootScope');
        });
    });

	it('should exists', function () {
        expect(objectToTest).toBeDefined();
    });
	
	describe('testing initialization method', function()
	{
	
		afterEach(function () {
			$httpBackend.flush();
			$httpBackend.verifyNoOutstandingExpectation();
			$httpBackend.verifyNoOutstandingRequest();
			objectToTest.quitTest();
		});

		
		it('should recieve data', function(){
			var exampleData = [{test: 'test'}];
			$httpBackend.expectGET(Settings.baseURL+'/fakedata/tests/getAll.json').respond(exampleData);
			$httpBackend.expectGET(Settings.baseURL+'/fakedata/tests/waitClosure.json').respond('');
			objectToTest.init('1',1,
			function()
			{
				expect(objectToTest.state.count).toBe(1);
				expect(objectToTest.state.current).toBe(0);
				expect(objectToTest.question).toBeDefined();
				console.log(objectToTest.question().test);
			},
			function()
			{
				
			});
		})
	
	});
});