﻿angular.module('httpInterceptor', ['ng', 'errorInterceptor', 'deviceService']).
	factory('HttpInterceptor', ['ErrorInterceptor', '$q', 'DeviceService',
		function(ErrorInterceptor, $q, DeviceService) {
			return (
			{
				'request': function(config) {
					if(config.params && DeviceService.tabId)
						config.params.tab_id = DeviceService.tabId;
					return config;
				},
				'response': function(response)
				{
					if(response.data)
					{
						if (response.data.error_code)
						{
							ErrorInterceptor.pushError(response.data, ErrorInterceptor.ERR_TYPE.SERVER);
							/*if(response.data.error_code == 6)
							 console.log('ошибка авторизации! Время жизни токена истекло');
							 else
							 if(response.data.error_code == 8)
							 console.log('ошибка авторизации! Токен недейстивтелен');
							 else
							 return response;
							 $injector.get('Profile').reset();
							 $injector.get('UserAuthService').resetAuth();*/
						}
					}
					return response;
				},
				responseError: function(rejection)
				{
					console.log(rejection);
				//	if(rejection.status == -1 || rejection.status == 0 || rejection.status == 404 || rejection.status == 403)
						ErrorInterceptor.pushError({error: rejection}, ErrorInterceptor.ERR_TYPE.NET);				
					return $q.reject(rejection);
				}
			});
		}]);
