angular.module('lessonsReports',['restApi.reports','restApi.tests','userAuthService']).
factory('LessonsReports',['RestApiReports', 'RestApiTests', 'UserAuthService',
    function(RestApiReports, RestApiTests, UserAuthService)
    {
        var apiReports = RestApiReports;
        var apiTests = RestApiTests;
        var auth = UserAuthService;

        var service =
        {
            //последние уроки
            lastLessons: [],
            countOfLessons: null,
            //загрузка следующей порции последних уроков
            getLastLessons: function(limit,success,fail)
            {
                var self = this;
                apiReports.getLessons(auth.getUserToken(), null, {offset: self.lastLessons.length, limit: limit, sort:'-lesson_id'}).$promise.then(
                    function(data)
                    {
                        if(!data.error_code)
                        {
                            self.countOfLessons = data.count;
                            self.lastLessons = self.lastLessons.concat(data.results);
                            if(success) success(data);
                        }
                        else
                            if(fail) fail(data);
                     },
                    function(error)
                    {
                        if(fail) fail(error);
                    }
                  );
            },
			getLessons: function(ids, params, success, fail)
			{
				apiReports.getLessons(auth.getUserToken(), ids, params).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
						    //если доступ к ресурсу (например, lessons/1/), оборачиваем в список
						    var response = [data];

                            // если выборка, то ответ лежит в results
						    if(ids.length > 1)
						        response = data.results;

                            if (success)
                                success(response);
						}
						else
							if(fail) fail(data);
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
            getLessonsStreams: function(ids, params, success, fail)
            {
                apiReports.getLessonsStreams(auth.getUserToken(),ids,params).$promise.then(
                    function(data)
                    {
                        if(!data.error_code)
                        {
                            if(success)
                                success(data.results);
                        }
                        else
                            if(fail) fail(data);
                    },
                    function(error)
                    {
                        if(fail) fail(error);
                    }
                );
            },
			getLessonsUsers: function(ids, params, success, fail)
			{
				apiReports.getLessonsUsers(auth.getUserToken(), ids, params).$promise.then(
                    function(data)
                    {
                        if(!data.error_code)
                        {
                            if(success) success(data.results);
                        }
                        else
                            if(fail) fail(data);
                    },
                    function(error)
                    {
                        if(fail) fail(error);
                    }
                );
			},
            clear: function()
            {
                this.lastLessons = [];
                this.countOfLessons = null;
            }
        };
        return service;
    }]);
