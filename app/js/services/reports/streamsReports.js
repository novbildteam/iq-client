angular.module('streamsReports',['restApi.reports','restApi.tests','userAuthService']).
	factory('StreamsReports',['RestApiReports', 'RestApiTests', 'UserAuthService',
		function(RestApiReports, RestApiTests, UserAuthService)
		{
			var apiReports = RestApiReports;
			var apiTests = RestApiTests;
			var auth = UserAuthService;

			var list = []; //������ ������� ���������� � �������.
			var cart = {}; /*���-������� ������� ����������� ��� ��������� (������� �������). ����: stream_id
				��������� �������� ������� �������
				{
					stream: - ������ ������
					{
						stream_id
						test 
						� �.�.
					},
					users: - ������ � ������������ ������ �� ������������� (��. reports/getStreamResults)
				}
				���� ������� ���������� � true, ������ �� ��� �� ��������
			*/
			var current = null; /*������� ��������������� ������ ������
				��������� ��. ����
			*/
			//����� ���������� ������� (�������)
			var numOfReports = null;
			
			//�������� ���������� � ������� ������� � local storage
			var localStorage = window.localStorage;
			var LS = 
			{
				cart: "streamReports.cart",
			};
			//����� local storage
			var resetState = function()
			{
				for(i in LS)
					localStorage.removeItem(LS[i]);
			}
			//�������������� ������ �� local storage
			var restoreState = function()
			{
				var arr;
				try
				{
					var arr = JSON.parse(localStorage[LS.cart]);
				}
				catch(e)
				{
					localStorage.removeItem(LS.cart);
				}
				if(arr)
					for(var i in arr)
						cart[arr[i]] = true;
				console.log(cart);
			}
			
			function saveCart()
			{
				var arr = [];
				for(var i in cart)
					if(i != 'length')
						arr.push(i);
				localStorage[LS.cart] = JSON.stringify(arr);
			}
			
			restoreState();
			
			var service =
			{
				getLastStreams: function(offset, limit, filter, success, fail)
				{
					apiReports.getStreams(auth.getUserToken(), null, offset, limit, 'add_time desc', filter).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							numOfReports = data.count;
							list = data.results;
							if(success)	success(data.results);
						}
						else
						if(fail) fail(data)
					},
					function(error)
					{
						if(fail) fail(error);
					});
				},
				/*
				���������� � ������� �������.
				� �������� stream ����� ��������� ���� ������ ������ (��. ����),
				���� stream_id (����� ��� ������ �� ���� ����� ���������� �����)
				*/
				addToCart: function(stream)
				{
					if(typeof(stream) != 'object')
						cart[stream] = true;
					else
						cart[stream.stream_id] = stream;
					saveCart();
				},
				/*
				�������� �������� �� �������. ���������� ������ stream_id
				*/
				removeFromCart: function(streamId)
				{
					delete cart[streamId];
					saveCart();
				},
				clearCart: function()
				{
					cart = {};
					localStorage.removeItem(LS.cart);
				},
				//������� �������� ������� �� �������
				loadCart: function(success, fail)
				{
					var self = this;
					var streams = [];
					// for(var i in cart)
					// 	if(cart[i].stream && cart[i].users)
					// 		continue;
					// 	else
					// 	if(cart[i].stream && !cart[i].users)
					// 		streams.push({stream_id: Number(i), short: true});
					// 	else
					// 		streams.push({stream_id: Number(i), short: false});
					for(var i in cart)
						streams.push(i.stream_id);
					apiReports.getStreamsResults(auth.getUserToken(), streams).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							for(var i in data.results)
							{
								var streamId = data.results[i].stream_id;
								if(data.results[i].stream)
								{
									var stream = data.results[i].stream;
									stream.stream_id = streamId;
									cart[streamId] = {stream: stream};
								}
								cart[streamId].users = data.results[i].users;
							}
							if(success) success();
						}
						else
						{
							if(data.error_code == 7)
								self.clearCart();
							if(fail) fail(data);
						}
					},
					function(error)
					{
						if(fail) fail(error);
					});
				},
				//загрзка текущего стрима (ОДНОГО)
				loadCurrent: function(success, fail)
				{
					self = this;
					if(!current)
						throw('No current stream');
					else
					if(!current.test)
						apiReports.getStreams(auth.getUserToken(), current.stream_id).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								current = data;
								self.loadStreamResult(current.stream_id,
								function(data)
								{
									if(!data.error_code)
									{
										current.users = data.users;
										if(success) success();
									}
									else
									if(data.error_code == 7)
										self.clearCart();
									else
										if(fail) fail(error);

								},
								function(error)
								{
									if(fail) fail(error);
								});
							}
						},
						function(error)
						{
							if(fail) fail(error);
						});
				},
				//метод загрузки результатов ОДНОГО стрима

				loadStreamResult: function(stream_id, success, fail)
				{
					//var self = this;
					apiReports.getStreamsResults(auth.getUserToken(), stream_id).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								if(success) success(data);
							}
							else
							{
								if(fail) fail(data);
							}
						},
						function(error)
						{
							if(fail) fail(error);
						});
				},
				// downloadStream: function(streamId, format, success, fail)
				// {
				// 		apiReports.downloadStream(auth.getUserToken(), streamId, format).$promise.then(
				// 			function(data)
				// 			{
				// 				if(success)
				// 					success(data);
				// 			},
				// 			function(error)
				// 			{
				// 				if(fail)
				// 					fail(error);
				// 			}
				// 		)
				// },
				downloadStream: function(streamId, format, success, fail)
				{
					apiReports.downloadStream(auth.getUserToken(), streamId, format).then(
						function(data)
						{
							if(success)
								success(data.data);
						},
						function(error)
						{
							if(fail)
								fail(error);
						});
				},
				getDownloadLink: function(streamId, type)
				{
					return apiReports.streamDownloadLink(auth.getUserToken(), streamId, type);
				},
				getFullTest: function(stream, success, fail)
				{
					apiTests.getFullTest(auth.getUserToken(), stream.test.test_id).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								if(success) success(data);
							}
							else
							if(fail) fail(data);
						},
						function(error)
						{
							if(fail) fail(error);
						}
					);
				},

			};
			
			service.__defineGetter__('list', function()
			{
				return list;
			});
			service.__defineGetter__('cart', function()
			{
				return cart;
			});
			service.__defineGetter__('cartLength',function()
			{
				var count = 0;
				for(var i in cart)
					count++;
				return count;
			});
			service.__defineGetter__('numOfReports', function()
			{
				return numOfReports;
			});
			service.__defineGetter__('current', function()
			{
				return current;
			});
			service.__defineSetter__('current',function(val)
			{
				current = val;
			});
			return service;
		}]);