angular.module('longPoller', []).
	factory('LongPoller', ['$rootScope', function($rootScope)
	{
		/* Для поддержки cross-tab communication необходимо задать имя лонг полера
		Это необходимо для того, чтобы обойти лимит запросов браузера к одному домену
		по ключу longpoll-{name}-data помещаются данные longpoller-а с именем name
		по ключу longpoll-{name} - уникальный идентификатор лонгполлера, который получил данные
		(т.е. ему обрабатывать событие обновление localstorage не надо)
		*/
		return(
			function(name)
			{
				return new poller(name);
			});
			
		function poller(name)
		{
			this.count = 0;
			this.limit = null;
			this.stopped = true;
			this.func = null;
			if(name)
			{
				this.salt = Number(new Date()); //генерируется уникальный идентификатор лонгполлера
				if (Modernizr.localstorage)
					localStorageAvaliable = true;
			}
			var self = this;
			var localStorageAvaliable;
			this.poll = function(func, success, fail)
			{
				this.count = 0;
				this.stopped = false;
				this.func = func;
				req(success, fail);
				//проверка поддержки localStorage браузером
				 if (!Modernizr.localstorage)
				 	return;
				localStorageAvaliable = true;
				/*если задано имя лонгполлера, то повесить слушателей событий на localstorage*/
				if(name)
				{
					if (window.addEventListener)
						window.addEventListener("storage", handleStorage, false);
					else
						window.attachEvent("onstorage", handleStorage);
					/*механизм проверки активности ведущего лонгполлера:
					если через 60 сек. не отвечает (salt не обновился текущем временем),
					то удалить связанные с localstorage поля
					 */
					var pollingInterval = setInterval(
						function()
						{
							if(window.localStorage['longpoller-'+name])
							{
								var time = 0;
								if(window.localStorage['longpoller-' + name+'-time'])
									time = Number(window.localStorage['longpoller-' + name+'-time']);
								var thistime = new Date();
								if (thistime - time > 1500)
								{
									window.localStorage.removeItem('longpoller-' + name);
									req(success, fail);
								}
							}
						}
					,1000);
				}
				function handleStorage(event)
				{
					if(!event) {event = window.event;}
					if(event.key == 'longpoller-'+name+'-data')
					{
						//завершить выполнение при совпадении идентификаторов
						if(self.salt == Number(window.localStorage['longpoller-'+name]))
							return;
						var data = JSON.parse(window.localStorage['longpoller-'+name+'-data']);
						if(!data.error_code)
						{
							success(data);
							$rootScope.$apply();
						}
						else
						{
							//в случае ошибки polling завершается
							fail(data);
						}
					}
				}
			};
			this.stop = function()
			{
				this.stopped = true;
			};
			
			var self = this;
			function req(success, fail)
			{
				if(self.limit)
					if(self.count+1 > self.limit)
						self.stopped = true;
				if(self.stopped)
					return;
				self.count++;
				if(localStorageAvaliable && name)
				{
					if (window.localStorage['longpoller-' + name] && Number(window.localStorage['longpoller-' + name]) != self.salt) //уже есть ведующий лонгполлер, ждем
						return;
					//обновление для ведущего лонгполлера (чтобы понимать что он живой)
					window.localStorage['longpoller-' + name] = self.salt;//ведущего нет, указываем на текущий лонгполлер
					window.localStorage['longpoller-' + name+'-time'] = Number(new Date());
					if(!self.saltInterval)
						self.saltInterval = setInterval(
							function()
							{
								window.localStorage['longpoller-' + name+'-time'] = Number(new Date()); //обновление таймера longpollera
							}, 500);
				}
				self.func().$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							if(localStorageAvaliable && name)
							{
								data.__salt = window.localStorage['longpoller-'+name+'-time'];
								window.localStorage['longpoller-'+name+'-data'] = JSON.stringify(data);
							}
							success(data);
							req(success, fail);
						}
						else
						{
							//в случае ошибки polling завершается
							//stopped = true;
							clearInterval(self.saltInterval);
							fail(data);
						}
					},
					function(data)
					{
						fail(data);
						req(success, fail);
					});
			}
		};
	}]);