angular.module('structures', []).
	factory('LinkedList',[function()
	{
		function LinkedList()
		{
			this.head = new Item(null);
			this.tail = new Item(null);
			this.head.next = this.tail;
			this.tail.prev = this.head;
			this.iterator = this.head;
		};

		LinkedList.prototype.push = function(item) //�������� � �����
		{
			var element = new Item(item);
			element.next = this.tail;
			element.prev = this.tail.prev;
			this.tail.prev.next = element;
			this.tail.prev = element;
		};
		LinkedList.prototype.removeFrom = function(num) //������� ������� �� ������ � �������� num (���������� item)
		{
			var iterator = this.head;
			var count = 0;
			while(iterator.next.next)
			{
				iterator = iterator.next;
				if(count == num)
				{
					iterator.prev.next = iterator.next;
					iterator.next.prev = iterator.prev;
					var result = iterator.item;
					delete iterator.item;
					iterator = null;
					return result;
				}
				count++;
			}
		};
		LinkedList.prototype.remove = function(item)
		{
			var iterator = this.head;
			while(iterator.next.next)
			{
				iterator = iterator.next;
				if(iterator.item == item) //��� ������������ is equal?
				{
					iterator.prev.next = iterator.next;
					iterator.next.prev = iterator.prev;
					var result = iterator.item;
					delete iterator.item;
					iterator = null;
					return result;
				}
			}
		};
		LinkedList.prototype.removeAll = function()
		{
			var iterator = this.head;
			while(iterator.next.next)
			{
				iterator = iterator.next;
				delete iterator.item;
				iterator.prev.next = null;
				iterator.prev = null;
			}
			this.head.next = this.tail;
			this.tail.prev = this.head;
			this.iterator = this.head;
		};
		LinkedList.prototype.addTo = function(num, item) //�������� ������� � ������ � ������� num
		{
			var iterator = this.head;
			var count = 0;
			while(iterator.next)
			{
				iterator = iterator.next;
				if(count == num)
				{
					var element = new Item(item);
				
					element.next = iterator;
					element.prev = iterator.prev;
					iterator.prev.next = element;
					iterator.prev = element;
					
					break;
				}
				count++;
			}
		};
		LinkedList.prototype.toArray = function() //������������� � ������
		{
			var iterator = this.head;
			var array = [];
			while(iterator.next.next)
			{
				iterator = iterator.next;
				array.push(iterator.item);
			}
			return array;
		};
		LinkedList.prototype.getAt = function(num) //���������� ������� � ������ ��� ������� num
		{
			var iterator = this.head;
			var count = 0;
			while(iterator.next.next)
			{
				iterator = iterator.next;
				if(count == num)
				{
					return iterator.item;
				}
				count++;
			}
		};
		LinkedList.prototype.next = function()
		{
			if(this.iterator.next.next)
			{
				this.iterator = this.iterator.next;
				return this.iterator;
			}
			else
				this.iterator = this.head;
			return null;
		};
		LinkedList.prototype.prev = function()
		{
			if(this.iterator.prev.prev)
			{
				this.iterator = this.iterator.prev;
				return this.iterator;
			}
			else
				this.iterator = this.tail;
			return null;
		};
		LinkedList.prototype.flush = function()
		{
			this.iterator = this.head;
		};
		function Item(item)
		{
			this.prev = null;
			this.next = null;
			this.item = item;
		};
		
		return (LinkedList);
}]).
	factory('TreeItem',['LinkedList', function(LinkedList)
	{
		function TreeItem(item, parent)
		{
			this.item = item;
			this.childs = new LinkedList();
			this.parent = parent;
		}

		TreeItem.prototype.addChild = function(item)
		{
			var child = new TreeItem(item, this);
			this.childs.push(child);
			return child;
		}

		TreeItem.prototype.childsToArray = function()
		{
			var arr = [];
			var child;
			this.childs.flush();
			while(child = this.childs.next())
			{
				if(child.item)
					arr.push(child.item.item);
			}
			return arr;
		}

		TreeItem.prototype.remove = function()
		{
			this.parent.childs.remove(this);
			this.item = null;
			var child;
			while(child = this.childs.next())
			{
				child.item.remove(); //�������� �����������
			}
			this.childs.removeAll(); //�������� ��������� �������� ������
			return this;
		}
		return(TreeItem);
	}]);