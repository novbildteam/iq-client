'use strict'
angular.module('profileService', ['restApi.accounts', 'userAuthService'])
	.factory('Profile', ['RestApiAccounts', 'UserAuthService',
		function(RestApiAccounts, UserAuthService)
		{
			var auth = UserAuthService;
			var api = RestApiAccounts;
			var profile = 
			{
				_username: null,
				_firstName: null,
				_secondName: null,
				_rootId: null,
				_avaImg: null
			}
			var changes = {}; //������ ���������
			for(var i in profile)
			{
				var name = i.substr(1,i.length-1);
				regGetterSetter(i, name)
			}
			
			function regGetterSetter(realName, name)
			{
				profile.__defineGetter__(name, function()
				{
					return profile[realName];
				});
				/*profile.__defineSetter__(name, function(val)
				{
					return profile[realName] = val;
				})*/
			}
			
			profile.__defineGetter__('temporary', function()
			{
				return auth.isTemporary();
			});
			
			function set(obj)
			{
				profile._username = auth.getUsername();
				for(var i in obj)
					if(obj[i] != null)
						profile['_'+i] = obj[i];
			}
			
			return(
			{
				profile: profile,
				//�������� �������
				set: function(obj)
				{
					set(obj);
				},
				load: function(success, fail)
				{
					auth.getProfile(
					function(profileData)
					{
						console.log('loading')
						var obj = {
							firstName: profileData.first_name,
							secondName: profileData.second_name,
							avaImg: profileData.ava_img,
							rootId: profileData.root_id
						};
						console.log('loaded');
						set(obj);
						if(success) success();
					},
					function(error)
					{
						if(fail) fail(error);
					});
				},
				//����� �������
				reset: function()
				{
					console.log('reset');
					this.profile._username = null;
					this.profile._firstName = null;
					this.profile._secondName = null;
					this.profile._rootId = null;
					this.profile._avaImg = null;
				},
				saveProfileInfo: function(obj, success, fail) //���������� ��������� ����������
				{
					var self = this;
					api.saveProfileInfo(auth.getUserToken(), obj).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							for(var i in obj)
								if(obj[i] != null)
									self.profile['_'+i] = obj[i];
							self.profile._avaImg = data.ava_img;		
							if(success) success();
						}
						else
							if(fail) fail(data);
					},
					function(error)
					{
						if(fail) fail(error);
					});
				},
				changePassword: function(currentPass, newPass, success, fail)
				{
					var self = this;
					api.changePassword(auth.getUserToken(), currentPass, newPass).$promise.then(
					function(data)
					{
						if(!data.error_code)
							success();
						else
							fail(data);
					},
					function(error)
					{
						fail(error);
					});
				}
			}
			);
		}]);