angular.module('fsExplorer', ['restApi.tests', 'userAuthService']).
	factory('FsExplorer', ['RestApiFs', 'UserAuthService',
		function(RestApiFs, UserAuthService)
		{
			var fsApi = RestApiFs;
			var auth = UserAuthService;
			function Explorer()
			{
				this.folder =
				{
					list: [], //список файлов в текущей директории
					path: [] //{folder_id: , name: }
				}
			}
			Explorer.prototype.open = function(itemId, success, fail)
			{
				var self = this;
				var needPath = true;
				for(var i in self.folder.path)
				{
					if(self.folder.path[i].folder_id == itemId)
					{
						self.folder.path = self.folder.path.slice(0, i);
						needPath = false;
						break;
					}
				}
				fsApi.open(auth.getUserToken(), itemId, needPath).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						if(needPath)
							self.folder.path = data.path;
						else
							self.folder.path.push({folder_id: data.path[0].folder_id, name:data.path[0].name});
						self.folder.list = data.items == ''? [] : data.items;
						if(success) success();
					}
					else
						if(fail) fail(data);
				},
				function(error)
				{
					if(fail) fail(error);
				});
			}
			return (Explorer);
		}]);