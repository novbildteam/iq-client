angular.module('fsManager', ['restApi.fs', 'restApi.tests', 'userAuthService', 'fsExplorer']).
	factory('FsManager', ['RestApiFs', 'RestApiTests', 'UserAuthService', 'FsExplorer',
		function(RestApiFs, RestApiTests, UserAuthService, FsExplorer)
		{
			var fsApi = RestApiFs;
			var testsApi = RestApiTests;
			var auth = UserAuthService;
			var explorer = new FsExplorer();
			explorer.makeDir = function(name, folderId, success, fail)
			{
				var self = this;
				fsApi.makeDir(auth.getUserToken(), name, folderId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var newItem =
						{
							item_id: data.item_id,
							creation_time: data.creation_time,
							editing_time: data.creation_time,
							name: name,
							folder_id: data.folder_id,
							item_type: 0
						};
						self.folder.list.push(newItem);
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				});
			}
			explorer.addTest = function(folderId, testInfo, success, fail)
			{
				var self = this;
				testsApi.create(auth.getUserToken(), folderId, testInfo).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var newItem =
						{
							item_id: data.item_id,
							item_type: 2,
							name: testInfo.name,
							test_type: testInfo.test_type,
							timer: testInfo.timer,
							random: testInfo.random,
							test_id: data.test_id,
							creation_time: data.creation_time,
							editing_time: data.creation_time,
							folder_id: folderId,
						};
						if(data.branch_id)
							newItem.branch_id = data.branch_id;
						self.folder.list.push(newItem);
						if(success)
							success(data);
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail) fail(error);
				}
				);
			}
			explorer.delete = function(itemId, success, fail)
			{
				var self = this;
				fsApi.delete(auth.getUserToken(), itemId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						/*if(data.path != self.folder.path)
							return;*/
						deleteItem(itemId);
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				}
				);
			}
			explorer.move = function(itemId, folderId, success, fail)
			{
				var self = this;
				fsApi.move(auth.getUserToken(), itemId, folderId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						// if(data.path != self.folder.path)
						// 	return;
						deleteItem(itemId);
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				}
				);
			}
			explorer.rename = function(itemId, name, success, fail)
			{
				if(!name || name == '')
					return;
				var self = this;
				fsApi.rename(auth.getUserToken(), itemId, name).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var list = self.folder.list;
						for(var i in list)
						{
							if(list[i].item_id == itemId)
								list[i].name = name;
						}
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				})
			}
			explorer.copy = function(itemId, success, fail)
			{
				var self = this;
				fsApi.copy(auth.getUserToken(), itemId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var item;
						var list = explorer.tests.list;
						for(var i in list)
							if((item = list[i]).item_id == itemId)
								break;
						var newItem = 
						{
							item_id: data.item_id,
							name: data.name,
							item_type: item.item_type,
							owner_id: item.owner_id,
							creation_type: item.creation_type,
							editing_type: item.editing_type
						};
						if(item.item_type == 1)
						{
							newItem.test_type = item.test_type;
							newItem.questions = item.questions;
							newItem.timer = item.timer;
							newItem.random = item.random;
							if(item.share_type)
								newItem.share_type = item.share_type;
						}
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				});
			}
			explorer.rename = function(itemId, name, success, fail)
			{
				if(!name || name == '')
					return;
				var self = this;
				fsApi.rename(auth.getUserToken(), itemId, name).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var list = explorer.folder.list;
						for(var i in list)
						{
							if(list[i].item_id == itemId)
								list[i].name = name;
						}
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				}
				)
			}
			explorer.copy = function(itemId, success, fail)
			{
				var self = this;
				fsApi.copy(auth.getUserToken(), itemId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var item;
						var list = explorer.tests.list;
						for(var i in list)
							if((item = list[i]).item_id == itemId)
								break;
						var newItem = 
						{
							item_id: data.item_id,
							name: data.name,
							item_type: item.item_type,
							owner_id: item.owner_id,
							creation_type: item.creation_type,
							editing_type: item.editing_type
						};
						if(item.item_type == 1)
						{
							newItem.test_type = item.test_type;
							newItem.questions = item.questions;
							newItem.timer = item.timer;
							newItem.random = item.random;
							if(item.share_type)
								newItem.share_type = item.share_type;
						}
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				});
			}
			explorer.shareFolder = function(folderId, userId, type, success, fail)
			{
				fsApi.shareFolder(auth.getUserToken(), folderId, userId, type).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var list = explorer.folder.list;
						for(var i in list)
							if(list[i] == folderId)
								list[i].share_type = type;
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				});
			}
			explorer.shareTest = function(folderId, userId, type)
			{
				fsApi.shareTest(auth.getUserToken(), folderId, userId, type).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						var list = explorer.folder.list;
						for(var i in list)
							if(list[i] == folderId)
								list[i].share_type = type;
						if(success)
							success();
					}
					else
						if(fail)
							fail(data);
				},
				function(error)
				{
					if(fail)
						fail(error);
				});
			}
			
			return explorer;
			
			function deleteItem(itemId)
			{
				var item = null;
				var list = explorer.folder.list;
				var count = 0;
				for(var i in list)
				{
					if(list[i].item_id == itemId)
					{
						item = list[i];
						break;
					}
					count++;
				}
				if(item)
					explorer.folder.list = list.slice(0,count).concat(list.slice(count+1));
				console.log(explorer.folder.list)
			}
			
		}]);