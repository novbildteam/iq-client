'use strict'
/**
@namespace userAuthService
@desc Модуль реализует сервис для авторизации пользователя
*/
angular.module('userAuthService', ['restApi.accounts','restApi.room'])
	.factory('UserAuthService', ['RestApiAccounts', 'RestApiRoom', 
	/**
			@class userAuthService.UserAuthService
			@description Сервис авторизации пользователя
			@returns {Object}
		*/
	function(RestApiAccounts, RestApiRoom)
	{
		
		var apiAcc = RestApiAccounts;
		var apiRooms = RestApiRoom;
		
		var localStorage = window.localStorage;
		
		var LS = 
		{
			token: 'auth.token',
			userId: 'auth.userId',
			username: 'auth.username'
		};
		
		var auth =
		{
			token: null, //загрузка из localstorage
			userId: null,
			username: null,
		};
		
		if(localStorage[LS.token])
		{
			auth.token = localStorage[LS.token] == 'null' ? null : localStorage[LS.token];
			auth.userId = localStorage[LS.userId] == 'null' ? null : localStorage[LS.userId];
			auth.username = localStorage[LS.username] == 'null' ? null : localStorage[LS.username];
		}
		
		function saveAuth()
		{
			localStorage[LS.token] = auth.token;
			localStorage[LS.username] = auth.username;
			localStorage[LS.userId] = auth.userId;
		}
		
		var self = this;
		
		
		
		return (
		{
			/**
			@function userAuthService.UserAuthService.resetAuth
			@desc Сброс авторизации
			*/
			auth:
			{
				username: auth.username
			},
			resetAuth: function()
			{
				localStorage[LS.token] = null;
				localStorage[LS.username] = null;
				localStorage[LS.userId] = null;
				auth.token = null;
				auth.userId = null;
				auth.username = null;
				this.auth.username = null;
			},
			/**
				@function userAuthService.UserAuthService.logInSystem
				@desc Авторизация в системе
				@param {Object} obj - Объект авторизации.
					@param {Number} obj.type - тип пользователя: 1 - админ; 2 - пользователь (студент)
					@param {String} obj.username - логин
					@param {String} obj.password - пароль
			 		@param {String} obj.token - токен доступа
			 		@param {String} obj.user_id - id пользователя
				@param {Function} fail - callback при неудачной авторизации,
					error содержит код ошибки авторизации - code
			*/
		
			logInSystem: function(obj, success, fail)
			{
				var self = this;
				/*
				Структура obj:
				username - логин
				password - пароль
				*/
				console.log(obj);
				if(!obj.type)
					obj.type = 2; //по умолчанию - пользователь (студент)
				if(!obj.token)
					apiAcc.signIn(obj.type, obj.username, obj.password).$promise.
					then(
						function(data)
						{
							if(!data.error_code)
							{
								console.log(data);
								auth.token = data.token;
								auth.userId = data.user_id;
								//auth.username = obj.username;
								auth.username = data.user_id; //ВРЕМЕННЫЙ ХАК
								//self.auth.username = obj.username;
								self.auth.username = data.user_id;
								//загрузка в localstorage
								localStorage[LS.token] = data.token;
								localStorage[LS.userId] = data.user_id;
								//localStorage[LS.username] = obj.username;
								localStorage[LS.username] = data.user_id;
								success(data.token);
							}
							else
							{
								self.resetAuth();
								fail(data);
							}
						},
						function(error)
						{
							self.resetAuth();
							fail(error);
						}
					);
				else
				{
					auth.token = obj.token;
					auth.userId = obj.user_id;
					auth.username = obj.user_id;
					self.auth.username = obj.user_id;
					//загрузка в localstorage
					localStorage[LS.token] = obj.token;
					localStorage[LS.userId] = obj.user_id;
					localStorage[LS.username] = obj.user_id;
					success(obj.token);
				}
				//структура obj: username - логин, password - пароль
			},
			/**
			 * Авторизация через соц. сети
			 **/
			socialSignInUrl: function(socType, accType, obj)
			{
				switch(accType)
				{
					case 1:
						accType = 'admin';
						break;
					case 2:
						accType = 'student';
						break;
				}
				var state =
				{
					type: accType,
				};
				if(obj)
				{
					if (obj.roomUrl) state.room_url = obj.roomUrl;
					if (obj.promo) state.promo = obj.promo;
				}
				state = JSON.stringify(state);
				switch(socType) {
					case 'vk':
						if(accType == 'admin')
						{
							var endpoint = 'vk';
							if (state.promo)
								endpoint = 'vk_promo';
							return 'https://oauth.vk.com/authorize?client_id=5603934&display=page&redirect_uri=https://instaquiz.ru/verify/' + endpoint + '&scope=email,offline&response_type=code&v=5.53&state=' + state;
						}
						else
							return 'https://oauth.vk.com/authorize?client_id=5603934&display=page&redirect_uri=https://instaquiz.ru/verify/vk&scope=email,offline&response_type=code&v=5.53&state='+state;
					case 'fb':
						if(accType == 'admin')
						{
							var endpoint = 'facebook';
							if (state.promo)
								endpoint = 'facebook_promo';
							return 'https://www.facebook.com/dialog/oauth?client_id=1683681588623430&redirect_uri=https://instaquiz.ru/verify/' + endpoint + '&scope=email,public_profile&response_type=code&state=' + state;
						}
						else
							return 'https://www.facebook.com/dialog/oauth?client_id=1683681588623430&redirect_uri=https://instaquiz.ru/verify/facebook&scope=email,public_profile&response_type=code&state='+state;
					case 'google':
						if(accType == 'admin')
						{
							var endpoint = 'google';
							if (state.promo)
								endpoint = 'google_promo';
							return 'https://accounts.google.com/o/oauth2/v2/auth?client_id=643025549553-ockvhsmnkbth1egkk46bjpmj20ja23vh.apps.googleusercontent.com&redirect_uri=https://instaquiz.ru/verify/' + endpoint + '&scope=https://www.googleapis.com/auth/plus.login%20https://www.googleapis.com/auth/userinfo.email&access_type=online&response_type=code&state=' + state;
						}
						else
							return 'https://accounts.google.com/o/oauth2/v2/auth?client_id=643025549553-ockvhsmnkbth1egkk46bjpmj20ja23vh.apps.googleusercontent.com&redirect_uri=https://instaquiz.ru/verify/google&scope=https://www.googleapis.com/auth/plus.login%20https://www.googleapis.com/auth/userinfo.email&access_type=online&response_type=code&state='+state;
				}
			},
			/**
				@function userAuthService.UserAuthService.logOutSystem
				@desc Выход из системы
				@param {String} token токен авторизации
				@param {Function} success callback успешного выхода из системы. Аргументов нет
				@param {Function} fail callback ошибки выхода из системы. Аргумент error содержит код ошибки авторизации - code
			*/
			logOutSystem: function(token, success, fail)
			{
				var self = this;
				if(this.isTemporary())
				{
					this.resetAuth()
					if(success) success();
				}
				else
				apiAcc.signOut(token).$promise.
				then(
					function(data)
					{
						self.resetAuth();
						if(!data.error)			
							if(success) success();
						else
							if(fail) fail(data.error);
							
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
			
			/**
				@function userAuthService.UserAuthService.logInRoom
				@desc Авторизация в комнате
				@param {Object} obj объект авторизации
					@param {Number} obj.access тип доступа:
					0 - свободный; 1 - по имени; 2 - необходима авторизация в системе
					@param {String} obj.username логин пользователя в системе
					@param {String} obj.password пароль пользователя в системе
					@param {String} obj.roomPassword пароль комнаты (null - значит комната без пароля)
					@param {String} obj.roomId идентификатор комнаты
					@param {String} obj.token токен доступа
				@param {Function} success callback успешной авторизации в команту.
					Аргмуент data содержит token - токен доступа к системе
				@param {Function} fail callback неудачной авторизации в комнату.
					Аргумент error содержит code - код ошибки
			*/
			logInRoom: function(obj, success, fail)
			{
			
				var self = this;
				/*
				Структура obj:
					access - 0,1,2 - тип доступа
					username - логин/имя пользователя (в зависимости от access)
					password - пароль пользователя
					roomPassword - пароль комнаты (null - значит комната без пароля)
					roomId - идентификатор комнаты
					token - токен доступа
				В результате удачного логина в комнату пользователь получает токен
				(если был авторизован, то все равно получает токен)
				*/
				if(!obj.token)
					obj.token = auth.token;
				apiRooms.signIn(obj).$promise.
				then(
					function(data)
					{
						if(!data.error_code)
						{
							if(!auth.token)
							{
								auth.token = data.token;
								auth.username = obj.username;
								saveAuth();
							}
							success(auth.token);
						}
						else
						{
							//some code...
							if(data.error_code == 8) //получен неверный токен! Сброс текущего
								self.resetAuth();
							fail(data);
						}
					},
					function(error)
					{
						//some code...
						fail(error);
					}
				);
			},
			/**
			@function userAuthService.UserAuthService.logOutRoom
			@desc Выход пользователя из комнаты
			@param {String} token токе доступа
			@param {Function} success callback успешной авторизации в команту.
					Без аргументов.
			@param {Function} fail callback неудачной авторизации в комнату.
					Аргумент error содержит code - код ошибки
			*/
			logOutRoom: function(token, success, fail)
			{
				apiRoom.signOut(token).$promise.
				then(
					function(data)
					{
						if(!data.error_code)
							success();
						else
							fail(data);
					},
					function(error)
					{
						fail(error);
					}
				);
			},
			/**
				@function userAuthService.UserAuthService.getUserId
				@desc Геттер id пользователя
				@returns {Number} id пользователя
			*/
			getUserId: function()
			{
				return auth.userId;
			},
			/**
				@function userAuthService.UserAuthService.getUserToken
				@desc Геттер токена авторизации пользователя
				@returns {String} токен пользователя
			*/
			getUserToken: function()
			{
				return auth.token;
			},
			/**
				@function userAuthService.UserAuthService.getUsername
				@desc Геттер имени/логина пользователя 
				@returns {String} имя/логин пользователя
			*/
			getUsername: function()
			{
				return auth.username;
			},
			/**
			@function userAuthService.UserAuthService.isTemporary
			@desc Является ли авторизация пользователя временной (токен доступа временный)
			@returns {Boolean | null} null в случае отсутствия авторизации
			*/
			isTemporary: function()
			{
				if(auth.token)
					return String(auth.token).slice(-2) == '_t';
				else
					return null;
			},
			
			getProfile: function(success, fail)
			{
				if(!this.getUserToken())
				{
					if(fail) fail('no user token');
					return;
				}
				apiAcc.get(this.getUserId(), this.getUserToken()).$promise.then(
				function(data)
				{
					if(!data.error_code)
						success(data);
					else
						if(fail) fail(data);
				},
				function(error)
				{
					if(fail) fail(error);
				});
			}
		});
	}
	]);