angular.module('room.adminRoom', ['restApi.room', 'userAuthService', 'longPoller']).
	factory('AdminRoom', ['RestApiRoom', 'UserAuthService', 'LongPoller',
		function(RestApiRoom, UserAuthService, LongPoller)
		{
		
			var apiRoom = RestApiRoom;
			var auth = UserAuthService;
			//������ ������������ �������������
			var users = new List();/*
							{
								workingUserId
								firstName
								secondName
								username
							}
							*/
			//������� ����
			var currentTest =
			{
				streamId: null,
				test: null,
				position: null, //������� � ������� queue
			};
			//������� ������
			var queue = new List();
			/*
						��. api/tests/getInfo
						{
						test_id
						owner_id - 1
						name -1
						type - 1
						questions - 1
						timer - 1
						creation_time - 1
						editing_time - 1
						+
						overdrive:
							{
								type - 1,
								timer - 1
							}
						}
						*/
			
			//���������� ������������, ����������� ������������� ������
			var testResults = [];
			
			//"���������" �������
			var config = 
			{
				status: null, //������ �������
				access: null, //����� �������
				password: null, //������
				url: null //��������� ������������� �������
			};
			
			var time = 
			{
				addTime: null, //����� ���������� �����: � ������ ��� ����������� - ���������, ����� - 0
				currentTime: null, //������� ����� ����������� �����: ���������� addTime
				timer: null, //����� ���������� ��� ����������� �����
				time: null
			};
			
			var testState = null; /*
								active - ���� ������������
								ready - ���� ����� � �������
								cancelled - ���� �������
								finished - ���� ��������
								*/
			
			var timerId;
			
			//LP ������
			//���������� ������� �������
			var waitEvent = LongPoller();
			//������� �� ���������
			var waitStudentEvent = LongPoller();
			//������� ����������� ������������
			var waitResults = LongPoller();
			
			var room = 
			{
				init: function(success, fail)
				{
					var self = this;
					apiRoom.get(auth.getUserId(), auth.getUserToken()).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							config.status = data.status;
							config.access = data.access;
							config.password = data.password;
							config.url = data.url;
							if(config.status == 1)
							{
								apiRoom.getCurrentState(auth.getUserToken()).$promise.then(
								function(data)
								{
									if(!data.error_code)
									{
										currentTest.streamId = data.stream_id;
										if(currentTest.streamId) //�������� streamId, � ������ ���� �������� ����
										{
											currentTest.test = data.test;
											currentTest.position = data.position;
											users.list = data.users;
											queue.list = data.queue;
											testResults = data.testResults;
											time.addTime = data.add_time;
											time.currentTime = data.current_time;
											time.timer = data.timer;
											self.initChannels();
											self.start();
										}
									}
									else
									{
										//������ ��������
										fail(data);
									}
									
								},
								function(error)
								{
									//err while loading room state
									fail();
								}								
								);
							}
							else
								success();
						}
						else
						{
							//some code...
							fail(data);
						}
					},
					function(error)
					{
						//some code
						fail(error);
					}
					);
				},
				nextTest: function(success, fail)
				{
					if(queue.list.length > 0)
					{
						this.putIntoRoom(queue.list[0], success, fail);
						//������ ������ ������� (���� �� ����)
						//queue.list = queue.list.slice(1, queue.list.length-1);
					}
				},
				putIntoRoom: function(test, success, fail) //������ test ��. ����
				{
					apiTests.putIntoRoom(auth.getUserToken(), test.testId, auth.getUserId()).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							currentTest.test = test;
							currentTest.streamId = data.streamId;
							time.addTime = 0;
							time.currentTime = 0;
							testState = 'ready';
							success();
						}
						else
						{
							fail(data);
						}
					},
					function(error)
					{
						fail(error);
					});
				},
				startTest: function(passive)
				{
					//������ (��� �����������) ����� � �������
					testState = 'active';
					//������ �������
					if(time.timer)
						startTimer();
					this.events.test.onActivated();
				},
				cancelTest: function(passive)
				{
					//��� ��������� ������� �������� �������� �� ��, ��� ���� ��� ����� ���� �����
					//����� ������� ������� ������ ������������ � ���� ���������: ��� �������� � ��������� ��������
					
					//�������� ���������� �������� �����
					testState = 'cancelled';
					if(!passive)
						//��������� ������ �� ������
					this.events.test.onCancelled();
				},
				finishTest: function()
				{
					testState = 'finished';
					this.events.test.onFinished();
				},
				killStudent: function(workingUserId, passive)
				{
					var user = users.remove(function(item){return item.testId}, workingUserId);
					//������� �������� �� �������
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.user.onKilled(user);
				},
				
				//�������� ����������� ������ � ������!
				
				addTestToQ: function(test, pos, passive) //obj {test, position}
				{
					//���������� ����� � ������� (��� �������)
					queue.push(test); //���� ��� ������ ���������� � ����� ������
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.queue.onAdded(test);
				},
				removeTestFromQ: function(position, passive)
				{
					//�������� ����� �� �������
					var test = queue.removeFrom(position);
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.queue.onRemoved(test);
				},
				initChannels: function()
				{
					if(!waitEvent)
						waitEvent.poll(apiRoom.waitEvent(), eventReceived, errorEvent);
					if(!waitStudentEvent)
						waitStudentEvent.poll(apiRoom.waitStudentEvent(), studentEventReceived, errorStudentEvent);
					if(!waitResults)
						waitResults.poll(apiRoom.waitResults(), resultsReceived, resultsError);
				},
				changeStatus: function(val, passive)
				{
					config.status = val;
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.room.onChangedStatus();
				},
				changeAccess: function(obj, passive)
				{
					config.access = obj.access;
					if(obj.access == 2)
						config.password = obj.password;
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.room.onChangedAccess();
				},
				//callback-� ��� �������
				events: 
				{
					test:
					{
						onActivated: function()
						{
						},
						onCancelled: function()
						{
							
						},
						onFinished: function()
						{
						},
					},
					queue:
					{
						onAdded: function(test)
						{
						},
						onRemoved: function(test)
						{
						}
					},
					room:
					{
						onChangedAccess: function()
						{
						},
						onChangedStatus: function()
						{
						},
					},
					users:
					{
						onLogged: function(user) //user ����� � �������
						{
							//Callback �������. ����� � ����� ��� �������� ���������� � ������������ � ���� ������������. 
						},
						onLeft: function(user)
						{
							
						},
						onKilled: function(user)
						{
						},
						onLost: function(user)
						{
						
						},
						onFinished: function(user)
						{
						}
					}
				}
			};
			
			function eventReceived(data)
			{
				switch(data.type)
				{
					case 'test.activated':
						currentTest.test = queue.find(function(i){i.test_id}, data.test_id);
						currentTest.sessionId = data.session_id;
						room.startTest('passive');
						break;
					case 'test.cancelled':
						//���� ������� ��������������
						room.cancelTest('passive');
						break;
					case 'test.finished':
						room.finishTest();
					case 'queue.removed':
						//���� ����� �� �������
						room.removeTestFromQ(data.position, 'passive');
						break;
					case 'queue.added':
						//���� �������� � �������
						room.addTestToQ(data, 'passive');
						break;
					case 'ping':
						break;
					case 'room.access':
						//������� ����� �������
						room.changeAccess(data, 'passive');
						break;
					case 'room.status':
						//������� ������ �������
						room.changeStatus(data.status, 'passive');
						break;
				}
			}
			
			function errorEvent()
			{
				//��������� ������ ������ waitEvent
			}
			
			function studentEventReceived(data)
			{
				switch(data.type)
				{
					case 'logged':
						//������� ����� � �������
						var user = 
						{
							workingUserId: data.workingUser_id,
							firstName: data.first_name,
							secondName: data.second_name,
							username: data.username
						};
						users.push(user);
						room.events.users.onLogged(user);
						break;
					case 'left':
						//������� �������
						var user = users.remove(function(item){return item.testId}, data.test_id);
						room.events.users.onLeft(user);
						break;
					case 'killed':
						//�������� �� ������� ��������������
						room.killStudent(data.test_id,'passive');
						break;
					case 'lost':
						//�������� �����
						var user = users.remove(function(item){return item.testId}, data.test_id);
						room.events.users.onRemoved(user);
						break;
					case 'finished':
						//�������� ����������� �����
						var user = users.find(function(i){return i.workingUserId}, data.workingUser_id);
						room.events.users.onFinished(user);
						break;
				}
			}
			
			function errorStudentEvent()
			{
				//��������� ������ ������ waitStudentEvent
			}
			
			function resultsReceived(data)
			{
				testResults[String(data.workingUser_id)][String(data.question_id)] =
				{
					answers: data.answers,
					correctAnswer: data.correct_answer
				};
			}
			
			function resultsError()
			{
				
			}
			
			function startTimer()
			{
				time.time = time.currentTime - time.addTime;
				timerId = setInterval(function()
				{
					time.time++;
					if(time.time > time.timer)
					{
						//����� ���� ��������
						//�������� ��������� �� ��������� �� �������
						stopTimer();
					}
				}, 1000);
			};
			
			
			function stopTimer()
			{
				clearInterval(timerId);
			};
			
			room.__defineGetter__('users', function()
			{
				return users;
			});
			room.__defineGetter__('currentTest', function()
			{
				return currentTest;
			});
			room.__defineGetter__('queue', function()
			{
				return queue;
			});
			room.__defineGetter__('testResults', function()
			{
				return testResults;
			});
			room.__defineGetter__('config', function()
			{
				return config;
			});
			room.__defineGetter__('time', function()
			{
				return time;
			});
			room.__defineGetter__('testState', function()
			{
				return testState;
			});
			return room;
			
			function List()
			{
				this.list = [];
				this.removeFrom = function(position)
				{
					//this.list[position] = null;
					var item = this.list[position];
					this.list = this.list.slice(0, position-1).concat(this.list.slice(position+1,this.list.length-1));
					return item;
				};
				this.addTo = function(obj, position)
				{
					this.list = this.list.slice(0, position-1).push(obj).concat(this.list.slice(position+1,this.list.length-1));
				};
				this.push = function(obj)
				{
					this.list.push(obj);
				};
				this.remove = function(func, value)
				{
					for(i in this.list)
						if(func(this.list[i]) == value)
						{
							return this.removeFrom(i);
						}
							
				};
				this.find = function(key, value)
				{
					for(i in this.list)
						if(func(this.list[i]) == value)
							return this.list[i];
				}
			}
			
		}]);