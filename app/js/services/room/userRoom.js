/**
@namespace "room.userRoom"
@desc модуль реализующий сервис комнаты пользователя (студента)
*/
angular.module('room.userRoom',['restApi.room','userAuthService', 'longPoller', 'deviceService']).
	factory('UserRoom',['RestApiRoom','UserAuthService', 'LongPoller', 'DeviceService',
	/**
	@class "room.userRoom".UserRoom
	@desc Сервис реализующий комнату пользователя
	*/
	function(RestApiRoom, UserAuthService, LongPoller, DeviceService)
	{
		var auth = UserAuthService;
		var roomApi = RestApiRoom;
		
		var eventPoller = LongPoller();
		
		var localStorage = window.localStorage;
		
		//наличие соединения с комнатой (long poll)
		var connected;
		
		var LS = 
		{
			url: 'userRoom.url'
		};
		var room =
		{
			roomId: null,
			_url: null,
			ownerIfno: null,
			status: null,
			access: null,
			needPassword: null,
			//showResults: false,
		}
		
		room.__defineGetter__('url', function()
		{
			return room._url;
		});
		
		room.__defineSetter__('url', function(val)
		{
			room._url = val;
			if(val == null)
				localStorage.removeItem(LS.url);
			else
				localStorage[LS.url] = val;
		});
		
		
		//инициализация комнаты
		room._url = localStorage[LS.url]
		
		//хеши событий
		var eventHash = 
		{
			waitStudentEvent: null,
		};
		
		var self = this;
		
		var service = 
		{
			/**
			@name "room.userRoom".UserRoom.room
			@desc Объект комнаты
			@param url {String} идентификатор комнаты (символический идентификатор)
			@param ownerInfo {Object}  информация о владельце комнаты
			@param status {String} состояние команты: 0 - неактивна, 1 - активна
			@param access {Number} тип доступа:
				0 - свободный доступ,
				1 - доступ по имени,
				2 - доступ только с авторизацией
			*/
		//	room: room,
			/**
			@function "room.userRoom".UserRoom.getRoom
			@desc Запрос команты. Если комната существует вызывается callback success, иначе fail.
			В случае успешного "получения" комнаты возвращается токен доступа пользователя в систему
			(если доступ происходит с полной авторизацией, то тогда возвращаемый токен такой же как и был)
			@param id {String} символический идентификатор комнаты
			@param success {Function} callback успешного доступа к комнате
			@param fail {Function} callback в случае неуспешного доступа к комнате (нет прав доступа/команты не существует)
			*/
			
			getRoom: function(id, success, fail)
			{
				if(!room.url && (!id || id == undefined))
				{
					fail({
					error: {code: 202}
					});
					return;
				}
				if(id && id != undefined && room.url != id)
					room.url = id;
				roomApi.getByUrl(room.url).$promise.
				then(
				function(data)
				{
					if(!data.error_code)
					{
						room.roomId = data.room_id;
						room.ownerInfo = data.owner_info;
						room.needPassword = data.need_pass;
						room.status = data.status;
						room.access = data.access;
						success(data);
					}
					else
					{
						fail(data);
						resetRoom();
					}
				},
				function(error)
				{
					/*Объект error тут не соответсвует объекту error,
					получаемому от сервера
					*/
					fail(error);
					resetRoom();
				}
				);
			},
					
			init: function(success, fail)
			{
				if(!room.roomId)
				{
					if(fail) fail();
					return;
				}
				var self = this;
				connected = true;
				initChannels();
				roomApi.getCurrentState(auth.getUserToken(), 2, room.roomId, DeviceService.deviceId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						/*if(data.show_results)
							room.showResults = data.show_results;*/
						if(data.hashes)
						{
							if(data.hashes.waitStduentEvent)
								eventHash.waitStduentEvent = data.hashes.waitStduentEvent;
						}
						if(success) success(data);
					}
					else
					{
						connected = false;
						if(fail) fail(data);
					}
				},
				function(error)
				{
					connected = false;
					fail(error);
				}
				);
			},
			
			resetRoom: resetRoom,
			
			quitRoom: function(success, fail)
			{
				if(!auth.getUserToken())
					resetRoom();
				else
				roomApi.signOut(auth.getUserToken(), room.roomId).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							resetRoom();
							if(success) success();
						}
						else
						{
							if(fail) fail(data);
						}					
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
			
			events:
			{
				test:
				{
					onActivated: function(data)
					{
						//заглушка, т.е. функция переопределяется в контроллере
					},
					onFinished: function(data)
					{
					},
					onCancelled: function(data)
					{
					}
				},
				room:
				{
					onStatusChanged: function(data)
					{
					},
					onBanned: function(data)
					{
					},
				},
				error:
				{
					pollerError: function(data)
					{
					}
				},
				question:
				{
					onEnabled: function() {},
					onFinished: function() {}
				},
				lesson:
				{
					onFinished: function(){}
				},
			},
			
		};
				
		service.__defineGetter__('roomId', function()
		{
			return room.roomId;
		});
		service.__defineGetter__('url', function()
		{
			return room.url;
		});
		service.__defineGetter__('connected', function()
		{
			return connected;
		});
		service.__defineGetter__('status', function()
		{
			return room.status;
		});
		/*service.__defineGetter__('showResults', function()
		{
			return room.showResults;
		});*/
		return service;
		
		function resetRoom()
		{
			room.roomId = null;
			room.url = null;
			if(eventPoller)
				eventPoller.stop();
			eventPoller = LongPoller();
			connected = false;
		}
		
		function receivedEvent(data)
		{
			if(data.hash)
				eventHash.waitStudentEvent = data.hash;
			for(var i in data.events)
				switch(data.events[i].event_type)
				{	
					case 'test.activated':
						service.events.test.onActivated(data.events[i]);
						break;
					case 'test.finished':
						service.events.test.onFinished(data.events[i]);
						break;
					case 'test.cancelled':
						service.events.test.onCancelled(data.events[i]);
						break;
					case 'lesson.finished':
						//room.status = data.status;
						//if(room.status == false)
						//	resetRoom();
						//service.events.room.onStatusChanged(data.events[i]);
						eventPoller.stop();
						room.url = null;
						service.events.lesson.onFinished();
						break;
					case 'room.banned':
						resetRoom();
						service.events.room.onBanned(data.events[i]);
						break;
					/*case 'room.show_results':
						room.showResults = data.show_results;
						break;*/
					case 'question.enabled': //событие активации следующего вопроса при pace_mode = 202}
						service.events.question.onEnabled();
						break;
					case 'question.finished':
						service.events.question.onFinished();
						break;
				}
		}
		
		function errorEvent(data)
		{
			connected = false;
			eventPoller.stop();
			service.events.error.pollerError(data);
		}
		
		function initChannels()
		{
			if(eventPoller && eventPoller.stopped)
				eventPoller.poll(function(){return roomApi.waitStudentEvent(UserAuthService.getUserToken(), room.roomId, eventHash.waitStudentEvent);}, receivedEvent, errorEvent);
		}
		
	}]);