angular.module('room.adminRoom', ['restApi.room', 'restApi.tests', 'userAuthService', 'longPoller']).
factory('AdminRoom', ['RestApiRoom', 'RestApiTests', 'AdminRoomResults', 'UserAuthService', 'LongPoller',
	function(RestApiRoom, RestApiTests, AdminRoomResults, UserAuthService, LongPoller)
	{

		var apiRoom = RestApiRoom;
		var apiTests = RestApiTests
		var auth = UserAuthService;
		//список подключенных пользователей
		var users = {};/*
	 {
	 workingUserId
	 firstName
	 secondName
	 username
	 }
	 */
		//текущий тест
		var currentTest =
		{
			streamId: null,
			test: null,
			showResults: null,
			paceMode: null,
		};
		//текущий урок
		var currentLesson = {};

		var testState = null; /*
	 active - идет тестирование
	 ready - тест готов к запуску
	 cancel - тест отменен
	 finish - тест завершен
	 */

		var currentQuestion = null; //номер текущего вопроса (для pace_mode == 2)
		var questionState = null; /*
	 finish - завершен
	 active - активен
	 */
		var lessonState = null; /* finish - урок завершен, active - урок запущен*/
		//очередь тестов
		var queue = new List();
		/*
		 см. api/tests/getInfo
		 {
		 test_id
		 owner_id - 1
		 name -1
		 type - 1
		 questions - 1
		 timer - 1
		 creation_time - 1
		 editing_time - 1
		 +
		 override:
		 {
		 type - 1,
		 timer - 1
		 }
		 }
		 */

		//результаты тестирования - словарь в соответствии с workingUser_id студентов
		//возможна проблема с таблицей ответов, так как будет перечисление словаря
		var testResults = AdminRoomResults.results.testResults;

		//"параметры" комнаты
		var config =
		{
			roomId: null,
			status: null, //статус комнаты
			access: null, //режим доступа
			password: null, //пароль
			url: null, //строковый идентификатор комнаты
			//showResults: null, //демонстрациЯ результатов студентам
		};

		var time =
		{
			addTime: null, //время добавления теста: в случае уже запущенного - серверное, иначе - 0
			currentTime: null, //текущее время прохождения теста: аналогично addTime
			time: null
		};


		var timerId;

		//LP каналы
		//глобальные события комнаты
		var waitEvent = LongPoller('waitEvent');
		//waitEvent.limit = 10;
		//события от студентов
		var waitEventFromStudents = LongPoller('waitEventFromStudents');
		//waitEventFromStudents.limit = 10;
		//события результатов тестирования
		var waitResults = LongPoller('waitResults');
		//waitResults.limit = 10;
		//хэши событий
		var eventHash =
		{
			waitEvent: null,
			waitResults: null,
			waitEventFromStudents: null
		};
		/*Ожидаемое клиентом событие.
		 Существует проблема повторной передачи событий для клиента, который был инициатором этого события.
		 Пример: добавление в очередь теста приводит к двоному добавлению (один из клиента, другой благодаря событию от сервера)
		 Если пришло событие (например типа queue.added), то сначала проверяется
		 было ли оно ожидаемым type = expectedEvents.waitEvent[i] (i - номер элемента списка)
		 Если было - то пропустить обработку события, а expectedEvent.waitEvent = []
		 expectedEvent должно быть установлено там, где необходимо.
		 Т.е. для примера queue.added в методе добавления putIntoRoom
		 */
		var expectedEvents =
		{
			waitEvent: [],
			waitStudentEvent: [],
			waitResults: [],
		};

		//есть подключение к комнате
		var connected = false;

		var room =
		{
			init: function(success, fail)
			{
				var self = this;
				apiRoom.get(auth.getUserToken()).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							config.roomId = data.room_id;
							config.status = data.status;
							config.access = data.access;
							config.password = data.password;
							if(data.url && data.lesson)
							{
								config.url = data.url;
								lessonState = 'active';
								currentLesson = data.lesson;
							}

							//config.showResults = data.show_results;
							// if(config.status == true)
							// {
							self.getCurrentState(
								function()
								{
									self.initChannels();
									connected = true;
									if(success)
										success();
								},
								function()
								{
									connected = false;
									if(fail)
										fail();
								});
							// }
							// else
							// {
							// 	connected = true;
							// 	if(success)
							// 		success();
							// }
						}
						else
						{
							connected = false;
							//some code...
							if(fail)
								fail(data);
						}
					},
					function(error)
					{
						connected = false;
						//some code
						if(fail)
							fail(error);
					}
				);
			},
			/*putIntoRoom: function(test, success, fail)
			 {
			 /*test:
			 {
			 test_id
			 position - optional
			 override:
			 {
			 type
			 timer
			 }
			 }

			 apiTests.putIntoRoom(auth.getUserToken(), test_id, position).$promise.then(
			 function(data)
			 {
			 if(!data.error_code)
			 {

			 currentTest.test = test;
			 currentTest.streamId = data.streamId;
			 success();
			 }
			 else
			 {
			 fail(data);
			 }
			 },
			 function(error)
			 {
			 fail(error);
			 });
			 },*/
			startLesson: function(config, success, fail)
			{
				var self = this;
				apiRoom.startLesson(auth.getUserToken(),config.id,config.title,config.description).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							expectedEvents.waitEvent.push('lesson.started');
							self.startLessonHandler({url: data.room_url, title: config.title, description: config.description});
							if(success) success();
						}
						else
						if(fail) fail(data);
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
			startLessonHandler: function(data)
			{
				users = {};
				currentLesson = {title: data.title, description: data.description, start_time: Date.now()/1000};
				currentTest.streamId = null;
				currentTest.test = null;
				queue = new List();
				config.status = true;
				AdminRoomResults.reset();
				testState = null;
				config.url = data.url;
				lessonState = 'active';
			},

			endLesson: function(success, fail)
			{
				var self = this;
				apiRoom.endLesson(auth.getUserToken()).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							expectedEvents.waitEvent.push('lesson.finished');
							self.endLessonHandler();
							if(success) success();
						}
						else
						if(fail) fail(data);
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
			endLessonHandler: function()
			{
				lessonState = 'finish';
				this.events.lesson.onFinished();
			},
			createActivity: function(type, activity, success, fail)
			{
				if(!type)
					throw Error('activity type is not defined');
				apiRoom.createActivity(auth.getUserToken(), type, activity).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							// возвращается test_id созданной активности
							if(success) success(data.test_id);
						}
						else
						{
							if(fail) fail(data);
						}
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			},
			resume: function(paceMode, options, testId)
			{
				if(queue.list.length > 0 || testId != null)
				{
					for(var i in users)
						if(users[i].del_reason == 'left')
							delete users[i];
					apiRoom.resume(auth.getUserToken(), paceMode, options, testId); //продолжить прохождение тестов в комнате
				}
				else
				{
					testState = null;
					currentTest.test = null;
				}
			},
			startTest: function(passive)
			{
				//запуск (или продолжение) теста в комнате
				testState = 'active';
				//запуск таймера
				//startTimer(); - Ќ… Ќ“†ЌЋ!
			},
			cancelTest: function(passive)
			{
				//при написании функции обратить внимание на то, что тест уже может быть начат
				//таким образом функция должна отрабатывать в двух сценариях: при активном и пассивном клиентах
				//отменить проведение текущего теста
				testState = 'cancel';
				stopTimer();
				if(passive)
					handler()
				else
					apiRoom.killTest(auth.getUserToken(), currentTest.streamId, 'cancel').$promise.then(
						function()
						{
							expectedEvents.waitEvent.push('test.cancelled');
							handler();
						}
					);
				function handler()
				{
					testState = 'cancel';
					stopTimer();
					room.events.test.onCancelled();
				}
			},
			finishTest: function(passive)
			{
				if(passive)
					handler();
				else
					apiRoom.killTest(auth.getUserToken(), currentTest.streamId, 'finish').$promise.then(
						function()
						{
							expectedEvents.waitEvent.push('test.finished');
							handler();
						}
					);
				function handler()
				{
					testState = 'finish';
					stopTimer();
					room.events.test.onFinished();
				}
			},
			//функция завершения вопроса (для pace_mode == 2)
			finishQuestion: function(passive)
			{
				if(!passive)
					apiRoom.finishQuestion(auth.getUserToken()).$promise.then(
						function(data)
						{
							expectedEvents.waitEvent.push('question.finished');
							handler();
						});
				else
					handler();
				function handler()
				{
					questionState = 'finished';
					room.events.question.onFinished();
				}
			},
			//функция активации следующего вопроса (для pace_mode == 2)
			enableNextQuestion: function(passive)
			{
				if(!passive)
					apiRoom.enableNextQuestion(auth.getUserToken()).$promise.then(
						function(data)
						{
							expectedEvents.waitEvent.push('question.enabled');
							handler();
						});
				else
					handler();
				function handler()
				{
					currentQuestion += 1;
					questionState = 'enabled';
					room.events.question.onEnabled();
				}
			},
			killStudent: function(workingUserId, passive)
			{
				var user = users[workingUserId];
				//user.del_reason = 'banned';
				delete users[workingUserId];
				//удалить студента из комнаты
				if(!passive)
				{
					/*
					 ЕСЛИ ЗАПРОС НЕ ПРОЙДЕТ, ТО ПОЛЬЗОВАТЕЛЬ ОКАЖЕТСЯ НЕДОСТУПЕН НА ЭТОМ КЛИЕНТЕ!
					 ДОБАВИТЬ ПРОВЕРКУ УСПЕШНОСТИ ЗАПРОСА
					 */
					expectedEvents.waitStudentEvent.push('killed');
					apiRoom.killStudent(auth.getUserToken(), workingUserId);
				}
				this.events.users.onKilled(user);
			},

			//добавить перемещение тестов в списке!

			putIntoRoom: function(test, pos, passive, success, fail) //obj {test, position}
			{
				console.log('put into room',test);
				var self = this;
				if(!passive)
				{
					apiRoom.putIntoRoom(auth.getUserToken(), test.test_id, pos).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								//добавление теста в комнату (или очередь)
								queue.addTo(test, pos-1);
								expectedEvents.waitEvent.push('queue.added');
								self.events.queue.onAdded(test);
								if(success) success();
							}
							else
							{
								//queue.removeFrom(pos-1);
								if(fail) fail();
							}
						},
						function(error)
						{
							queue.removeFrom(pos-1);
							if(fail) fail();
						});
				}
				else
				{
					queue.addTo(test, pos - 1);
					this.events.queue.onAdded(test);
					if(success) success();
				}
			},
			removeFromRoom: function(pos, testId, passive, success, fail)
			{
				var self = this;
				if(!passive)
				{
					apiRoom.removeFromRoom(auth.getUserToken(), pos, testId).$promise.then(
						function(data)
						{
							if(!data.error)
							{
								//удаление теста из очереди
								var test = queue.removeFrom(pos-1);
								expectedEvents.waitEvent.push('queue.removed');
								self.events.queue.onRemoved(test);
								if(success) success(test);
							}
							else
							{
								if(fail) fail(data);
							}
						},
						function(error)
						{
							queue.addTo(test, pos-1);
							if(fail) fail(error);
						});
				}
				else
				{
					var test = queue.removeFrom(pos-1);
					self.events.queue.onRemoved(test);
					if(success) success(test);
				}
			},
			overrideTest: function(obj, pos, passive)
			{
				var test;
				if(pos == null)
				{
					if(testState != 'active')
						return;
					test = currentTest.test;
				}
				else
					test = queue.list[pos-1];
				if(!test.override)
					test.override = {};
				if(obj.timer)
					test.override.timer = obj.timer;
				if(obj.type)
					test.override.type = obj.type;
				if(!passive)
				{
					console.log(obj);
					apiRoom.overrideTest(auth.getUserToken(), test.test_id, pos, obj);
					expectedEvents.waitEvent.push('test.overrided');
				}

			},
			initChannels: function()
			{
				if(waitEvent && waitEvent.stopped)
					waitEvent.poll(function(){return apiRoom.waitEvent(auth.getUserToken(), eventHash.waitEvent)}, eventReceived, errorEvent);
				if(waitEventFromStudents && waitEventFromStudents.stopped)
					waitEventFromStudents.poll(function() {return apiRoom.waitEventFromStudents(auth.getUserToken(), eventHash.waitEventFromStudents)}, studentEventReceived, errorStudentEvent);
				if(waitResults && waitResults.stopped)
					waitResults.poll(function(){return apiRoom.waitResults(auth.getUserToken(), currentTest.streamId, eventHash.waitResults)}, resultsReceived, resultsError);
			},
			changeStatus: function(val, passive)
			{
				var self = this;
				if(config.status == val)
					return;
				if(val == false)
				{
					/*if(testState == 'active')
					 {
					 //прежде необходимо остановить тест
					 return;
					 этот запрос может и не пройти!
					 apiRoom.killTest(auth.getUserToken(), test.streamId);
					 stopTimer();
					 }*/
					//testState = null;
					//reset();
				}
				/*else
				 {
				 testState = null;
				 //self.getCurrentState();
				 }*/
				config.status = val;
				if(!passive)
				{
					apiRoom.setStatus(auth.getUserToken(), val).$promise.then(
						function(data)
						{
							if(!data.error)
							{
								expectedEvents.waitEvent.push('room.status');
								self.events.room.onChangedStatus();
							}
							else
								config.status = !val;
						},
						function(error)
						{
							config.status = !val;
						});
				}
				else
					self.events.room.onChangedStatus();
			},
			changeAccess: function(obj, passive)
			{
				console.log('access',obj)
				if(obj.access == config.access && config.password == obj.password)
					return;
				if(!passive)
				{
					apiRoom.saveRoomInfo(auth.getUserToken(), obj).$promise.then(
						function(data)
						{
							expectedEvents.waitEvent.push('room.info');
							if(obj.access != config.access)
								config.access = obj.access;
							if(obj.password != config.password)
								config.password = obj.password;
						},
						function(error)
						{
						});
				}
				else
				{
					if(obj.access != config.access)
						config.access = obj.access;
					if(obj.password != config.password)
						config.password = obj.password;
				}
				if(this.events.room.onChangedAccess)
					this.events.room.onChangedAccess();
			},
			/*showResults: function(value, passive)
			 {
			 if(config.showResults == value)
			 return;
			 if(!passive)
			 {
			 apiRoom.saveRoomInfo(auth.getUserToken(),{showResults: value}).$promise.then(
			 function(data)
			 {
			 config.showResults = value;
			 },
			 function(error)
			 {
			 });
			 }
			 else
			 config.showResults = value;
			 if(this.events.room.onChangedShowResults)
			 this.events.room.onChangedShowResults();
			 },*/
			/*	changeAccess: function(obj, passive)
			 {
			 config.access = obj.access;
			 if(obj.access == 2)
			 config.password = obj.password;
			 if(!passive)
			 {
			 //отправить запрос на сервер
			 }
			 this.events.room.onChangedAccess();
			 },*/

			getCurrentState: function(success, fail)
			{
				var self = this;
				apiRoom.getCurrentState(auth.getUserToken(),1,config.roomId).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							//users = data.users;
							users = {};
							for(var i in data.users)
								users[data.users[i].workingUser_id] = data.users[i];
							if(data.queue == "")
								queue.list = [];
							else
								queue.list = data.queue;
							if(data.hashes)
							{
								if(data.hashes.waitResults)
									eventHash.waitResults = data.hashes.waitResults;
								if(data.hashes.waitEventFromStudents)
									eventHash.waitEventFromStudents = data.hashes.waitEventFromStudents;
								if(data.hashes.waitEvent)
									eventHash.waitEvent = data.hashes.waitEvent;
							}
							testState = data.test_state;
							if(data.current_test)
							{
								currentTest.test = data.current_test;
								currentTest.showResults = data.show_results;
								currentTest.paceMode = data.pace_mode;
								currentQuestion = data.question_index;
								questionState = data.question_state;
								AdminRoomResults.getFullTest(currentTest.test.test_id); //необходимо для загрузки всех вопросов и ответов ЗАРАНЕЕ
								AdminRoomResults.init(data.users);
								currentTest.streamId = data.stream_id;
								time.addTime = data.add_time*1000;//возвращаетсЯ в секундах
								time.currentTime = new Date();
								if(data.test_state == 'active')
									self.startTest();
							}
							if(data.question_index != null)
								currentQuestion = data.question_index;
							if(data.currentState != null)
								questionState = data.questionState;
							if(success)
								success();
						}
						else
						{
							//ошибка загрузки
							if(fail)
								fail(data);
						}

					},
					function(error)
					{
						//err while loading room state
						if(fail)
							fail();
					});
			},

			reset: reset,
			//callback-и для событий
			events:
			{
				lesson:
				{
					onFinished: function(){},
				},
				test:
				{
					onActivated: function()
					{
					},
					onCancelled: function()
					{

					},
					onFinished: function()
					{
					},
				},
				question:
				{
					onFinished: function() {},
					onEnabled: function() {}
				},
				queue:
				{
					onAdded: function(test)
					{
					},
					onRemoved: function(test)
					{
					}
				},
				room:
				{
					onChangedAccess: function()
					{
					},
					onChangedStatus: function()
					{
					},
					connectionLost: function()
					{

					},
					/*onChangedShowResults: function()
					 {
					 },*/
				},
				users:
				{
					onLogged: function(user) //user зашел в комнату
					{
						//Callback события. Здесь и далее они являются заглушками и определяются в теле контроллеров.
					},
					onLeft: function(user)
					{

					},
					onKilled: function(user)
					{
					},
					onLost: function(user)
					{

					},
					onFinished: function(user)
					{
					},
					onResult: function(result)
					{

					},
				},
				timer:
				{
					onTick: function()
					{
					},
					onStart: function()
					{
					},
					onStop: function()
					{
					}
				}
			}
		};

		function eventReceived(eventsData)
		{
			if(eventsData.hash)
				eventHash.waitEvent = eventsData.hash;
			if(eventsData.events)
				var evData = eventsData.events;
			else
				evData = [eventsData];

			for(var e in evData)
			{
				var data = evData[e];
				if (data.event_type)
					var type = data.event_type.toLowerCase();
				else
					return;
				/*Когда будут добавлены хэши, сделать нормальную обработку
				 * Сейчас предполагается что события приходят сразу и без задержек,
				 * поэтому массив содержит только один элемент*/
				if (expectedEvents.waitEvent[expectedEvents.waitEvent.length - 1] == type)
				{
					console.log('try expected');
					expectedEvents.waitEvent = [];
					return;
				}
				switch (type)
				{
					case 'lesson.started':
						if (data.room_url)
							room.startLessonHandler(data.room_url);
						break;
					case 'lesson.finished':
						room.endLessonHandler();
						break;
					case 'test.activated':
						if (!data.test)
							currentTest.test = queue.removeFrom(0);
						else
							currentTest.test = data.test;
						currentTest.streamId = data.stream_id;
						currentTest.showResults = data.show_results;
						currentTest.paceMode = data.pace_mode;
						if (currentTest.paceMode == 2)
						{
							currentQuestion = 1;
							questionState = 'enabled';
						}
						else if (currentTest.paceMode == 1)
						{
							currentQuestion = null;
							questionState = null;
						}
						time.addTime = 0;
						time.currentTime = 0;
						AdminRoomResults.reset();
						AdminRoomResults.getFullTest(currentTest.test.test_id); //необходимо для загрузки всех вопросов и ответов ЗАРАНЕЕ
						room.startTest('passive');
						room.events.test.onActivated();
						break;
					case 'test.cancelled':
						//тест отменен преподавателем
						room.cancelTest('passive');
						break;
					case 'test.finished':
						room.finishTest('passive');
						break;
					case 'test.overrided':
						room.overrideTest(data.override, data.position, 'passive');
						break;
					case 'queue.removed':
						//тест убран из очереди
						room.removeFromRoom(data.position, data.test_id, 'passive');
						break;
					case 'queue.added':
						//тест добавлен в очередь
						room.putIntoRoom(data.test, data.position, 'passive');
						break;
					case 'ping':
						break;
					case 'room.info':
						//изменен режим доступа
						if (data.access || data.password)
							room.changeAccess({access: data.access, password: data.password}, 'passive');
						/*if(data.show_results)
						 room.showResults(data.show_results,'passive')*/
						break;
					case 'room.status':
						//изменен статус комнаты
						room.changeStatus(data.status, 'passive');
						break;
					/*case 'room.show_results':
					 //изменен статус демонстрации результатов тестированиЯ студентам
					 room.showResults(data.show_results, 'passive');
					 break;*/
					case 'question.finished':
						room.finishQuestion('passive');
						break;
					case 'question.enabled':
						room.enableNextQuestion('passive');
						break;
				}
			}
		}

		function errorEvent()
		{
			//произошла ошибка канала waitEvent
			waitEvent.stop();
			connected = false;
			room.events.room.connectionLost();
			console.log('errorEvent');
		}

		function studentEventReceived(eventsData)
		{

			if(eventsData.hash)
				eventHash.waitEventFromStudents = eventsData.hash;
			if(eventsData.events)
				var evData = eventsData.events;
			else
				evData = [eventsData];

			for(var e in evData)
			{
				var data = evData[e];
				// if(!evData.results.hasOwnProperty(data))
				// 	continue;
				if(data.event_type)
					var type = data.event_type.toLowerCase();
				else
					continue;
				// if(expectedEvents.waitStudentEvent[0] == type)
				// {
				// 	expectedEvents.waitStudentEvent = [];
				// 	return;
				// }
				switch (type) {
					case 'logged':
						//студент зашел в комнату
						var user =
						{
							workingUser_id: data.workingUser_id,
							first_name: data.first_name,
							second_name: data.second_name,
							username: data.username
						};
						users[data.workingUser_id] = user;
						//testResults[data.workingUser_id] = [];
						if (room.events.users.onLogged) room.events.users.onLogged(user);
						break;
					case 'left':
						//покинул комнату
						//var user = users.remove(function(item){return item.testId}, data.test_id);
						var user = users[data.workingUser_id];
						user.del_reason = 'left';
						if (room.events.users.onLeft) room.events.users.onLeft(user);
						break;
					case 'killed':
						//исключен из комнаты преподавателем
						room.killStudent(data.workingUser_id, 'passive');
						break;
					case 'lost':
						//потеряна связь
						//var user = users.remove(function(item){return item.testId}, data.test_id);
						//var user = users[data.workingUser_id];
						//delete users[data.workingUser_id];
						if (room.events.users.onLost) room.events.users.onLost(user);
						break;
					case 'finished':
						//закончил прохождение теста
						//var user = users.find(function(i){return i.workingUserId}, data.workingUser_id);
						var user = users[data.workingUser_id];
						if (room.events.users.onFinished) room.events.users.onFinished(user);
						break;
				}
			}
		}

		function errorStudentEvent()
		{
			waitEventFromStudents.stop();
			connected = false;
			//произошла ошибка канала waitEventFromStudent
		}

		function resultsReceived(data)
		{
			var results = data.results;
			if(data.hash)
				eventHash.waitResults = data.hash;
			if(results)
			{
				AdminRoomResults.add(data.results);
				if(room.events.users.onResult)
					room.events.users.onResult(data.results);
			}
			/*for(var i in results)
			 {
			 if(!testResults[results[i].workingUser_id])
			 testResults[results[i].workingUser_id] = [];
			 testResults[results[i].workingUser_id].push(
			 {
			 question_id: results[i].question_id,
			 answers: results[i].answers,
			 correct_answers: results[i].correct_answers,
			 all_correct_answers: results[i].all_correct_answers,
			 type: results[i].type,
			 });
			 console.log(testResults);
			 }*/
		}

		function resultsError()
		{
			connected = false;
			waitResults.stop();
		}

		function startTimer()
		{
			if(timerId)
				return;
			room.events.timer.onStart();
			time.time = time.currentTime - time.addTime;
			var obj = null;

			timerId = setInterval(function()
			{
				time.time++;
				room.events.timer.onTick();
				if(currentTest.test.override && currentTest.test.override.timer)
					obj = currentTest.test.override;
				else
				if(currentTest.test.timer)
					obj = currentTest.test;
				if(obj)
					if(time.time > obj.timer && obj.timer > 0)
					{
						//время тест окончено
						//ожидание сообщения об окончании от сервера
						stopTimer();
					}
			}, 1000);
		};

		function reset()
		{
			if(waitEvent)
				waitEvent.stop();
			if(waitEventFromStudents)
				waitEventFromStudents.stop();
			if(waitResults)
				waitResults.stop();
			users = {};
			currentTest.streamId = null;
			currentTest.test = null;
			queue = new List();
			AdminRoomResults.reset();
			time.addTime = null;
			time.currentTime = null;
			time.time = null;
			testState = null;
			connected = false;
			lessonState = null;
		}

		function stopTimer()
		{
			room.events.timer.onStop();
			clearInterval(timerId);
		};

		room.__defineGetter__('users', function()
		{
			return users;
		});
		room.__defineGetter__('currentTest', function()
		{
			return currentTest;
		});
		room.__defineGetter__('queue', function()
		{
			return queue;
		});
		room.__defineGetter__('testResults', function()
		{
			return testResults;
		});
		room.__defineGetter__('config', function()
		{
			return config;
		});
		room.__defineGetter__('time', function()
		{
			return time;
		});
		room.__defineGetter__('testState', function()
		{
			return testState;
		});
		room.__defineGetter__('connected', function()
		{
			return connected;
		});
		room.__defineGetter__('currentQuestion', function()
		{
			return currentQuestion;
		});
		room.__defineGetter__('currentLesson', function()
		{
			return currentLesson;
		});
		room.__defineGetter__('questionState', function()
		{
			return questionState;
		});
		room.__defineGetter__('lessonState', function()
		{
			return lessonState;
		});
		return room;

		function List()
		{
			this.list = [];
			this.removeFrom = function(position)
			{
				//this.list[position] = null;
				var item = this.list[position];
				this.list = this.list.slice(0, position).concat(this.list.slice(position+1));
				console.log(position);
				console.log(this.list);
				return item;
			};
			this.addTo = function(obj, position)
			{
				var tmp = this.list.slice(0, position);
				console.log(this.list)
				tmp.push(obj);
				tmp = tmp.concat(this.list.slice(position));
				this.list = tmp;
				console.log(this.list);
			};
			this.push = function(obj)
			{
				this.list.push(obj);
			};
			this.remove = function(func, value)
			{
				for(i in this.list)
					if(func(this.list[i]) == value)
					{
						return this.removeFrom(i);
					}

			};
			this.find = function(key, value)
			{
				for(i in this.list)
					if(func(this.list[i]) == value)
						return this.list[i];
			}
		}

	}]).
factory('AdminRoomResults', ['RestApiRoom', 'RestApiTests', 'UserAuthService', '$filter',
	function(RestApiRoom, RestApiTests, UserAuthService, $filter)
	{
		var auth = UserAuthService;
		var apiTests = RestApiTests;
		//кэш длЯ подгружаемых вопросов и ответов
		var groupsCache = {};
		var questionsCache = {};
		/*
		 Џлагины (plugins) обеспечивают преобразование данных в конкретную модель
		 ‘остоит из
		 {
		 name - название объекта доступа к данным
		 results: {} - объект результатов
		 init(data) - метода инициализации модели данных (формат data см. в rooms/getCurrentState)
		 add(data) - метод добавлениЯ новой порции данных (формат data см. в rooms/waitResults)
		 reset() - метод длЯ "обнулениЯ" данных
		 }
		 */

		var testResultsPlugin =
		{
			name: 'testResults',
			results: {},
			init: function(data)
			{
				for(var i in data)
				{
					this.results[data[i].workingUser_id] = data[i].questions;//$filter('orderBy')(data[i].questions, 'position');
					//console.log(data[i].questions);
				}
			},
			add: function(data)
			{
				var self = this;
				for(var i in data)
				{
					var obj = {
						question_id: data[i].question_id,
						answers: data[i].answers,
						correct_answers: data[i].correct_answers,
						all_correct_answers: data[i].all_correct_answers,
						type: data[i].type,
						group_id: data[i].group_id,
						position: data[i].position,
					};
					if(!self.results[data[i].workingUser_id])
						self.results[data[i].workingUser_id] = [];
					self.results[data[i].workingUser_id].push(obj);
					// if(!self.results[data[i].workingUser_id])
					// 	self.results[data[i].workingUser_id] = [];
					// var obj = {
					// 	question_id: data[i].question_id,
					// 	answers: data[i].answers,
					// 	correct_answers: data[i].correct_answers,
					// 	all_correct_answers: data[i].all_correct_answers,
					// 	type: data[i].type,
					// 	group_id: data[i].group_id,
					// 	position: data[i].position,
					// };
					// if(data[i].position)
					// 	self.results[data[i].workingUser_id][data[i].position-1] = obj;
					// else
					// 	self.results[data[i].workingUser_id].push(obj);
				}
			},
			reset: function()
			{
				this.results = {};
			}
		};
		/*
		 Џлагин предоставлЯет модель результатов опроса/тестированиЯ по вопросам (статистики).
		 „лЯ каждого вопроса выводитсЯ процент правильности ответа на него (при условии 100% правильности).
		 „лЯ каждого ответа выводитсЯ процент выбора пользователЯми этого ответа.
		 Њодель результатов описана ниже.
		 */
		var questionsResultsPlugin =
		{
			name: 'questionsResults',
			/*
			 Њодель results:
			 results[group_id] =
			 {
			 group_id
			 questions[question_id]:
			 {
			 question_id
			 text - текст вопроса
			 type - тип вопроса
			 correct_rate - долЯ правильных ответов (100% правильность)
			 correct_answers - количество пользователей давших 100% правильный ответ
			 count - количество прошедших этот вопрос пользователей
			 answers[answer_id]:
			 {
			 answer_id
			 text - текст вопроса
			 correct - правильность (если type = 1 или 2)
			 rate - долЯ выбравших этот вопрос пользователей (count/question.count)
			 count - количество выбравших этот вопрос пользователей
			 }
			 }
			 }

			 */
			results: {},
			/*
			 Њетод инициализации вопроса группы (используетсЯ длЯ методов init и add)
			 */
			checkQuestion: function(questionData)
			{
				var result = null; //если группы нет в результатах, то вернуть ее номер в качестве результата функции
				var question = questionData;
				if(!this.results[question.group_id])
				{
					//если группы нет в результатах, то добавить новую
					this.results[question.group_id] = {
						group_id: question.group_id, questions:{}, position: question.position,
						count: 0, correct_answers: 0, correct_rate: 0
					};
					result = question.group_id;
				}
				if(!this.results[question.group_id].questions[question.question_id]) //если нет вопроса в списке, то добавить новый
				{
					this.results[question.group_id].questions[question.question_id] =
					{
						question_id: question.question_id,
						position: question.position,
						type: question.type,
						count: 0, correct_answers: 0, correct_rate: 0,
						answers: {},
					}
				}
				var questionObj = this.results[question.group_id].questions[question.question_id];//объект вопроса
				questionObj.count++;
				this.results[question.group_id].count++;
				if(question.correct_answers === question.all_correct_answers) //проверка правильности ответа на вопрос пользователем
				{
					questionObj.correct_answers++;
					this.results[question.group_id].correct_answers++;
				}
				this.results[question.group_id].correct_rate = this.results[question.group_id].correct_answers / this.results[question.group_id].count; //вычисление доли правильных вопросов в группе
				questionObj.correct_rate = questionObj.correct_answers/questionObj.count; //вычисление долЯ правильных ответов на вопрос
				for(var k in question.answers)
				{
					var answer = question.answers[k];
					var answerObj;
					if(question.type != 2)//если вопросы закрытого типа, то поместить их id в качестве ключа таблицы
					{
						if(!questionObj.answers[answer.answer_id])
							questionObj.answers[answer.answer_id] =
							{
								answer_id: answer.answer_id,
								rate: 0, count: 0,
								correct: answer.correct,
							}
						answerObj = questionObj.answers[answer.answer_id];
					}
					if(question.type == 2) //если вопросы открытого типа, то text в качестве ключа таблицы
					{
						if(!questionObj.answers[answer.text])
							questionObj.answers[answer.text] =
							{
								text: answer.text,
								rate: 0, count: 0,
							};
						answerObj = questionObj.answers[answer.text];
					}
					answerObj.count++;
				}
				for(var k in questionObj.answers)
				{
					var answerObj = questionObj.answers[k];
					answerObj.rate = answerObj.count/questionObj.count;
				}
				return result;
			},
			/*
			 Њетод загрузки групп вопросов (а также инициализации вопросов и ответов)
			 */
			loadGroups: function(groups)
			{
				var self = this;
				getGroups(groups,
					function(data)
					{
						if(!data.error_code)
						{
							for(var i in data)
							{
								var group = self.results[data[i].group_id];
								for(var j in data[i].questions)
								{
									var question = data[i].questions[j];
									if(!group.questions[question.question_id]) //если такого вопроса еще не было, то добавить
										group.questions[question.question_id] =
										{
											question_id: question.question_id,
											position: question.position,
											type: question.type,
											count: 0, correct_answers: 0, correct_rate: 0,
											answers: {},
										};
									group.questions[question.question_id].text = question.text; //добавить текст вопроса
									for(var k in question.answers)
									{
										var answer = question.answers[k];
										if(question.type != 2) //если вопрос закрытого типа, то ключ - answer.text
										{
											if(!group.questions[question.question_id].answers[answer.answer_id])
												group.questions[question.question_id].answers[answer.answer_id] =
												{
													answer_id: answer.answer_id,
													rate: 0, count: 0,
												};
											group.questions[question.question_id].answers[answer.answer_id].text = answer.text; //добавить полЯ текста вопроса
											group.questions[question.question_id].answers[answer.answer_id].correct = answer.correct; //...и правильности
										}
										else //если вопрос открытого типа
										{
											if(!group.questions[question.question_id].answers[answer.text])
												group.questions[question.question_id].answers[answer.text] =
												{
													text: answer.text,
													rate: 0, count: 0,
												};
											group.questions[question.question_id].answers[answer.text].correct = true;//добавить поле правильности
										}
									}
								}
							}
						}
						else
							throw new Error('err while loading groups');
					},
					function(error)
					{
						throw new Error('err while loading groups');
					});
			},
			init: function(data)
			{
				var groups_id = [];
				for(var i in data)
				{
					for(var j in data[i].questions)
					{
						var q = this.checkQuestion(data[i].questions[j]);
						if(q)
							groups_id.push(q); //добавить в список длЯ загрузки из кэша
					}
				}
				if(groups_id.length > 0) //загрузка содержимого вопросов и ответов
				{
					this.loadGroups(groups_id);
				}
			},
			add: function(data)
			{
				var groups_id = [];
				for(var i in data)
				{
					var q = this.checkQuestion(data[i]);
					if(q)
						groups_id.push(q);
				}
				if(groups_id.length > 0) //загрузка содержимого вопросов и ответов
				{
					this.loadGroups(groups_id);
				}
			},
			reset: function()
			{
				this.results = {};
			}
		};

		var quizResultsPlugin =
		{
			name: 'quizResults',
			/*
			 Њодель results:
			 results[question_id] =
			 {
			 text - текст ответа,
			 answers[answer_id] =
			 {
			 text - текст ответа
			 count - количество ответов
			 }
			 }
			 */
			results: {},
			init: function(data)
			{
				var self = this;
				var questions_id = [];
				for(var i in data)
					for(var j in data[i].questions)
					{
						var question = data[i].questions[j];
						questions_id.push(question.question_id);
						if(!this.results[question.question_id])
							this.results[question.question_id] = {question_id: question.question_id, answers:{}, count: 0};
						this.results[question.question_id].count += question.answers.length;
						for(var k in question.answers)
						{
							var answerId = question.answers[k].answer_id;
							if(!this.results[question.question_id].answers[answerId])
								this.results[question.question_id].answers[answerId] = {answer_id: answerId, count:0};
							this.results[question.question_id].answers[answerId].count++;
						}
					}
				if(questions_id.length > 0)
					getQuestions(questions_id,
						function(data)
						{
							if(!data.error_code)
							{
								for(var i in data)
								{
									var question = self.results[data[i].question_id];
									if(!self.results[data[i].question_id])
										continue;
									var question = self.results[data[i].question_id];
									question.text = data[i].text;
									for(var j in data[i].answers)
									{
										if(!question.answers[data[i].answers[j].answer_id])
											question.answers[data[i].answers[j].answer_id] = {answer_id: data[i].answers[j].answer_id, count:0};
										var answer = question.answers[data[i].answers[j].answer_id];
										answer.text = data[i].answers[j].text;
									}
								}
							}
							console.log(results);
						},
						function(error)
						{

						}
					);
			},
			add: function(data)
			{
				var questions_id = [];
				var self = this;
				for(var j in data)
				{
					var questionId = data[j].question_id;
					if(!this.results[questionId])
					{
						this.results[questionId] = {question_id: questionId, answers:{}, count: 0};
						questions_id.push(questionId);
					}
					var question = this.results[questionId];
					question.count += data[j].answers.length;
					for(var i in data[j].answers)
					{
						if(!question.answers[data[j].answers[i].answer_id])
							question.answers[data[j].answers[i].answer_id] = {answer_id: data[j].answers[i].answer_id,count: 0};
						question.answers[data[j].answers[i].answer_id].count++;
					}
				};
				if(questions_id.length > 0)
				{
					console.log(questions_id);
					getQuestions(questions_id,
						function(data)
						{
							if(!data.error_code)
							{
								for(var i in data)
								{
									var question = self.results[data[i].question_id];
									if(!self.results[data[i].question_id])
										continue;
									var question = self.results[data[i].question_id];
									question.text = data[i].text;
									for(var j in data[i].answers)
									{
										if(!question.answers[data[i].answers[j].answer_id])
											question.answers[data[i].answers[j].answer_id] = {answer_id: data[i].answers[j].answer_id,count:0};
										var answer = question.answers[data[i].answers[j].answer_id];
										answer.text = data[i].answers[j].text;
									}
								}
							}
							console.log(self.results);
						},
						function(error)
						{

						}
					);
				}
			},
			reset: function()
			{
				this.results = {};
			},
		};

		var results = {};

		var plugins = [testResultsPlugin, questionsResultsPlugin];//, quizResultsPlugin];//, questionsResultsPlugin];

		for(var i in plugins)
			results[plugins[i].name] = plugins[i].results;

		return(
		{
			results: results,
			init: function(data)
			{
				for(var i in plugins)
					plugins[i].init(data);
			},
			add: function(data)
			{
				for(var i in plugins)
					plugins[i].add(data);
			},
			reset: function()
			{
				for(var i in plugins)
				{
					plugins[i].reset();
					results[plugins[i].name] = plugins[i].results;
				}
				groupsCache = {};
				questionsCache = {};
			},
			getGroups: getGroups,
			getQuestions: getQuestions,
			getFullTest: function(testId)
			{
				if(!questionsResultsPlugin in plugins)
					return;
				getFullTest(testId,
					function(data)
					{
						var data = data.groups;
						for(var i in data)
						{
							if(!questionsResultsPlugin.results[data[i].group_id])
								questionsResultsPlugin.results[data[i].group_id] = {group_id: data[i].group_id, questions:{}, position: data[i].position,
									count: 0, correct_answers: 0, correct_rate: 0
								};
							for(var j in data[i].questions)
							{
								if(!questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id])
									questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id] =
									{
										question_id: data[i].questions[j].question_id,
										//text: data[i].questions[j].text,
										position: data[i].questions[j].position,
										type: data[i].questions[j].type,
										count: 0, correct_answers: 0, correct_rate: 0,
										answers: {},
									};
								questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].text = data[i].questions[j].text;


								for(var k in data[i].questions[j].answers)
								{
									var answer_id = data[i].questions[j].answers[k].answer_id;
									var answer_text = data[i].questions[j].answers[k].text;
									var correct = data[i].questions[j].answers[k].correct || data[i].questions[j].type == 2 && answer_text;
									if(data[i].questions[j].type == 0 || data[i].questions[j].type == 1)
									{
										if(!questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_id])
											questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_id] =
											{
												rate: 0, count: 0,
											};
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_id].correct = correct;
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_id].answer_id = answer_id;
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_id].text = answer_text;
									}
									if(data[i].questions[j].type == 2)
									{
										if(!questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_text])
											questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_text] =
											{
												rate: 0, count: 0,
											};
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_text].correct = correct;
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_text].answer_id = answer_id;
										questionsResultsPlugin.results[data[i].group_id].questions[data[i].questions[j].question_id].answers[answer_text].text = answer_text;
									}

								}

							}
						}

					},
					function()
					{
					});
			},
		});

		function getFullTest(testId, success, fail)
		{
			apiTests.getFullTest(auth.getUserToken(), testId).$promise.then(
				function(data)
				{
					if(!data.error_code)
					{
						if(success) success(data);
					}
					else
					if(fail) fail(data);
				},
				function(error)
				{
					if(fail) fail(error);
				}
			);
		}

		function getGroups(groupsId, success, fail)
		{
			var groups_id = [];
			var groups = [];
			if(typeof(groupsId) != 'object')
				var groupsId = [groupsId];
			for(var i in groupsId)
			{
				if(!groupsCache[groupsId[i]])
					groups_id.push(groupsId[i]);
				else
					groups.push(groupsCache[groupsId[i]]);
			}
			if(groups_id.length > 0)
			{
				apiTests.getGroups(auth.getUserToken(), groups_id).$promise.then(
					function(data)
					{
						for(var i in data)
						{
							groupsCache[data[i].group_id] = data[i];
							groups.push(data[i]);
							for(var j in data[i].questions)
							{
								var question = data[i].questions[j];
								questionsCache[question.question_id] = question;
							}
						}
						console.log('groups', groups);
						success(groups);
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			}
			else
				success(groups);
		}
		function getQuestions(questionsId, success, fail)
		{
			var questions_id = [];
			var questions = [];
			if(typeof(questionsId) != 'object')
				var questionsId = [questionsId];
			for(var i in questionsId)
			{
				if(!questionsCache[questionsId[i]])
					questions_id.push(questionsId[i]);
				else
					questions.push(questionsCache[questionsId[i]]);
			}
			if(questions_id.length > 0)
			{
				apiTests.getQuestions(auth.getUserToken(), questions_id).$promise.then(
					function(data)
					{
						for(var i in data)
						{
							questionsCache[data[i].question_id] = data[i];
							questions.push(data[i]);
						}
						success(questions);
					},
					function(error)
					{
						if(fail) fail(error);
					}
				);
			}
			else
				success(questions);
		}

	}]);