angular.module('room.adminRoom', ['restApi.room', 'userAuthService', 'longPoller']).
	factory('AdminRoom', ['RestApiRoom', 'UserAuthService', 'LongPoller',
		function(RestApiRoom, UserAuthService, LongPoller)
		{
		
			var apiRoom = RestApiRoom;
			var auth = UserAuthService;
			//������ ������������ �������������
			var users = new List();/*
							{
								workingUserId
								firstName
								secondName
								username
							}
							*/
			//������� ����
			var currentTest =
			{
				streamId: null,
				test: null,
				position: null, //������� � ������� queue
			};
			//������� ������
			var queue = new List();
			/*
						��. api/tests/getInfo
						{
						test_id
						owner_id - 1
						name -1
						type - 1
						questions - 1
						timer - 1
						creation_time - 1
						editing_time - 1
						+
						overdrive:
							{
								type - 1,
								timer - 1
							}
						}
						*/
			
			//���������� ������������, ����������� ������������� ������
			var testResults = [];
			
			//"���������" �������
			var config = 
			{
				roomId: null,
				status: null, //������ �������
				access: null, //����� �������
				password: null, //������
				url: null //��������� ������������� �������
			};
			
			var time = 
			{
				addTime: null, //����� ���������� �����: � ������ ��� ����������� - ���������, ����� - 0
				currentTime: null, //������� ����� ����������� �����: ���������� addTime
				timer: null, //����� ���������� ��� ����������� �����
				time: null
			};
			
			var testState = null; /*
								active - ���� ������������
								ready - ���� ����� � �������
								cancelled - ���� �������
								finished - ���� ��������
								*/
			
			var timerId;
			
			//LP ������
			//���������� ������� �������
			var waitEvent = LongPoller();
		//	waitEvent.limit = 2;
			//������� �� ���������
			var waitStudentEvent = LongPoller();
			//waitStudentEvent.limit = 2;
			//������� ����������� ������������
			var waitResults = LongPoller();
			//waitResults.limit = 2;
			
			var room = 
			{
				init: function(success, fail)
				{
					var self = this;
					apiRoom.get(auth.getUserToken()).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							config.roomId = data.room_id;
							config.status = data.status;
							config.access = data.access;
							config.password = data.password;
							config.url = data.url;
							if(config.status == true)
							{
								self.getCurrentState(
									function()
									{
										if(success)
											success();
									},
									function()
									{
										if(fail)
											fail();
									});
							}
							else
								if(success)
									success();
						}
						else
						{
							//some code...
							if(fail)
								fail(data);
						}
					},
					function(error)
					{
						//some code
						if(fail)
							fail(error);
					}
					);
				},
				/*putIntoRoom: function(test, success, fail) 
				{
					/*test:
					{
						test_id
						position - optional
						overdrive:
						{
							type
							timer
						}
					}
					
					apiTests.putIntoRoom(auth.getUserToken(), test_id, position).$promise.then(
					function(data)
					{
						if(!data.error_code)
						{
							
							currentTest.test = test;
							currentTest.streamId = data.streamId;
							success();
						}
						else
						{
							fail(data);
						}
					},
					function(error)
					{
						fail(error);
					});
				},*/
				resume: function()
				{
					if(queue.list.length > 0)
					{
						apiRoom.resume(auth.getUserToken()); //���������� ����������� ������ � �������
					}
					else
					{
						testState = null;
						currentTest.test = null;
					}
				},
				startTest: function(passive)
				{
					//������ (��� �����������) ����� � �������
					testState = 'active';
					//������ �������
					startTimer();
					this.events.test.onActivated();
				},
				cancelTest: function(passive)
				{
					//��� ��������� ������� �������� �������� �� ��, ��� ���� ��� ����� ���� �����
					//����� ������� ������� ������ ������������ � ���� ���������: ��� �������� � ��������� ��������
					
					//�������� ���������� �������� �����
					testState = 'cancelled';
					stopTimer();
					if(!passive)
						apiRoom.killTest(auth.getUserToken());
					this.events.test.onCancelled();
				},
				finishTest: function()
				{
					testState = 'finished';
					this.events.test.onFinished();
				},
				killStudent: function(workingUserId, passive)
				{
					var user = users.remove(function(item){return item.testId}, workingUserId);
					//������� �������� �� �������
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.user.onKilled(user);
				},
				
				//�������� ����������� ������ � ������!
				
				putIntoRoom: function(test, pos, passive) //obj {test, position}
				{
					var self = this;
					//���������� ����� � ������� (��� �������)
					queue.addTo(test, pos);
					//queue.push(test);
					if(!passive)
					{
						apiRoom.putIntoRoom(auth.getUserToken(), test.test_id, pos).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								
								currentTest.test = test;
								currentTest.streamId = data.streamId;
								self.events.queue.onAdded(test);
							}
							else
							{
								queue.removeFrom(pos);
								//fail(data);
							}
						},
						function(error)
						{
							queue.removeFrom(pos);
							//fail(error);
						});
					}
					else
						this.events.queue.onAdded(test);
				},
				removeFromRoom: function(pos, testId, passive)
				{
					var self = this;
					//�������� ����� �� �������
					var test = queue.removeFrom(pos);
					if(queue.list.length == 0)
					{
						testState = null;
						currentTest.test = null;
					}
					if(!passive)
					{
						apiRoom.removeFromRoom(auth.getUserToken(), pos, testId).$promise.then(
						function(data)
						{
							if(!data.error)
							{
								self.events.queue.onRemoved(test);
							}
							else
							{
								queue.addTo(test, pos);
							}
						},
						function(error)
						{
							queue.addTo(test, pos);
						});
					}
					else
						self.events.queue.onRemoved(test);
				},
				initChannels: function()
				{
					if(waitEvent)
						waitEvent.poll(function(){return apiRoom.waitEvent(auth.getUserToken())}, eventReceived, errorEvent);
					if(waitStudentEvent)
						waitStudentEvent.poll(function() {return apiRoom.waitStudentEvent(auth.getUserToken())}, studentEventReceived, errorStudentEvent);
					if(waitResults)
						waitResults.poll(function(){return apiRoom.waitResults(auth.getUserToken(), currentTest.streamId)}, resultsReceived, resultsError);
				},
				changeStatus: function(val, passive)
				{
					var self = this;
					if(config.status == val)
						return;
					if(val == false)
					{
						if(testState == 'active')
						{
							//���� ������ ����� � �� ������!
							apiRoom.killTest(auth.getUserToken(), test.streamId);
							stopTimer();
						}
						testState = null;
					}
					else
					{
						getCurrentState();
					}
					config.status = val;
					if(!passive)
					{
						apiRoom.setStatus(auth.getUserToken(), val).$promise.then(
						function(data)
						{
							if(!data.error)
								self.events.room.onChangedStatus();
							else
								config.status = !val;
						},
						function(error)
						{
							config.status = !val;
						});
					}
					else
						self.events.room.onChangedStatus();
				},
				changeAccess: function(obj, passive)
				{
					config.access = obj.access;
					if(obj.access == 2)
						config.password = obj.password;
					if(!passive)
					{
						//��������� ������ �� ������
					}
					this.events.room.onChangedAccess();
				},
				
				getCurrentState: function(success, fail)
				{
					var self = this;
					apiRoom.getCurrentState(auth.getUserToken()).$promise.then(
						function(data)
						{
							if(!data.error_code)
							{
								users.list = data.users;
										queue.list = data.tests;
										testState = data.test_state;
										if(queue.list.length > 0)
										{
											currentTest.test = queue.list[0];
											for(var i in data.users)
											{
												testResults[i.username] = i.questions;
											}
											if(data.test_state) //active, cancelled, finished
											{
												currentTest.streamId = data.stream_id;
												time.addTime = data.add_time;
												time.currentTime = data.current_time;
												time.timer = currentTest.test.timer;
												if(data.test_state == 'active')
													self.startTest();
											}
											self.initChannels();
										}
										if(success)
											success();
									}
									else
									{
										//������ ��������
										if(fail)
											fail(data);
									}
									
								},
								function(error)
								{
									//err while loading room state
									if(fail)
										fail();
								}								
								);
				},
				
				//callback-� ��� �������
				events: 
				{
					test:
					{
						onActivated: function()
						{
						},
						onCancelled: function()
						{
							
						},
						onFinished: function()
						{
						},
					},
					queue:
					{
						onAdded: function(test)
						{
						},
						onRemoved: function(test)
						{
						}
					},
					room:
					{
						onChangedAccess: function()
						{
						},
						onChangedStatus: function()
						{
						},
					},
					users:
					{
						onLogged: function(user) //user ����� � �������
						{
							//Callback �������. ����� � ����� ��� �������� ���������� � ������������ � ���� ������������. 
						},
						onLeft: function(user)
						{
							
						},
						onKilled: function(user)
						{
						},
						onLost: function(user)
						{
						
						},
						onFinished: function(user)
						{
						}
					},
					timer:
					{
						onTick: function()
						{
						},
						onStart: function()
						{
						},
						onStop: function()
						{
						}
					}
				}
			};
						
			function eventReceived(data)
			{
				switch(data.type)
				{
					case 'test.activated':
						if(testState == 'cancelled' || testState == 'finished')
							queue.removeFrom(0);
						currentTest.test = queue.list[0];
						currentTest.sessionId = data.session_id;
						time.addTime = 0;
						time.currentTime = 0;
						room.startTest('passive');
						break;
					case 'test.cancelled':
						//���� ������� ��������������
						room.cancelTest('passive');
						break;
					case 'test.finished':
						room.finishTest();
					case 'queue.removed':
						//���� ����� �� �������
						room.removeFromRoom(data.position, data.test_id, 'passive');
						break;
					case 'queue.added':
						//���� �������� � �������
						room.putIntoRoom(data.test, data.position, 'passive');
						break;
					case 'ping':
						break;
					case 'room.access':
						//������� ����� �������
						room.changeAccess(data, 'passive');
						break;
					case 'room.status':
						//������� ������ �������
						room.changeStatus(data.status, 'passive');
						break;
				}
			}
			
			function errorEvent()
			{
				//��������� ������ ������ waitEvent
			}
			
			function studentEventReceived(data)
			{
				switch(data.type)
				{
					case 'logged':
						//������� ����� � �������
						var user = 
						{
							workingUserId: data.workingUser_id,
							firstName: data.first_name,
							secondName: data.second_name,
							username: data.username
						};
						users.push(user);
						room.events.users.onLogged(user);
						break;
					case 'left':
						//������� �������
						var user = users.remove(function(item){return item.testId}, data.test_id);
						room.events.users.onLeft(user);
						break;
					case 'killed':
						//�������� �� ������� ��������������
						room.killStudent(data.test_id,'passive');
						break;
					case 'lost':
						//�������� �����
						var user = users.remove(function(item){return item.testId}, data.test_id);
						room.events.users.onRemoved(user);
						break;
					case 'finished':
						//�������� ����������� �����
						var user = users.find(function(i){return i.workingUserId}, data.workingUser_id);
						room.events.users.onFinished(user);
						break;
				}
			}
			
			function errorStudentEvent()
			{
				//��������� ������ ������ waitStudentEvent
			}
			
			function resultsReceived(data)
			{
				testResults[String(data.workingUser_id)][String(data.question_id)] =
				{
					answers: data.answers,
					correctAnswer: data.correct_answer
				};
			}
			
			function resultsError()
			{
				
			}
			
			function startTimer()
			{
				if(timerId)
					return;
				room.events.timer.onStart();
				time.time = time.currentTime - time.addTime;
				var obj = null;
				if(queue.list[0].override && queue.list[0].override.timer)
					obj = queue.list[0].override;
				else
				if(queue.list[0].timer)
					obj = queue.list[0];
				timerId = setInterval(function()
				{
					time.time++;
					room.events.timer.onTick();
					if(obj)
						if(time.time > obj.timer)
						{
							//����� ���� ��������
							//�������� ��������� �� ��������� �� �������
							stopTimer();
						}
				}, 1000);
			};
			
			
			function stopTimer()
			{
				room.events.timer.onStop();
				clearInterval(timerId);
			};
			
			room.__defineGetter__('users', function()
			{
				return users;
			});
			room.__defineGetter__('currentTest', function()
			{
				return currentTest;
			});
			room.__defineGetter__('queue', function()
			{
				return queue;
			});
			room.__defineGetter__('testResults', function()
			{
				return testResults;
			});
			room.__defineGetter__('config', function()
			{
				return config;
			});
			room.__defineGetter__('time', function()
			{
				return time;
			});
			room.__defineGetter__('testState', function()
			{
				return testState;
			});
			return room;
			
			function List()
			{
				this.list = [];
				this.removeFrom = function(position)
				{
					//this.list[position] = null;
					var item = this.list[position];
					this.list = this.list.slice(0, position).concat(this.list.slice(position+1));
					console.log(position);
					console.log(this.list);
					return item;
				};
				this.addTo = function(obj, position)
				{
					var tmp = this.list.slice(0, position);
					tmp.push(obj);
					tmp = tmp.concat(this.list.slice(position));
					this.list = tmp;
					console.log('!');
					console.log(this.list);
				};
				this.push = function(obj)
				{
					this.list.push(obj);
				};
				this.remove = function(func, value)
				{
					for(i in this.list)
						if(func(this.list[i]) == value)
						{
							return this.removeFrom(i);
						}
							
				};
				this.find = function(key, value)
				{
					for(i in this.list)
						if(func(this.list[i]) == value)
							return this.list[i];
				}
			}
			
		}]);