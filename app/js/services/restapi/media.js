angular.module('restApi.media',['ngResource','settings']).
	factory('RestApiMedia',['$resource','Settings', 
	function($resource, Settings)
	{
		var devapi = 'http://api.iquiz.win';
		var resource = $resource(Settings.imagesApiURL+'/:method'); //api media
		var uploadImageRes = $resource(Settings.imagesApiURL+'/upload/', {}, //�������� �����������
		{
			upload:
			{
			  method: 'POST',
			  //headers: {enctype:'multipart/form-data'}
			  headers: {'Content-Type': undefined},
			  transformRequest: angular.identity,
			},
		}); 
		var service = {
			uploadImage: function(token, file)
			{
				var fd = new FormData();
				fd.append('file', file);
				return uploadImageRes.upload({token: token}, fd);
			},
			getImagesList: function(token, offset, limit)
			{
				return resource.get({method: '', token: token, offset: offset, limit: limit});
			},
		};
		return service;
	}]);