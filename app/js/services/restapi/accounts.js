/**
			@namespace "restApi.accounts"
			@description Модуль для реализации взаимодействия с API сервера api/accounts
		*/
angular.module('restApi.accounts',['ngResource','settings']).
	factory('RestApiAccounts', ['$resource','Settings',
	/**
	@class "restApi.accounts".RestApiAccounts
	
	@description Сервис предоставлющий интерфейс API сервера
	*/
	function($resource, Settings)
	{
		var resource = $resource(Settings.baseURL+'/account/:url'); //api аккаунтов
		var signInRes = $resource(Settings.baseURL+'/account/signin/:username:password');
		var signUpRes = $resource(Settings.baseURL+'/account/signup');
		var signUpByPromoRes = $resource(Settings.baseURL+'/account/signupByPromo');
		var signOutRes = $resource(Settings.baseURL+'/account/signout');
		var saveProfileRes = $resource(Settings.baseURL+'/account/saveProfileInfo');
		var changePasswordRes = $resource(Settings.baseURL+'/account/changePassword');
		var resetPasswordRes = $resource(Settings.baseURL+'/account/resetPassword');
		return(
		{
				/**
				Авторизация в системе
				@function "restApi.accounts".RestApiAccounts.signIn
				@param {Number} type тип пользователя: 1 - админ (препод); 2 - студент
				@param {String} username логин пользователя
				@param {String} password пароль пользователя
				*/
				signIn: function(type, username, password) //авторизация в СИСТЕМЕ (для комнат см. api rooms)
				{
					//JSON Style
					//return signInRes.save({username: username, password: password}); 
					//Normal (?) style
					//return signInRes.post({username: username, password: password});
					return signInRes.save('username='+username+'&password='+password);
				},
				/**
				Регистрация (в разработке ...)
				@function "restApi.accounts".RestApiAccounts.signUp
				@param {String} username логин
				@param {String} password пароль
						
				*/
				signUp: function(username, password) //регистрация
				{
					return signUpRes.save('username='+username+'&password='+password);
				},
				signUpByPromo: function(username, password, first_name, second_name, promo)
				{
					return signUpByPromoRes.save('username='+username+'&password='+password+'&promo='+promo
						+'&first_name='+first_name+'&second_name='+second_name);
				},
				/**
				Выход из системы (в разработке)
				@function "restApi.accounts".RestApiAccounts.signUp
				@param {String} token токен доступа в систему
				*/
				signOut: function(token)
				{
					//return signOutRes.save({token: token});
					return signOutRes.save('token='+token);
				},
				/**
				Получить данные пользователя (в разработке)
				@function "restApi.accounts".RestApiAccounts.get
				@param {Number} userId идентификатор пользователя в системе
				@param {String} token токен доступа в систему
				*/
				get: function(userId, token)
				{
					return resource.get({url: 'get', user_id: userId, token: token});
				},
				/**
				Сохранить данные пользователя (в разработке)
				@function "restApi.accounts".RestApiAccounts.saveProfileInfo
				@param {Object} object объект с данными
				@param {Number} object.userId идентификатор пользователя
				@param {String} object.token токен доступа
				@param {String} object.firstName имя пользователя
				@param {String} object.secondName фамилия пользователя
				*/
				saveProfileInfo: function(token, object) //см. описание api
				{
					/*var tmp = {};
					if(object.firstName) tmp.first_name = object.firstName;
					if(object.secondName) tmp.second_name = object.secondName;
					return saveProfileRes.save(tmp);*/
					var tmp = 'token='+token;
					if(object.firstName) tmp += '&first_name='+object.firstName+'&';
					if(object.secondName != null) tmp += 'second_name='+object.secondName;
					return saveProfileRes.save(tmp);
				},
				changePassword: function(token, currentPass, newPass)
				{
					return changePasswordRes.save('token='+token+'&current='+currentPass+'&new='+newPass);
				},
			//восстановление пароля
				resetPassword: function(username)
				{
					return resetPasswordRes.save('username='+username);
				}
			
		}
		);
	}]);