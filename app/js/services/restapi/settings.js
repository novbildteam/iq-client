/**
@namespace settings
@description Модуль настроек приложения
*/

angular.module('settings',[]).
	factory('Settings',
	/**
	@class settings.Settings
	@description Сервис настройки приложения
	*/
	function()
	{
		/**
		@name settings.Settings.STATE
		@description "Состояние" приложения
		@readonly
		@prop {String} dev в разработке
		@prop {String} production продашкн
		*/
		var STATE = {
			/**
			В разработке */
			dev: 'dev',
			/** Продакшн */
			production: 'prod'
		};
		/**
		@name settings.Settings.settings
		@description Настройки приложения
		@prop {String} appState состояние приложения см. {@link settings.Settings.STATE}
		@prop {String} baseURL ссылка на API
		*/
		/*Не работает в strict mode!!! http://stackoverflow.com/questions/22603078/syntaxerror-use-of-const-in-strict-mode*/
		var ERRCODE = 
		{
			WRONG_GROUP: 1, //Неверная группа запроса
			WRONG_METH: 2, //Неверный метод запроса
			WRONG_TYPE: 3, //Неверный тип запроса
			NOT_ARGS: 4, //Не все требуемые параметры
			WRONG_VAL: 5, //Неверное значение параметра
			TOKEN_OVER: 6, //Время жизни токена истекло
			FORBIDDEN: 7, //Доступ запрещен
			WRONG_TOKEN: 8, //Неверный токен
			WRONG_STREAM: 9, //неверный id потока??
			WRONG_DIR: 10, //неверный id директории??
			MISS_METH: 1010, //метод не реализован
			ACC: 
			{
				WRONG_EMAIL: 101, //Неверный логин
				LOGIN_EXIST: 103, //Пользователь с таким логином уже существует
				WRONG_LOGIN: 104, //неверный логин
				WRONG_PASS: 105, //скорее всего будет убрано!
				WRONG_USERID: 106, //Неверный идентификатор пользователя
			},
			ROOMS:
			{
				NOT_FOUND: 201, //Комната не найдена
				WRONG_PASS: 202, //Неверный пароль комнаты
				WRONG_USERID: 204, //неверный id раб.пользователя
				NOT_AUTH_IN_ROOM: 205, //не авторизован в комнате
			}
		}
		
		var settings = {
		/**
		Состояние API приложения
		*/
			apiState: STATE.production, //dev, prod
		/**
		Ссылка на API
		*/
			host: 'https://instaquiz.ru',
			baseURL: 'https://devapi.instaquiz.ru',
			reportApiURL: 'https://devapi.instaquiz.ru/reports',
			imagesApiURL: 'https://devapi.instaquiz.ru/images',
		/**
		Коды ошибок
		*/
			ERRCODE: ERRCODE
		};
		var host = window.location.hostname;
		//проверка, локальная версия?
		if(host == 'localhost' || host == '127.0.0.1')
		{
			settings.baseURL = 'http://localhost:15000';
			settings.reportApiURL = 'http://localhost:15000/reports';
			settings.imagesApiURL = 'http://localhost:15000/images';
			//settings.reportApiURL = 'http://127.0.0.1:8001/reports';
			// settings.imagesApiURL = 'http://127.0.0.1:8001/images';
		}
		return settings;
	}
	);