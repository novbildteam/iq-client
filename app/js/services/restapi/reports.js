angular.module('restApi.reports',['ngResource','settings']).
	factory('RestApiReports',['$resource','Settings', '$http',
		function($resource, Settings, $http)
		{
			// var resource = $resource(Settings.baseURL+'/reports/:method/:ids');
			// var lessonsStreamsRes = $resource(Settings.baseURL+'/reports/lessons/:ids/streams');
			// var lessonsUsersRes = $resource(Settings.baseURL+'/reports/lessons/:ids/users');
			var resource = $resource(Settings.reportApiURL+'/:method/:ids/');
			var resource_old = $resource(Settings.baseURL+'/reports/:method/:ids/');
			var lessonsStreamsRes = $resource(Settings.reportApiURL+'/lessons/:ids/streams/');
			var lessonsUsersRes = $resource(Settings.reportApiURL+'/lessons/:ids/users/');
			var streamsResultsRes = $resource(Settings.reportApiURL+'/streams/:ids/users_results/');
			var streamsQuestionsResultsRes = $resource(Settings.reportApiURL+'/streams/:ids/questions_results');
			var downloadStreamRes = $resource(Settings.reportApiURL+'/streams/:id/download', null, {
				'get_xlsx': {
					method: 'GET',
					responseType: "arraybuffer"
				}
			});
			

			return(
			{
				getLastStreams: function(token, offset, limit)
				{
					//return resource_old.get({method: 'getLastStreams', token: token, offset: offset, limit: limit});
					return resource.get({method: 'streams', token: token, offset: offset, limit: limit, sort:'-lesson_id'});
				},
				getStream: function(token, id)
				{
					return resource.get({method: 'streams', token: token, ids:id})
				},
				// downloadStream: function(token, id, type)
				// {
				// 	if(!type)
				// 		type = 'xlsx'; // default format
				// 	if(type == 'xlsx')
				// 		return downloadStreamRes.get_xlsx({id: id, token: token, type: type});
				// },
				downloadStream: function(token, id, type)
				{
					var url = this.streamDownloadLink(token, id, type);
					return $http.get(url, {responseType: "arraybuffer"})
				},
				streamDownloadLink: function(token, id, type)
				{
					var link = Settings.host+'/download/reports/streams/'+id+'?token='+token;
					if(!type)
						type = 'xlsx';
					link += '&type='+type;
					return link;
				},
				getStreams: function(token, ids, offset, limit, sort, filter)
				{
					if(ids != null)
						if(typeof(ids) != 'number')
							ids = ids.join(',');
					return resource.get({method: 'streams', ids: ids, token: token, offset: offset, limit: limit, filter:filter, sort:sort});
				},
				getStreamsResults: function(token, ids)
				{
					//return resource_old.get({method: 'getStreamsResult', token: token, streams: {'streams': streams}});
					if(ids == null)
						throw new Error('ids is not defined in getStreamsResults');
					if(typeof(ids) != 'number')
						ids = ids.join(',');
					return streamsResultsRes.get({ids: ids,  token: token,});
				},
				getStreamsQuestionsResults: function(token, ids)
				{
					if(ids == null)
						throw new Error('ids is not defined in getStreamsQuestionsResults');
					if(typeof(ids) != 'number')
						ids = ids.join(',');
					return streamsQuestionsResultsRes.get({ids: ids, token: token})
				},
				getLessons: function(token, ids, params)
				{
					return resource.get({method: 'lessons', token: token, ids: ids,
						offset: params.offset, limit: params.limit, sort: params.sort, filter: params.filter
					});
				},
				getLessonsStreams: function(token, ids, params)
				{
					if(ids == null)
						throw new Error('ids is not defined in getLessonsStreams');
					if(typeof(ids) != 'number')
						ids = ids.join(',');
					return lessonsStreamsRes.get({ids: ids, token: token,
						offset: params.offset, limit: params.limit, sort: params.sort, filter: params.filter
					});				
				},
				getLessonsUsers: function(token, ids, params)
				{
					if(ids == null)
						throw new Error('ids is not defined in getLessonsUsers');
					if(typeof(ids) != 'number')
						ids = ids.join(',');
					return lessonsUsersRes.get({ids: ids, token: token,
						offset: params.offset, limit: params.limit, sort: params.sort, filter: params.filter
					})
				}
			});
			
		}]);