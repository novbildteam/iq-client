angular.module('restApi.fs',['ngResource','settings']).
	factory('RestApiFs',['$resource', 'Settings',
		function($resource, Settings)
		{
			var openRes = $resource(Settings.baseURL+'/fs/open');
			var addRes = $resource(Settings.baseURL+'/fs/mkdir');
			var deleteRes = $resource(Settings.baseURL+'/fs/delete');
			var moveRes = $resource(Settings.baseURL+'/fs/move');
			var renameRes = $resource(Settings.baseURL+'/fs/rename');
			var copyRes = $resource(Settings.baseURL+'/fs/copy');
			var shareFolderRes = $resource(Settings.baseURL+'/fs/shareFolder');
			var shareTestRes = $resource(Settings.baseURL+'/fs/shareTest');
			
			return(
			{
				open: function(token, folderId, needPath)
				{
					return openRes.get({token: token, folder_id: folderId, need_path: needPath});
				},
				makeDir: function(token, name, folderId)
				{
					/*obj = {token: token, name: name, folder_id: folderId};
					return addRes.save(obj);*/
					var tmp = 'token='+token+'&name='+name+'&folder_id='+folderId;
					return addRes.save(tmp);
					
				},
				delete: function(token, itemId)
				{
					//return deleteRes.save({token: token, item_id: itemId});
					return deleteRes.save('token='+token+'&item_id='+itemId);
				},
				move: function(token, itemId, folderId)
				{
					//return moveRes.save({token: token, item_id: itemId, folder_id: folderId});
					var tmp = 'token='+token+'&item_id='+itemId+'&folder_id='+folderId;
					return moveRes.save(tmp);
				},
				rename: function(token, itemId, name)
				{
					//return renameRes.save({token: token, item_id: itemId, name: name});
					var tmp = 'token='+token+'&item_id='+itemId+'&name='+name;
					return renameRes.save(tmp);
				},
				copy: function(token, item_id)
				{
					//return copyRes.save({token: token, item_id: item_id});
					return copyRes.save('token='+token+'&item_id='+item_id);
				},
				shareFolder: function(token, folderId, userId, type)
				{
					//return shareFolderRes.save({token: token, folder_id: folderId, user_id: userId, type: type});
					var tmp = 'token='+token+'&folder_id='+folderId+'&user_id='+userId+'&type='+type;
					return shareFolderRes.save(tmp);
				},
				shareTest: function(token, testId, userId, type)
				{
					//return shareTestRes.save({token: token, folder_id: testId, user_id: userId, type: type});
					var tmp = 'token='+token+'&test_id='+folderId+'&user_id='+userId+'&type='+type;
					return shareTestRes.save(tmp); 
				}
			}
			);
			
		}]);