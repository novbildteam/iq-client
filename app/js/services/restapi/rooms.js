/**
 @namespace "restApi.room"
 @description модуль для реализации сервиса взаимодействия с API комнат api/room
 */
angular.module('restApi.room',['ngResource','settings']).
factory('RestApiRoom',['$resource','Settings',
	/**
	 @class "restApi.room".RestApiRoom
	 @description Сервис предоставляющий интерфейс API сервера api/rooms
	 */
		function($resource, Settings)
	{
		var signInRes = $resource(Settings.baseURL+'/rooms/signin'); //для post запроса signIn
		var signOutRes = $resource(Settings.baseURL+'/rooms/signout'); //для post запроса signOut
		var removeFromRoomRes = $resource(Settings.baseURL+'/rooms/removeFromRoom');
		var statusRes = $resource(Settings.baseURL+'/rooms/setStatus');
		var putIntoRoomRes = $resource(Settings.baseURL+'/rooms/putIntoRoom');
		var killTestRes = $resource(Settings.baseURL+'/rooms/killTest');
		//var finishTestRes = $resource(Settings.baseURL+'/rooms/finishTest');
		var overrideTestRes = $resource(Settings.baseURL+'/rooms/overrideTest');
		var createActivityRes = $resource(Settings.baseURL+'/rooms/createActivity');
		var resumeRes = $resource(Settings.baseURL+'/rooms/resume');
		var killStudentRes = $resource(Settings.baseURL+'/rooms/killStudent');
		var sendAnswerRes = $resource(Settings.baseURL+'/rooms/sendAnswer');
		var saveRoomInfoRes = $resource(Settings.baseURL+'/rooms/saveRoomInfo');
		var enableNextQuestionRes = $resource(Settings.baseURL+'/rooms/enableNextQuestion');
		var finishQuestionRes = $resource(Settings.baseURL+'/rooms/finishQuestion');
		var startLessonRes = $resource(Settings.baseURL+'/rooms/startLesson');
		var endLessonRes = $resource(Settings.baseURL+'/rooms/endLesson');
		var resource = $resource(Settings.baseURL+'/rooms/:method'); //api комнат
		return(
		{

			/**
			 Авторизация в комнате
			 @function "restApi.room".RestApiRoom.signIn
			 @param {Object} object объект авторизации. Описание объекта авторизации смотри в {@link UserAuthService~logInRoom}
			 */
			signIn: function(object)
			{
				console.log(object);
				if(object.token)
					if(object.roomPassword)
						return signInRes.save('room_id='+object.roomId+'&token='+object.token+'&room_pass='+object.roomPassword);
					/*{room_id:object.roomId,
					 token: object.token,
					 room_pass: object.roomPassword
					 });*/ //авторизованный запрос с паролем комнаты
					else
						return signInRes.save('room_id='+object.roomId+'&token='+object.token);
				/*return signInRes.save({
				 room_id:object.roomId,
				 token: object.token
				 }); //авторизованный запрос без пароля к комнате*/
				else
				if(object.access == 0)
					if(object.roomPassword)
						return signInRes.save('room_id='+object.roomId+'&room_pass='+object.roomPassword);
					/*return signInRes.save(
					 {
					 username: object.username,
					 room_id: object.roomId,
					 room_pass: object.roomPassword
					 }); //анонимный вход с паролем комнаты*/
					else
						return signInRes.save('room_id='+object.roomId);
				/*	return signInRes.save(
				 {
				 room_id: object.roomId,
				 username: object.username
				 }); //анонимный вход*/
				else
				if(object.access == 1)
				{
					var tmp = 'room_id='+object.roomId;
					tmp += '&first_name='+object.firstName;
					if(object.secondName)
						tmp += '&second_name='+object.secondName;
					if(object.roomPassword)
						tmp += '&room_pass='+object.roomPassword;
					return signInRes.save(tmp);
				}

				/*return signInRes.save(
				 {
				 room_id: object.roomId,
				 username: object.username,
				 });*/
				//неанонимный вход без пароля комнаты
				/*else
				 return signInRes.save({url:'signin'});*/
				//ситуациЯ object.access == 2 не обрабатывается здесь
				//если необходимо войти в комнату авторизованным, то сначала производится авторизация
				//методом account/signin, а затем вход в комнату по полученному токену
				/*
				 Структура object
				 roomId - номер комнаты
				 token - токен пользователя в системе
				 username - имя пользователя (для авторизации с именем,
				 при анонимной авторизации генерируется на клиенте)
				 roomPass - пароль к комнате
				 */
			},
			/**
			 Выход из комнаты
			 @function "restApi.room".RestApiRoom.signOut
			 @param {String} token токен авторизации
			 */
			signOut: function(token, roomId)
			{
				/*
				 используется метод accounts/signout
				 */
				//return signOutRes.save({token: token, room_id: roomId});
				return signOutRes.save('token='+token+'&room_id='+roomId);
			},
			/**
			 @function "restApi.room".RestApiRoom.get
			 @param {Number} userId id пользователи
			 @param {String} token токен пользователя
			 Уточняется ...
			 */
			get: function(token)
			{
				return resource.get({method:'get', token: token});
			},
			/**
			 Получение данных комнаты по url-идентификатору (!= идентификатору комнаты)
			 @function "restApi.room".RestApiRoom.getByUrl
			 @param {String} id - url-идентификатор
			 */
			getByUrl: function(id)
			{
				return resource.get({method:'getByUrl',url:id});
			},
			/**
			 Сохранить данные комнаты
			 @function "restApi.room".RestApiRoom.saveRoomInfo
			 @param {String} token токен доступа
			 @param {Number} userId идентификатор пользователя
			 @param {Number} access режим доступа к комнат
			 @param {String} password пароль пользователя
			 */
			saveRoomInfo: function(token, obj)
			{
				/*
				 Структура object:
				 access - 0,1,2 - тип доступа к комнате
				 password - пароль к комнате
				 showResults - boolean - демонстрировать ли результаты тестирования студентам
				 */
				var req = 'token='+token;
				if(obj.access != null)
					req += '&access='+obj.access;
				if(obj.password != null)
					req += '&password='+obj.password;
				if(obj.showResults != null)
					req += '&show_results='+obj.showResults;
				return saveRoomInfoRes.save(req);
			},
			/**
			 Установка статуса комнаты (активна/неактивна)
			 @function "restApi.room".RestApiRoom.setStatus
			 @param {String} token токен доступа
			 @param {Number} status статус
			 */
			setStatus: function(token, status)
			{
				return statusRes.save('token='+token+'&status='+status);
			},
			/**
			 Список студентов
			 @function "restApi.room".RestApiRoom.getStudents
			 @param {String} token токен доступа
			 */
			getStudents: function(token)
			{

			},

			getCurrentState: function(token, type, roomId, deviceId)
			{
				var obj = {method:'getCurrentState', token:token, type: type, room_id: roomId};
				if(deviceId)
					obj.device_id = deviceId;
				return resource.get(obj);
			},
			/**
			 Ожидание событий для студентов (long poll)
			 @function "restApi.room".RestApiRoom.waitStudent
			 @param {String} token токен доступа
			 @param {Number} userId идентификатор пользователя (нужен ли)?

			 waitStudentEvent: function(token)
			 {
                 return resource.get({url: 'waitStudentEvent', token: token});
             },*/
			/**
			 Бан студента
			 @function "restApi.room".RestApiRoom.killStudent
			 @param {String} token токен доступа
			 @param {Number} workingUserId идентификатор доступа
			 */
			killStudent: function(token, workingUserId)
			{
				return killStudentRes.save('token='+token+'&workingUser_id='+workingUserId);
			},
			/**
			 Ожидание теста (long poll)
			 @function "restApi.room".RestApiRoom.waitTest
			 @param {String} token токен доступа
			 @param {Number} userId идентификатор пользователя
			 */
			waitStudentEvent: function(token, roomId, hash)
			{
				//return resource.get({url:'getByUrl',id:1});
				/*var resource = $resource(Settings.baseURL+'/fakedata/rooms/:url');
				 return resource.get({url:'waitTest.json'});*/
				return resource.get({method:'waitStudentEvent', token: token, room_id: roomId, hash: hash});

			},

			removeFromRoom: function(token, position, test_id)
			{
				var tmp = 'token='+token+'&position='+position+'&test_id='+test_id;
				return removeFromRoomRes.save(tmp);
			},
			/**
			 Создание "активности" в комнате
			 * @param token {String} токен доступа
			 * @param type {Number} тип активности (2,3,4)
			 * @param activity {Object} объект активности с полями
			 * text и списком ответов answers
			 */
			createActivity: function(token, type, activity)
			{
				var req = 'token='+token+'&type='+type;
				req += '&activity='+encodeURI(JSON.stringify(activity));
				return createActivityRes.save(req);
			},
			putIntoRoom: function(token, testId, position)
			{
				var tmp = 'token='+token+'&test_id='+testId+'&position='+position;
				return putIntoRoomRes.save(tmp);
			},
			overrideTest: function(token, testId, pos, obj)
			{
				var tmp = 'token='+token+'&test_id='+testId+'&position='+pos;
				for(var i in obj)
					if(obj[i])
						tmp += '&'+i+'='+obj[i];
				return overrideTestRes.save(tmp);
			},
			resume: function(token, paceMode, options, testId)
			{
				var req = '';
				if(testId != null)
					req = '&test_id='+testId;
				req += '&pace_mode='+paceMode;
				if(options)
				{
					if(paceMode === 1 && options.random != null) req += '&random='+options.random;
					if(paceMode === 1 && options.timer != null) req += '&timer='+options.timer;
					if(options.showResults != null) req += '&show_results='+options.showResults;
				}
				return resumeRes.save('token='+token+''+req);
			},
			enableNextQuestion: function(token)
			{
				return enableNextQuestionRes.save('token='+token);
			},
			finishQuestion: function(token)
			{
				return finishQuestionRes.save('token='+token);
			},
			changePosInRoom: function(token, testId, position, newPosition)
			{
				//сделать
			},

			/**
			 @function "restApi.room".RestApiTests.getNext
			 @desc получение следующего вопроса
			 @param {String} token токен доступа
			 @param {String} sessionId сессия
			 */

			killTest: function(token, streamId, reason)
			{
				return killTestRes.save('token='+token+'&stream_id='+streamId+'&reason='+reason);
			},

			/*	finishTest: function(token)
			 {
			 return finishTestRes.save('token='+token);
			 },*/

			getNext: function(token, sessionId)
			{
				return resource.get({method: 'getNext', token: token, session_id: sessionId});
			},
			/**
			 @function "restApi.room".RestApiTests.getAll
			 @desc получение всех вопросов теста
			 @param {String} token токен доступа
			 @param {Number} sessionId идентификатор сессии
			 */
			getAll: function(token, sessionId)
			{
				//return resource.query({url:'getAll.json'});
				return resource.get({method: 'getAll', token: token, session_id: sessionId});
			},
			/**
			 @function "restApi.room".RestApiTests.sendAnswer
			 @desc Отправка ответа на вопрос
			 @param object
			 @param object.token {String} token токен доступа
			 @param object.session_id {Number} session_id идентификатор сессии
			 @param object.question_id {Number}
			 @param object.answers {Array} массив ответов
			 @param object.answers.answer_id {Number} идентификатор ответа (используется в закрытом тесте)
			 @param object.answers.text {String} введенный ответ (используется в открытом тесте)
			 В РАЗРАБОТКЕ
			 */
			sendAnswer: function(token, sessionId, object)
			{
				/*
				 Структура object:
				 token - 1 (r)
				 session_id - 1 (r)
				 question_id - 1 (r)
				 answer_time - 1(r)
				 Answers: (o)
				 answer_id - ~ (r)
				 text - 1 (o)
				 */
				//return sendAnswerRes.save({url:'sendAnswer'});
				var tmp = 'token='+token;
				tmp += '&session_id='+sessionId;
				tmp += '&question_id='+object.questionId;
				tmp += '&answers='+encodeURIComponent(JSON.stringify({answers: object.answers}));
				tmp += '&answer_time='+object.answerTime;
				return sendAnswerRes.save(tmp);
			},
			/**
			 @function "restApi.room".RestApiTests.waitClosure
			 @desc Ожидание закрытия теста преподавателем
			 @param {String} token токен доступа
			 @param {Number} sessionId идентификатор сессии
			 */
			waitClosure: function(token, sessionId)
			{
				return resource.get({method:'waitClosure'});
			},

			/**
			 Ожидание результатов тестирования (long poll)
			 @function "restApi.room".RestApiRoom.waitResults
			 @param {String} token токен доступа
			 @param {Number} streamId идентификатор потока (нужен ли?)
			 @param {Number} hash хэш события
			 */
			waitResults: function(token, streamId, hash)
			{
				return resource.get({method:'waitResults', token: token, hash: hash});
			},
			/**
			 @function "restApi.room".RestApiRoom.waitEvent
			 @param {String} token токен доступа
			 @param {Number} hash хэш события
			 */
			waitEvent: function(token, hash)
			{
				return resource.get({method:'waitEvent', token: token, hash:hash});
			},
			/**
			 @function "restApi.room".RestApiRoom.waitEventFromStudents
			 @param {String} token токен доступа
			 @param {Number} hash хэш события
			 */
			waitEventFromStudents: function(token, hash)
			{
				return resource.get({method:'waitEventFromStudents', token: token, hash:hash});
			},
			startLesson: function(token, id, title, description)
			{
				var tmp = 'token='+token;
				if(id != null)
					tmp += '&subject_id='+id;
				else
				{
					if(title) tmp += '&subject_title='+title;
					if(description) tmp += '&description='+description;
				}

				return startLessonRes.save(tmp);
			},
			getLessonsSubjects: function(token) //добавить sort и limit
			{
				return resource.get({method:'getLessonsSubjects'});
			},
			endLesson: function(token)
			{
				return endLessonRes.save('token='+token);
			}


		});
	}
]);