/**
@namespace "restApi.fakedata"
@description модуль реализующий сервис-заглушку (mock) для API 
*/
angular.module('restApi.fakedata', ['ngMockE2E','settings']).
	run(['$httpBackend', 'Settings', function($httpBackend, Settings)
	{
		
		var currentState1 = 
		{
  "stream_id": 397,
  "test_state": "active",
  "add_time": 1455698312,
  "current_test": {
    "branch_id": 18,
    "test_id": 103,
    "owner_id": 3,
    "type": 0,
    "name": "Санкт-Петербург",
    "random": false,
    "timer": 0,
    "questions": 4,
    "description": "",
    "override": ""
  },
  "hashes": {
    "waitResults": 12
  },
  "queue": [
    
  ],
  "users": [
    {
      "first_name": "Василий",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_time": 44,
          "answers": [
            {
              "text": "1703"
            }
          ],
          "correct_answers": 1,
          "group_id": 315,
          "position": 1,
          "question_id": 58,
          "type": 2
        },
        {
          "all_correct_answers": 3,
          "answer_time": 175,
          "answers": [
            {
              "answer_id": 153
            },
            {
              "answer_id": 152
            },
            {
              "answer_id": 150
            }
          ],
          "correct_answers": 2,
          "group_id": 313,
          "position": 4,
          "question_id": 61,
          "type": 1
        },
        {
          "all_correct_answers": 1,
          "answer_time": 31,
          "answers": [
            {
              "answer_id": 464
            }
          ],
          "correct_answers": 1,
          "group_id": 312,
          "position": 2,
          "question_id": 216,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 60,
          "answers": [
            {
              "text": "Красный"
            }
          ],
          "correct_answers": 0,
          "group_id": 314,
          "position": 3,
          "question_id": 60,
          "type": 2
        }
      ],
      "second_name": null,
      "workingUser_id": 3561
    },
    {
      "first_name": "Василий",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_time": 34,
          "answers": [
            {
              "text": "1704"
            }
          ],
          "correct_answers": 0,
          "group_id": 315,
          "position": 1,
          "question_id": 58,
          "type": 2
        },
        {
          "all_correct_answers": 3,
          "answer_time": 37,
          "answers": [
            {
              "answer_id": 153
            },
            {
              "answer_id": 151
            },
            {
              "answer_id": 150
            }
          ],
          "correct_answers": 1,
          "group_id": 313,
          "position": 4,
          "question_id": 61,
          "type": 1
        },
        {
          "all_correct_answers": 1,
          "answer_time": 20,
          "answers": [
            {
              "answer_id": 463
            }
          ],
          "correct_answers": 0,
          "group_id": 312,
          "position": 2,
          "question_id": 216,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 45,
          "answers": [
            {
              "text": "синий"
            }
          ],
          "correct_answers": 1,
          "group_id": 314,
          "position": 3,
          "question_id": 60,
          "type": 2
        }
      ],
      "second_name": "Новиков",
      "workingUser_id": 3562
    },
    {
      "first_name": "Dima",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_time": 36,
          "answers": [
            {
              "text": "1703"
            }
          ],
          "correct_answers": 1,
          "group_id": 315,
          "position": 1,
          "question_id": 58,
          "type": 2
        },
        {
          "all_correct_answers": 3,
          "answer_time": 36,
          "answers": [
            {
              "answer_id": 153
            },
            {
              "answer_id": 152
            },
            {
              "answer_id": 149
            }
          ],
          "correct_answers": 3,
          "group_id": 313,
          "position": 4,
          "question_id": 61,
          "type": 1
        },
        {
          "all_correct_answers": 1,
          "answer_time": 17,
          "answers": [
            {
              "answer_id": 462
            }
          ],
          "correct_answers": 0,
          "group_id": 312,
          "position": 2,
          "question_id": 216,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 113,
          "answers": [
            {
              "text": "Дворцовый"
            }
          ],
          "correct_answers": 0,
          "group_id": 314,
          "position": 3,
          "question_id": 60,
          "type": 2
        }
      ],
      "second_name": null,
      "workingUser_id": 3563
    }
  ]
};
      for(var i=0;i<10;i++)
        currentState1.users.push({
          "first_name": "Василий",
          "questions": [
            {
              "all_correct_answers": 1,
              "answer_time": 34,
              "answers": [
                {
                  "text": Math.random()*1000
                }
              ],
              "correct_answers": 0,
              "group_id": 315,
              "position": 1,
              "question_id": 58,
              "type": 2
            },
            {
              "all_correct_answers": 3,
              "answer_time": 37,
              "answers": [
                {
                  "answer_id": 153
                },
                {
                  "answer_id": 151
                },
                {
                  "answer_id": 150
                }
              ],
              "correct_answers": 1,
              "group_id": 313,
              "position": 4,
              "question_id": 61,
              "type": 1
            },
            {
              "all_correct_answers": 1,
              "answer_time": 20,
              "answers": [
                {
                  "answer_id": 463
                }
              ],
              "correct_answers": 0,
              "group_id": 312,
              "position": 2,
              "question_id": 216,
              "type": 0
            },
            {
              "all_correct_answers": 1,
              "answer_time": 45,
              "answers": [
                {
                  "text": "синий"
                }
              ],
              "correct_answers": 1,
              "group_id": 314,
              "position": 3,
              "question_id": 60,
              "type": 2
            }
          ],
          "second_name": "Новиков",
          "workingUser_id": Math.random()*1000
        })

		var currentState = 
			{
  "stream_id": 396,
  "test_state": "active",
  "add_time": 1455648452,
  "current_test": {
    "branch_id": 73,
    "test_id": 162,
    "owner_id": 3,
    "type": 0,
    "name": "Сжатие данных",
    "random": false,
    "timer": 90,
    "questions": 4,
    "description": "",
    "override": ""
  },
  "hashes": {
    "waitResults": 12
  },
  "queue": [
    
  ],
  "users": [
    {
      "first_name": "Vasilii",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_time": 15,
          "answers": [
            {
              "answer_text": '596'
            }
          ],
          "correct_answers": 0,
          "group_id": 374,
          "position": 1,
          "question_id": 261,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 183,
          "answers": [
            {
              "answer_id": 605
            }
          ],
          "correct_answers": 1,
          "group_id": 377,
          "position": 4,
          "question_id": 265,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 12,
          "answers": [
            {
              "answer_id": 598
            }
          ],
          "correct_answers": 1,
          "group_id": 375,
          "position": 2,
          "question_id": 262,
          "type": 0
        },
        {
          "all_correct_answers": 3,
          "answer_time": 32,
          "answers": [
            {
              "answer_id": 602
            },
            {
              "answer_id": 601
            },
            {
              "answer_id": 599
            }
          ],
          "correct_answers": 3,
          "group_id": 376,
          "position": 3,
          "question_id": 263,
          "type": 1
        }
      ],
      "second_name": null,
      "workingUser_id": 3558
    },
    {
      "first_name": "Dima",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_text": '36',
          "answers": [
            {
              "answer_id": 609
            }
          ],
          "correct_answers": 0,
          "group_id": 374,
          "position": 1,
          "question_id": 267,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 54,
          "answers": [
            {
              "answer_id": 607
            }
          ],
          "correct_answers": 0,
          "group_id": 377,
          "position": 4,
          "question_id": 266,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 49,
          "answers": [
            {
              "answer_id": 598
            }
          ],
          "correct_answers": 1,
          "group_id": 375,
          "position": 2,
          "question_id": 262,
          "type": 0
        },
        {
          "all_correct_answers": 3,
          "answer_time": 19,
          "answers": [
            {
              "answer_id": 602
            },
            {
              "answer_id": 601
            },
            {
              "answer_id": 599
            }
          ],
          "correct_answers": 3,
          "group_id": 376,
          "position": 3,
          "question_id": 263,
          "type": 1
        }
      ],
      "second_name": null,
      "workingUser_id": 3559
    },
    {
      "first_name": "Василий",
      "questions": [
        {
          "all_correct_answers": 1,
          "answer_time": 23,
          "answers": [
            {
              "answer_id": 611
            }
          ],
          "correct_answers": 0,
          "group_id": 374,
          "position": 1,
          "question_id": 267,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 64,
          "answers": [
            {
              "answer_id": 605
            }
          ],
          "correct_answers": 1,
          "group_id": 377,
          "position": 4,
          "question_id": 265,
          "type": 0
        },
        {
          "all_correct_answers": 1,
          "answer_time": 41,
          "answers": [
            {
              "answer_id": 598
            }
          ],
          "correct_answers": 1,
          "group_id": 375,
          "position": 2,
          "question_id": 262,
          "type": 0
        },
        {
          "all_correct_answers": 3,
          "answer_time": 114,
          "answers": [
            {
              "answer_id": 602
            },
            {
              "answer_id": 601
            },
            {
              "answer_id": 599
            }
          ],
          "correct_answers": 3,
          "group_id": 376,
          "position": 3,
          "question_id": 263,
          "type": 1
        }
      ],
      "second_name": "Новиков",
      "workingUser_id": 3560
    }
  ]
};

var groups1 =
[
  {
    "description": null,
    "group_id": 315,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 141,
            "text": "1703"
          }
        ],
        "question_id": 58,
        "text": "<p>Укажите <b>год основания</b> Санкт-Петербурга</p><p><img src=\"http://spb.kraynevi.ru/template/images/portfolio/thumbs/img1.jpg\"/><br/></p>",
        "type": 2
      }
    ]
  },
  {
    "description": null,
    "group_id": 313,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 149,
            "correct": true,
            "text": "построен на болоте"
          },
          {
            "answer_id": 150,
            "correct": false,
            "text": "столица России (в наст.время)"
          },
          {
            "answer_id": 151,
            "correct": false,
            "text": "построен на семи холмах"
          },
          {
            "answer_id": 152,
            "correct": true,
            "text": "место работы и творчества Эйлера (ученый)"
          },
          {
            "answer_id": 153,
            "correct": true,
            "text": "назывался Ленинградом"
          }
        ],
        "question_id": 61,
        "text": "<p>Выберите <b>верные факты</b> о Санкт-Петербурге</p>",
        "type": 1
      }
    ]
  },
  {
    "description": null,
    "group_id": 312,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 462,
            "correct": false,
            "text": "Клодт Петр Карлович"
          },
          {
            "answer_id": 463,
            "correct": false,
            "text": "Бартоломео Карло Растрелли"
          },
          {
            "answer_id": 464,
            "correct": true,
            "text": "Этьен Морис Фальконе"
          }
        ],
        "question_id": 216,
        "text": "<p>Укажите скульптора &#34;Медного всадника&#34;</p><p><img src=\"http://mycalend.ru/media/uploads/2012-08-13/4_small.jpg\"/><br/></p>",
        "type": 0
      }
    ]
  },
  {
    "description": null,
    "group_id": 314,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 145,
            "text": "синий"
          },
          {
            "answer_id": 146,
            "text": "Синий"
          },
          {
            "answer_id": 147,
            "text": "синий мост"
          },
          {
            "answer_id": 148,
            "text": "Синий мост"
          }
        ],
        "question_id": 60,
        "text": "<p>Напишите название <b>самого широкого</b> моста в Санкт-Петербурге (и в мире)</p>",
        "type": 2
      }
    ]
  }
];

var groups = 
[
  {
    "description": null,
    "group_id": 374,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 593,
            "correct": false,
            "text": "да, т.к. K < 1"
          },
          {
            "answer_id": 594,
            "correct": false,
            "text": "нет, т.к. K > 1"
          },
          {
            "answer_id": 595,
            "correct": true,
            "text": "да, т.к. K > 1"
          },
          {
            "answer_id": 596,
            "correct": false,
            "text": "нет, т.к. K < 1"
          }
        ],
        "question_id": 261,
        "text": "<p><span style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"><b><b>Было ли сжатие данных проведено успешно, </b></b>если в</span> результате сжатия данных Vвых = 1133 бита, а Vвх = 4096 бит? </p><p>(Vвых - объем выходного потока данных, Vвх - объем входного потока данных, K - коэффициент сжатия данных)</p>",
        "type": 0
      },
      {
        "answers": [
          {
            "answer_id": 609,
            "correct": false,
            "text": "да, т.к. K < 1"
          },
          {
            "answer_id": 610,
            "correct": false,
            "text": "нет, т.к. K > 1"
          },
          {
            "answer_id": 611,
            "correct": false,
            "text": "да, т.к. K > 1"
          },
          {
            "answer_id": 612,
            "correct": true,
            "text": "нет, т.к. K < 1"
          }
        ],
        "question_id": 267,
        "text": "<p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"><b><b><b>Было ли сжатие данных проведено успешно,<span class=\"Apple-converted-space\"> </span></b></b></b>если в</span> результате сжатия данных Vвых = 2300 бит, а Vвх = 2146 бит? </p><p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">(Vвых - объем выходного потока данных, Vвх - объем входного потока данных, K - коэффициент сжатия данных)</p>",
        "type": 0
      }
    ]
  },
  {
    "description": null,
    "group_id": 377,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 603,
            "correct": false,
            "text": "да"
          },
          {
            "answer_id": 604,
            "correct": true,
            "text": "нет"
          }
        ],
        "question_id": 264,
        "text": "<p>Является ли код в этой таблице префиксным?</p><p>A - 00<br/>B - 10<br/>C - 11<br/>D - 101<br/>E - 010</p>",
        "type": 0
      },
      {
        "answers": [
          {
            "answer_id": 605,
            "correct": true,
            "text": "да"
          },
          {
            "answer_id": 606,
            "correct": false,
            "text": "нет"
          }
        ],
        "question_id": 265,
        "text": "<p>Является ли код в этой таблице префиксным?</p><p>A - 1<br/>B - 01<br/>C - 001<br/>D - 0000<br/>E - 0001</p>",
        "type": 0
      },
      {
        "answers": [
          {
            "answer_id": 607,
            "correct": false,
            "text": "да"
          },
          {
            "answer_id": 608,
            "correct": true,
            "text": "нет"
          }
        ],
        "question_id": 266,
        "text": "<p>Является ли код в этой таблице префиксным?</p><p>A - 11<br/>B - 01<br/>C - 000<br/>D - 0011<br/>E - 0001</p>",
        "type": 0
      }
    ]
  },
  {
    "description": null,
    "group_id": 375,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 597,
            "correct": false,
            "text": "сжатия с потерями"
          },
          {
            "answer_id": 598,
            "correct": true,
            "text": "сжатия без потерь"
          }
        ],
        "question_id": 262,
        "text": "<p><b>Какого типа</b> алгоритмы сжатия данных применяются в архиваторах?</p>",
        "type": 0
      }
    ]
  },
  {
    "description": null,
    "group_id": 376,
    "questions": [
      {
        "answers": [
          {
            "answer_id": 599,
            "correct": true,
            "text": "дает префиксный код"
          },
          {
            "answer_id": 600,
            "correct": false,
            "text": "сжимает данные с потерями"
          },
          {
            "answer_id": 601,
            "correct": true,
            "text": "длина кода - нефиксированная"
          },
          {
            "answer_id": 602,
            "correct": true,
            "text": "для декодирования требуется кодовая таблица"
          }
        ],
        "question_id": 263,
        "text": "<p>Отметьте верные высказывания относительно алгоритма Хаффмана</p>",
        "type": 1
      }
    ]
  }
];
		var lessons =
        {
          lessons: [
            {
              lesson_id: 29,
              room_url: '1234',
              title: 'Доклады студентов - 263',
              description: '',
              streams: 3,
              users: 10,
              begin_time: 0,
              end_time: 0,
            },
            {
              lesson_id: 28,
              room_url: '4321',
              title: 'Доклады студентов - 223',
              description: '',
              streams: 1,
              users: 12,
              begin_time: 0,
              end_time: 0
            }
          ]
        };
      
        var lessonsStreams =
        {
            streams:
            [
				{
				"add_time":1466092593,
				"del_reason":"finish",
				"del_time":1466094313,
				"pace_mode":1,
				"stream_id":906,
				"test":
					{
					"branch_id":117,
					"description":"",
					"name":"263 - 16.06",
					"owner_id":3,
					"questions":1,
					"random":false,
					"test_id":186,
					"timer":0,
					"type":1
					}
				},
				{
				"add_time":1466089998,
				"del_reason":"finish",
				"del_time":1466090079,
				"pace_mode":1,
				"stream_id":905,
				"test":
					{
					"branch_id":116,
					"description":"",
					"name":"243 - 16.06",
					"owner_id":3,
					"questions":1,
					"random":false,
					"test_id":185,
					"timer":0,
					"type":1
					}
				},
				{
				"add_time":1466085896,
				"del_reason":"finish",
				"del_time":1466088036,
				"pace_mode":1,
				"stream_id":904,
				"test":
					{
					"branch_id":115,
					"description":"",
					"name":"243/253/263 - 16.06",
					"owner_id":3,
					"questions":1,
					"random":false,
					"test_id":184,
					"timer":0,
					"type":1
					}
				},
            ]
        };
		
		var users = 
		{
			users:
			[
				{
					workingUser_id: 1,
					first_name: 'Alex',
					second_name: '',
					rate: 0.9,
				},
				{
					workingUser_id: 2,
					first_name: 'Max',
					second_name: '',
					rate: 0.43,
				},
				{
					workingUser_id: 3,
					first_name: 'Cage',
					second_name: '',
					rate: 0.51,
				},
				{
					workingUser_id: 4,
					first_name: 'Larry',
					second_name: '',
					rate: 0.75,
				}
			]
		}

		/*$httpBackend.whenGET('http://api.iquiz.win/rooms/getCurrentState?room_id=3&token=798716a72d857226ad9027e41ef0ae2c&type=1').respond(currentState);
		$httpBackend.whenGET('http://api.iquiz.win/rooms/get?token=798716a72d857226ad9027e41ef0ae2c').respond('{"room_id":3,"url":"11","access":1,"status":true,"password":null,"queue":[],"show_results":true}');
		$httpBackend.whenGET('http://api.iquiz.win/tests/getGroups?groups_id=%7B%22groups%22:%5B315,313,312,314%5D%7D&token=798716a72d857226ad9027e41ef0ae2c').respond(groups1);
		$httpBackend.whenGET('http://api.iquiz.win/tests/getGroups?groups_id=%7B%22groups%22:%5B374,377,375,376%5D%7D&token=798716a72d857226ad9027e41ef0ae2c').respond(groups)
		$httpBackend.whenGET('http://api.iquiz.win/rooms/waitEvent?token=798716a72d857226ad9027e41ef0ae2c').passThrough();
		$httpBackend.whenGET('http://api.iquiz.win/rooms/waitEventFromStudents?token=798716a72d857226ad9027e41ef0ae2c').passThrough();
		$httpBackend.whenGET('http://api.iquiz.win/rooms/waitResults?hash=12&token=798716a72d857226ad9027e41ef0ae2c').passThrough();*/
        // $httpBackend.whenGET('http://devapi.instaquiz.ru/rooms/getCurrentState?room_id=3&token=6b1236e3d67e154b5bc73eb85e9c4e6a&type=1').respond(currentState1);
      $httpBackend.whenGET('http://devapi.instaquiz.ru/reports/lessons?limit=20&offset=0&sort=-date&token=4e17bc47352396d15fdac940b63c90f5').respond(lessons);
	  $httpBackend.whenGET('http://devapi.instaquiz.ru/reports/lessons/29?limit=1&offset=0&token=4e17bc47352396d15fdac940b63c90f5').respond({lessons: [lessons.lessons[0]]});
	  $httpBackend.whenGET('http://devapi.instaquiz.ru/reports/lessons/29/streams?limit=20&offset=0&token=4e17bc47352396d15fdac940b63c90f5').respond(lessonsStreams);
	  $httpBackend.whenGET('http://devapi.instaquiz.ru/reports/lessons/29/users?limit=100&offset=0&token=4e17bc47352396d15fdac940b63c90f5').respond(users);
      $httpBackend.whenGET(/rooms\/get/).passThrough();
        $httpBackend.whenGET(/rooms\/waitEvent/).passThrough();
        $httpBackend.whenGET(/tests\/getFullTest/).passThrough();
        $httpBackend.whenGET(/tests\/getGroups/).passThrough();
        $httpBackend.whenGET(/rooms\/waitResults/).passThrough();
        $httpBackend.whenGET(/fs\//).passThrough();
		$httpBackend.whenGET(/layout\//).passThrough();
		$httpBackend.whenGET(/room\//).passThrough();
        $httpBackend.whenPOST(/rooms\//).passThrough();
		$httpBackend.whenGET(/reports\//).passThrough();
		$httpBackend.whenGET(/test\//).passThrough();
        $httpBackend.whenGET(/auth\//).passThrough();
        $httpBackend.whenPOST(/account\//).passThrough();
        $httpBackend.whenGET(/account\//).passThrough();
		$httpBackend.whenGET(/report\/view\/stream\//).passThrough();
        $httpBackend.whenGET(/lessons\//).passThrough();
        $httpBackend.whenGET(/manage\//).passThrough();

		
	}]);	