/**
@namespace "restApi.tests"
@description Модуль, в котором реализуется сервис взаимодействия с API api/tests
*/
angular.module('restApi.tests',['ngResource','settings'])
	.factory('RestApiTests',['$resource','Settings',
	/**
	@class "restApi.tests".RestApiTests
	@description Сервис обеспечивающий взаимодействие с API api/tests
	*/
	function($resource, Settings)
	{
		var resource = $resource(Settings.baseURL+'/tests/:url');
		var sendChangesRes = $resource(Settings.baseURL+'/tests/sendChanges');
		var createRes = $resource(Settings.baseURL+'/tests/create');
		return(
		{
			getFullTest: function(token, testId)
			{
				return resource.get({url:'getFullTest', token: token, test_id: testId});
			},
			getInfo: function(token,testId)
			{
				return resource.get({url: 'getInfo', token: token, test_id: testId});
			},
			create: function(token, folderId, object)
			{
				/*
				Структура object:
					token
					name
					type
					timer
					random
					description
				*/
				return createRes.save('token='+token+'&folder_id='+folderId+'&name='+object.name+'&test_type='+object.test_type+'&timer='+object.timer+'&random='+object.random+'&description='+object.description);
			},
			addGroup: function(token, testId, position)
			{
			},
			deleteGroup: function(token, testId, groupId)
			{
				
			},
			addQuestion: function(object)
			{
				/*
				Структура object:
					token - 1
					group_id - 1
					text - 1
					type - 1
					Answers:
					[
						{text - ~,
						correct - ~}
					]
					all required!
				*/
			},
			getGroups: function(token, groupsId)
			{
				return resource.query({url: 'getGroups', token: token, groups_id: {groups: groupsId}});
			},
			getQuestions: function(token, questionsId)
			{
				console.log(questionsId);
				return resource.query({url: 'getQuestions', token: token, questions_id: {questions: questionsId}});
			},
			sendChanges: function(token, testId, deleted, edited)
			{
				//return sendChangesRes.save({deleted: deleted, edited: edited});
				console.log(JSON.stringify(edited));
				return sendChangesRes.save('token='+token+'&test_id='+testId+'&deleted='+encodeURIComponent(JSON.stringify(deleted))+'&edited='+encodeURIComponent(JSON.stringify(edited)));
				//return sendChangesRes.save('token='+token+'&test_id='+testId+'&deleted='+(JSON.stringify(deleted))+'&edited='+(JSON.stringify(edited)));
			},
			saveTestInfo: function(object)
			{
				/*Структура object:
				user_id (r)
				token (r)
				name (o)
				timer (o)
				random (o)
				description (o)
				*/
			},
			saveQuestionInfo: function(object)
			{
				/*
				Структура object:
				token - 1 (r)
				question_id - 1 (r)
				text - 1 (o)
				type - 1 (o)
				Answers: (o)
				answer_id - ~(r)
				text - ~ (r)
				correct - ~ (r)
				*/
			},
		}
		);
	}]);