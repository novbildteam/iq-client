angular.module('IQApp.headerBar',['userAuthService', 'profileService', 'room.userRoom', 'tests.userTest']).
controller('HeaderBarCtrl', ['$scope', '$location', 'UserAuthService', 'Profile', 'UserRoom', 'UserTest', function($scope, $location, UserAuthService, Profile, UserRoom, UserTest)
{
	$scope.profile = Profile.profile;
	$scope.auth = UserAuthService;
	$scope.room = UserRoom;
	$scope.goToRoom = function()
	{
		if(!UserRoom.connected)
			UserRoom.resetRoom();
		$location.path('#/n').replace();
	}	
	$scope.logOut = function()
		{
			//���� ���� ����������� � �������, �� ������� ����� �� ���, � ����� �� �������
			if(UserRoom.connected)
			{
				$scope.logOutRoom(
				function()
				{
					$scope.logOutSystem();
				},
				function()
				{
					$scope.logOutSystem();
				}
				);
			}
			else
				$scope.logOutSystem();
		}
		$scope.logOutSystem = function()
		{
			UserAuthService.logOutSystem(UserAuthService.getUserToken(),
			function()
			{
				Profile.reset();
			},
			function()
			{
				console.log('error while logOut');
			});
		}
		$scope.logOutRoom = function(success, fail)
		{
			UserTest.quitTest();
			UserRoom.quitRoom(
			function()
			{
				UserRoom.resetRoom();
				$location.path('/');
				if(success) success();
			},
			function()
			{
				UserRoom.resetRoom();
				$location.path('/');
				if(fail) fail();
				console.log('error while logOut...room has reseted');
			})
		}
}]);