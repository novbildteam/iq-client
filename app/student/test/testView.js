'use strict'
angular.module('IQApp.testView',['ngRoute','tests.userTest','room.userRoom','userAuthService']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/test',
		{
			templateUrl: 'test/testView.html',
			controller: 'testViewCtrl'
		});
	}
	]).
	controller('testViewCtrl',['$scope', '$interval', '$location', '$routeParams' ,'UserTest', 'UserRoom', 'UserAuthService',
		function($scope, $interval, $location, $routeParams, UserTest, UserRoom, UserAuthService)
	{
		//если не подключен к комнате, то редирект на страницу с авторизацией
		console.log('r id: '+UserRoom.url);
			
		$scope.testState;
		$scope.question;
		$scope.answers;
		$scope.postviewAnswers;
		$scope.answer;
		$scope.tmpAnswers;
		$scope.room = UserRoom;
		$scope.reconnectState = null; //состояние переподключения
		//стиль вывода ответа см. binary, numeric, text
		$scope.answerViewType;
		$scope.viewQuestionResult = false;
		function connect()
		{
			UserRoom.init(
				function(data)
				{
					console.log('con', UserRoom);
					if(data.session_id)
					{
						if(data.session_created)
							init(data, 'new');
						else
							init(data);
					}
					else
						console.log('no session id yet!')
				},
				function(data)
				{
					console.log('failed while got current state');
					if(UserRoom.url)
						$location.path('/n'+UserRoom.url);
					else
						$location.path('/n');
					return;
				}
				);
		}
		
		function reconnect()
		{
			$scope.reconnectState = 'reconnecting';
			$interval(function(){
				UserRoom.init(
						function(data)
						{
							$scope.reconnectState = null;
							if(data.session_id)
							{
								init(data);
							}
							else
								console.log('no session id!')
						},
						function(data)
						{
							$scope.reconnectState = 'failed';
						}
					)}, 1000, 1);
		}
		$scope.reconnect = reconnect;
		
		if(!UserRoom.connected)
			connect();
		
		
		$scope.startTest = function()
		{
			if(UserRoom.connected)
			{
				$scope.waitForAnswer = true;
				$scope.nextQuestion(function()
				{
					$scope.waitForAnswer = false;
				});
			}
		}

		$scope.submitAnswer = function()
		{
			if($scope.waitForAnswer)
				return;
			$scope.postviewAnswers = [];
			if(!UserRoom.connected)
				return;
			if(UserTest.question.type == 1)
			{
				for(var i in $scope.tmpAnswers)
					if($scope.tmpAnswers[i] == true)
						$scope.answers.push({answer_id: Number(i)});
				if($scope.answers.length == 0)
					if(!$scope.timeIsUp)
						return;
			}
			else
			if(UserTest.question.type == 0)
			{
				if($scope.answer.answer_id == null)
				{
					if(!$scope.timeIsUp)
						return;
				}
				else
					UserTest.answers[0] = $scope.answer;
			}
			else
				if($scope.answer.text == '' || $scope.answer.text == null)
				{
					if(!$scope.timeIsUp)
						return;
				}
				else
					UserTest.answers[0] = $scope.answer;
					
			//Добавление ответов студента в массив демо результатов $scope.postviewAnswers
			if(UserTest.question.type == 0 || UserTest.question.type == 1)
			{
				for(var j in UserTest.answers)
					for(var i in $scope.question.answers)
						if($scope.question.answers[i].answer_id == UserTest.answers[j].answer_id)
							$scope.postviewAnswers.push({text: $scope.question.answers[i].text, answer_id: $scope.question.answers[i].answer_id});
			}
			else
				$scope.postviewAnswers.push({text: $scope.answer.text});
			
			$scope.waitForAnswer = true;
			UserTest.submitAnswer(function(data)
			{
				$scope.submitResults = data;
				if(UserTest.state.showResults || UserTest.state.paceMode == 2)
				{
					var results = data.answers;
					for(var i in results)
					{
						if(UserTest.question.type == 0 || UserTest.question.type == 1)
							for(var k in $scope.question.answers)
								if(results[i].answer_id == $scope.question.answers[k].answer_id)
									results[i].text = $scope.question.answers[k].text;
						for(var j in $scope.postviewAnswers)
						{
							if(results[i].answer_id && results[i].answer_id == $scope.postviewAnswers[j].answer_id
							|| results[i].text && results[i].text == $scope.postviewAnswers[j].text)
								$scope.postviewAnswers[j].correct = true;
						}
					}
					//проверка на правильность ответа
					var submitResults = $scope.submitResults;
					var answers = $scope.answers;
					var question = $scope.question;
					if(UserTest.state.type == 0)
					{
						if(submitResults.correct_answers == submitResults.all_correct_answers && answers.length == submitResults.all_correct_answers)
							$scope.postviewState = 'excellent';
						else
						if(submitResults.correct_answers == 0)
							$scope.postviewState = 'bad';
						else
						if(question.type == 1 && !(submitResults.correct_answers == submitResults.all_correct_answers && answers.length == submitResults.all_correct_answers))
							$scope.postviewState = 'good';
						else
							$scope.postviewState = null;
					}
					else
						$scope.postviewState = null;
					
				}
				if($scope.submitResults.finished)
				{
					//данные о результатах тестирования
					if(!UserTest.state.showResults)
					{
						for(var i in $scope.submitResults.questions)
							$scope.submitResults.questions[i].answers = $scope.submitResults.questions[i].user_answers;
					}
					else
					{
						/*
						Модификация полученного объекта
						объекты ответов массива questions дополняются полями:
							correct_answers - количество правильных ответов пользователя
							all_correct_answers - общее количество правильных ответов
						А также производится преобразование массивов answers и user_answer в общий массив answer
						содержащий объекты ответов
						text - текст вопроса
						correct - правильность/неправильность (в случае отсутсвия поля - ответ не был указан пользователем)
						*/
						var resultStars = 0; //количество "звездочек" заработанных пользователем
						for(var i in $scope.submitResults.questions)
						{
							var correctAnswers = 0;
							var obj = {};
							var answers = $scope.submitResults.questions[i].answers;
							var userAnswers = $scope.submitResults.questions[i].user_answers;
							if($scope.submitResults.questions[i].type == 1) //закрытый вопрос, с несколькими вариантами ответов
								$scope.submitResults.questions[i].all_correct_answers = answers.length;
							else //иначе - один правильный ответ
								$scope.submitResults.questions[i].all_correct_answers = 1;
							for(var j in answers)
								obj[answers[j].text] = {text: answers[j].text};
							for(var j in userAnswers)
								if(obj[userAnswers[j].text])
								{
									obj[userAnswers[j].text].correct = true;
									correctAnswers++;
								}
								else
									obj[userAnswers[j].text] = {text: userAnswers[j].text, correct: false}
							answers = [];
							for(var j in obj)
								answers.push(obj[j]);
							console.log('ans',answers);
							$scope.submitResults.questions[i].answers = answers;
							$scope.submitResults.questions[i].correct_answers = correctAnswers;
							if(correctAnswers == $scope.submitResults.questions[i].all_correct_answers)
								resultStars++;
							else
								if(correctAnswers != 0)
									resultStars += .5;
						}
						$scope.resultStars = $scope.resultStarsHelper(resultStars, $scope.submitResults.questions.length); //массив "звездочек": объект с полем star - если полная звезда, halfStar - если не полная
					}
					$scope.testState.state = 'finish';
				}
				if(UserTest.state.paceMode === 1 || UserTest.state.paceMode == null) //режим audience-paced
				{
					if(UserTest.state.showResults && $scope.testState.type == 0)
						$scope.viewQuestionResult = true;
					else
					if((!UserTest.state.showResults || $scope.testState.type == 1) && $scope.submitResults.finished)
						$scope.showEnding();
					else
						$scope.nextQuestion();
					$scope.waitForAnswer = false;
				}
				else
				if(UserTest.state.paceMode === 2) //режим presenter-paced
				{
					$scope.viewQuestionResult = false;
					$scope.waitForNext = true;
					console.log('ответ принят. ждем препода');
				}

			},
			function()
			{
				$scope.waitForAnswer = false;
			});
		}
		$scope.showEnding = function()
		{
			$scope.viewQuestionResult = false;
			$scope.ending = true;
		}
		$scope.nextQuestion = function(success)
		{
			$scope.questionLoaded = false;
			$scope.waitForNext = true;
			$scope.waitForAnswer = false;
			UserTest.nextQuestion(
			function()
			{
				initQuestion();
				$scope.waitForNext = false;
				$scope.questionIsNotEnabled = false;
				$scope.viewQuestionResult = false;
				$scope.questionLoaded = true;
				if(success) success();
			},
			function(error)
			{
				if(error.error_code == 217) //следующий вопрос еще недоступен (pace_mode = 2)
				{
					$scope.questionIsNotEnabled = true;
					$scope.questionLoaded = true;
				}
				console.log('err while loading next question');
			});
		};
		$scope.resultStarsHelper = function(resultStars, max)
		{
			if(resultStars)
			{
				var arr = new Array(max);
				var num = resultStars;
				var i = 0;
				while(num > 0)
				{
					if(num != .5)
					{
						arr[i] = ({star: true});
						num--;
					}
					else
					{
						num -= .5;
						arr[i] = ({halfStar: true});
					}
					i++;
				}
			}
			return arr;
		}
		
		
		$scope.$on('$routeChangeStart', function(e)
		{
			UserTest.closeTest();
		});
		UserRoom.events.question.onFinished = function() //текущий вопрос завершен в режиме presenter-paced
		{
			$scope.timeIsUp = true;
			$scope.submitAnswer();
			// $scope.timeIsUp = false;
			$scope.viewQuestionResult = true;
		};
		UserRoom.events.question.onEnabled = function() //переход к следующему вопросу в режиме presenter-paced
		{
			$scope.nextQuestion();
		};
		UserRoom.events.test.onActivated = function(data)
		{
			init(data,'new');
		};
		UserRoom.events.test.onFinished = function(data)
		{
			if(UserTest.state.state != 'cancel')
			{
				UserTest.state.state = 'finish';
				UserTest.closeTest();
			}
		};
		UserRoom.events.test.onCancelled = function(data)
		{
			if(UserTest.state.state != 'finish')
			{
				UserTest.state.state = 'cancel';
				UserTest.closeTest();
			}
		};
		UserRoom.events.room.onStatusChanged = function(data)
		{
			if(data.status == false)
				$scope.status = false;
			UserTest.quitTest();
		}
		UserRoom.events.room.onBanned = function(data)
		{
			$scope.banned = true;
		}
		UserRoom.events.error.pollerError = function(data)
		{
			
		}
		UserTest.event.timeIsUp = function()
		{
			$scope.$apply();
			$scope.timeIsUp = true;
			$scope.submitAnswer();
		};
		UserTest.event.timerTick = function()
		{
			$scope.progressTimer = UserTest.state.timer;
			$scope.$apply();
		};
		UserRoom.events.lesson.onFinished = function()
		{
			$scope.lessonState = 'finish';
		}
		
		function initQuestion()
		{
			$scope.postviewAnswers = [];
			$scope.answers = UserTest.answers;
			$scope.question = UserTest.question;
			identifyAnswerViewType();
			console.log('view type: '+$scope.answerViewType);
			if(UserTest.question.type == 0 || UserTest.question.type == 2)
			{
				$scope.answer = {answer_id: null, text: null};
				//UserTest.answers[0] = {answer_id: null, text: null};		
				//$scope.answer = UserTest.answers[0];
			}
			else
			{
				$scope.tmpAnswers = [];
			}
		}
		
		
		/*инициализация теста/опроса
		type - указывает на источник происхождения: 'new' - пришло событие о новом тесте
													null - загрузка по getCurrentState
		*/
		function init(data, type)
		{
			$scope.timeIsUp = false;
			$scope.waitForNext = false;
			$scope.waitForAnswer = false;
			$scope.ending = false;
			UserTest.init(data, type);
			$scope.testState = UserTest.state;
			$scope.name = UserTest.state.name;
			if($scope.testState.state == 'active')
				$scope.nextQuestion();
			/*UserTest.init(data.session_id,
			function()
			{
				$scope.testState = UserTest.state;
				$scope.name = UserTest.state.name;				
				initQuestion();
			},
			function(error)
			{
				if(error.error_code == 217)
				{
					//следующий вопрос еще недоступен (pace_mode = 2)
					$scope.testState = UserTest.state;
					$scope.name = UserTest.state.name;
					$scope.waitForNext = true;
					console.log('err while loading next question');
				}
				
				//произошла ошибка...
			});*/
		}
		
		function identifyAnswerViewType()
		{
			var question = UserTest.question;
			if(question.type == 0 && question.answers.length == 2
			&& (question.answers[0].text+question.answers[1].text).length < 6)
			{
				$scope.answerViewType = 'binary';
			}
			else
			if(question.type == 1)
			{
				$scope.answerViewType = 'numeric';
				for(var i in question.answers)
					if(question.answers[i].text.length > 4 || question.answers[i].text.search(/[A-z]/) > -1)
					{
						$scope.answerViewType = 'text';
						break;
					}
			}
			else
				$scope.answerViewType = 'text';
			
		}
		
		
	}
	]);