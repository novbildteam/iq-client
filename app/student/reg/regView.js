'use strict'
angular.module('IQApp.regView',['ngRoute', 'userAuthService', 'userRegService']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/reg:room',
		{
			templateUrl: 'reg/regView.html',
			controller: 'regViewCtrl'
		}).
		when('/reg', 
		{
			templateUrl: 'reg/regView.html',
			controller: 'regViewCtrl'
		});
	}
	]).
	controller('regViewCtrl',['$scope','$routeParams', '$location', 'UserAuthService', 'UserRegService',
	function($scope, $routeParams, $location, UserAuthService, UserRegService)
	{
		var auth = UserAuthService;
		if(auth.getUserToken() && !auth.isTemporary())
		{
			$scope.state = 'disallow'
			return;
		}
		else
			$scope.state = 'allow';
		var reg = UserRegService;
		$scope.username = reg.username;
		$scope.password = reg.password;
		$scope.firstName = reg.firstName;
		$scope.secondName = reg.secondName;
		
		$scope.submitData = function()
		{
			if($scope.username && $scope.password && $scope.firstName)
				reg.register(
				function()
				{
					//��� ����
					$scope.state = 'finished';
					$scope.timer = 0;
					var roomUrl = $routeParams.room ? $routeParams.room : '';
					setTimeout(function()
					{
						//�������� ����� 5 ���.
						$scope.timer++;
						$location.path('n'+$routeParams.room);
						$location.replace();
					}, 5000);
					
				},
				function(error)
				{
					$scope.state = 'failed'
					//������ ��� �����������, ��. error.code
				});
		}
		
		
	}]);