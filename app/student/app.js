/*'use strict';

// Declare app level module which depends on views, and components
angular.module('IQApp', [
  'ngRoute',
  'IQApp.roomView',
  'IQApp.testView',
  'IQApp.waitView',
  'IQApp.regView',
  'room.userRoom',
  'restApi.room',
  'restApi.accounts',
  'restApi.tests',
  'tests.userTest',
  'userAuthService',
  'userRegService',
  'restApi.fakedata'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/number'});
}]);*/

(function() {
	var profileData = {};
	var noNetAlertPages = //список со страницами, для которых не надо выводить стандартные ошибки (alert) подключения
	{
		'/test': true,
		'/n': true,
	}
	var myApplication = angular.module('IQApp', [
	'ngRoute',
  'IQApp.roomView',
  'IQApp.testView',
  'IQApp.waitView',
  'IQApp.regView',
  'IQApp.headerBar',
  'IQApp.authView',
  'room.userRoom',
  'restApi.room',
  'restApi.accounts',
  'restApi.tests',
  'tests.userTest',
  'userAuthService',
  'deviceService',
  'profileService',
  'userRegService',
  'httpInterceptor',
  'restApi.fakedata',
  'errorInterceptor',
  'ui.bootstrap',
  'ngSanitize',
]).
config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
  $httpProvider.interceptors.push('HttpInterceptor');
  $routeProvider.otherwise({redirectTo: '/n'});
}]).
run(['$rootScope', '$location', 'UserAuthService', 'UserRoom', 'UserTest', 'Profile', 'DeviceService', function($rootScope, $location, UserAuthService, UserRoom, UserTest, Profile)
{
	if(auth.getUserToken())
	{
		var obj = IQHelpers.toCamelCase(profileData);
		Profile.set(obj);
	}
}]).
controller('AppCtrl', ['$scope', '$location', '$rootScope', 'ErrorInterceptor', 'Settings', 'Profile', 'UserAuthService', function($scope, $location, $rootScope, ErrorInterceptor, Settings, Profile, UserAuthService)
{
	$scope.errorAlert; //объект события ошибки
	//обработчик полученных от сервера или сети
	ErrorInterceptor.onError = function(error)
	{
		console.log(error);
		if(error.type == ErrorInterceptor.ERR_TYPE.SERVER)
		{
			$scope.errorAlert = {type: 'danger'};
			switch(error.error.error_code)
			{
				case Settings.ERRCODE.TOKEN_OVER:
				case Settings.ERRCODE.WRONG_TOKEN:
				case Settings.ERRCODE.ROOMS.NOT_AUTH_IN_ROOM:
					/*$scope.errorAlert.msg = 'Ошибка аутентификации. Пожалуйста войдите <a href="#/n">еще раз</a>';
					$scope.errorAlert.timer = 5000;*/
					Profile.reset();
					UserAuthService.resetAuth();
				break;
				case Settings.ERRCODE.ACC.WRONG_LOGIN:
					$scope.errorAlert.msg = 'Неверные логин/пароль';
				break;
				default:
					$scope.errorAlert = null;
				break;
			}
		}
		else
		if(error.type == ErrorInterceptor.ERR_TYPE.NET)
		{
			if($location.path() in noNetAlertPages)
				return;
			$scope.errorAlert = {type: 'danger'};
			$scope.errorAlert.msg = 'Потеряна связь с сервером. Проверьте подключение';
			//$scope.errorAlert.timer = 15000;
		}
	}
	$scope.closeErrorAlert = function()
	{
		$scope.errorAlert = null;
	}
	$scope.$on('$routeChangeSuccess',
	function()
	{
		$scope.closeErrorAlert();
	});
}]);

		var authInjector = angular.injector(["userAuthService"])
		var auth = authInjector.get("UserAuthService");
		auth.getProfile(
		function(data)
		{
			profileData = data;
			bootstrapApplication();
		},
		function(error)
		{
			bootstrapApplication();
		});

		function bootstrapApplication()
		{
			angular.element(document).ready(
			function()
			{
				if(IQHelpers.isMobile())
					FastClick.attach(document.body); //подключение библиотеки FastClick для уменьшения времени отклика нажатия кнопок/ссылок
				angular.bootstrap(document, ["IQApp"]);
			});
		}
    
}());