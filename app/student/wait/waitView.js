'use strict';
angular.module('IQApp.waitView',['ngRoute','room.userRoom', 'tests.userTest']).
config(['$routeProvider', function($routeProvider)
{
	$routeProvider.when('/wait',
	{
		templateUrl: 'wait/waitView.html',
		controller: 'waitViewCtrl'
	});
}]).
controller('waitViewCtrl',['$scope','$location','UserRoom', 'UserTest', function($scope, $location, UserRoom, UserTest)
{
	$scope.roomId = UserRoom.roomId;
	UserRoom.waitTest(
	function(data)
	{
		console.log('wtf');
		UserTest.state.name = data.name;
		UserTest.state.type = data.type;
		UserTest.state.timeLimit = data.timer;
		$location.path("/test"+data.session_id);
		$location.replace();
	},
	function(error)
	{
		
	});
}]);