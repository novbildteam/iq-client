'use strict';
angular.module('IQApp.roomView',['ngRoute', 'room.userRoom','userAuthService', 'profileService']).
config(['$routeProvider', function($routeProvider)
{

	$routeProvider.when('/n:roomId',
	{
		templateUrl: 'number/roomView.html',
		controller: 'roomViewCtrl',
	}).
	when('/n',
	{
		templateUrl: 'number/roomView.html',
		controller: 'roomViewCtrl'
	});
}]).
controller('roomViewCtrl',['$scope', '$routeParams', '$location', 'Profile', 'UserRoom', 'UserAuthService', function($scope, $routeParams, $location, Profile, UserRoom, UserAuthService)
{

	var auth = UserAuthService;
	
	var ROOM_STATE = 
	{
		loading: 'loading', //комната загружается
		notFound: 'notFound', //комната не найдена
		loaded: 'loaded',//комната загружена
		authorization: 'authorization',
		undefined: 'undefined'
	}
	
	$scope.roomState = ROOM_STATE.loading;
	
	var roomId = $routeParams.roomId;
	
	//var room = UserRoom.room;
	
	$scope.roomInfo; //данные комнаты
	/*
		Объект ошибки авторизации
			empty - пустое значение поля
			wrongSymbols - недопустимые символы
			wrongValue - недопустимое/неправильное значение (ответ от сервера)
	*/
	$scope.authErrorObject = 
	{
		firstName: null,
		username: null,
		password: null,
		authError: false,
		roomPassword: null,
	};
	
	$scope.input =
	{
		username: null,
		password: null,
		roomPassword: null
	};

	$scope.social =
	{
	};

	$scope.needRoomPassword; //необходим пароль от комнаты
	
	$scope.needAuth; //необходима авторизация
	
	$scope.redirectToRoom = function(id)
	{
		if(!id)
			return;
		$location.path('n'+id);
	};
	$scope.changeRoom = function()
	{
		UserRoom.resetRoom();
		$location.path('#/n').replace();
	};
	$scope.logIn = logIn;
	$scope.roomId = roomId;
	
	if(UserRoom.connected)
		$location.path('/test');
	else
	if(!roomId)
	{
		if(!UserRoom.url)
			$scope.roomState = ROOM_STATE.undefined;
		else
			enterRoom(null);
	}
	else
		enterRoom(roomId);

	function enterRoom(roomId)
	{
		UserRoom.getRoom(roomId,
		function(data)
		{
			//$scope.roomId = roomId;
			$scope.roomInfo = data;
			$scope.roomState = ROOM_STATE.loaded;
			$scope.needRoomPassword = $scope.roomInfo.need_pass;
			if( (auth.getUserToken() || $routeParams.token) || $scope.roomInfo.access == 0 && !$scope.needRoomPassword)
				logIn();
			else
			{
				$scope.needAuth = true;
			}
			/* Ссылки на авторизации через соц сети*/
			$scope.social.vk = UserAuthService.socialSignInUrl('vk',2,{roomUrl: UserRoom.url});
			$scope.social.fb = UserAuthService.socialSignInUrl('fb',2,{roomUrl: UserRoom.url});
			$scope.social.google = UserAuthService.socialSignInUrl('google',2,{roomUrl: UserRoom.url});

		},
		function(error)
		{
			$scope.roomState = ROOM_STATE.notFound;
		}
		);
	}


	function logIn()
	{	
		var obj =
		{
			access: $scope.roomInfo.access,
			username: $scope.input.username,
			firstName: $scope.input.firstName,
			secondName: $scope.input.secondName,
			password: $scope.input.password,
			roomId: UserRoom.roomId,
			roomPassword: $scope.input.roomPassword,
			token: auth.getUserToken() || $routeParams.token
		};
		console.log('token: '+obj.token);
		if(!obj.token && obj.access == 2)
		{
			if(obj.username && obj.password)
			{
				//сначала залогиниться в системе!
				var obj1 = {username: obj.username, password: obj.password};
				auth.logInSystem(obj1,
				function(token)
				{
					//удачно залогинились
					$scope.authErrorObject.authError = false;
					$scope.authErrorObject.username = null;
					$scope.authErrorObject.password = null;
					$scope.needAuth = false;
					console.log('log in system: '+token);
					obj.token = token;
					logInRoom(obj);
					//Profile.set();
				},
				function(error)
				{
					console.log('login system error');
					//ошибка авторизации
					$scope.needAuth = true;
					$scope.authErrorObject.authError = true;
					$scope.authAlert = {type: 'danger', msg: 'Ошибка авторизации: неверный логин/пароль'};
				});
			}
			else
				$scope.needAuth = true;
		}
		else
		if(!(obj.token || $scope.input.firstName) && obj.access == 1)
		{
			if(!$scope.input.firstName) $scope.authErrorObject.firstName = 'empty';
			$scope.needAuth = true;
		}
		else
			logInRoom(obj);
		
		function logInRoom(obj)
		{
			$scope.roomState = ROOM_STATE.authorization;
			auth.logInRoom(obj,
			function()
			{
				$scope.roomState = null;
				$scope.needAuth = false;
				$scope.needRoomPassword = false;
				console.log('login room!!!');
				Profile.load(
					function()
					{
						//UserRoom.connect();
						//$location.url('/test').replace();
						$location.url('/test');
						$location.replace();
					});
			},
			function(error)
			{
				$scope.roomState = 'loaded';
				//ловим ошибки...
				if(error.error_code == 202)
				{
					$scope.authErrorObject.firstName = null;
					$scope.needRoomPassword = true;
					$scope.authErrorObject.roomPassword = 'wrongValue';
				}
				else
				if(error.error_code == 8) //токен входа оказался неверным!
				{
					console.log('получен неверный токен доступа');
					$scope.needAuth = true;
				}
				else
				if(error.error_code == 4)
				{
					console.log('не введено имя пользователя');
					$scope.authErrorObject.firstName = 'empty';
					$scope.needAuth = true;
				}
			}
			);
		}
		
	}
}
]);