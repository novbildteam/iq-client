'use strict';
angular.module('IQApp.roomView',['ngRoute', 'room.userRoom','userAuthService']).
config(['$routeProvider', function($routeProvider)
{

	$routeProvider.when('/number:roomId',
	{
		templateUrl: 'number/roomView.html',
		controller: 'roomViewCtrl',
	}).
	when('/number',
	{
		templateUrl: 'number/roomView.html',
		controller: 'roomViewCtrl'
	});
}]).
controller('roomViewCtrl',['$scope', '$routeParams', '$location', 'UserRoom', 'UserAuthService', function($scope, $routeParams, $location, UserRoom, UserAuthService)
{

	var auth = UserAuthService;
	
	var ROOM_STATE = 
	{
		loading: 'loading', //комната загружается
		notFound: 'notFound', //комната не найдена
		loaded: 'loaded',//комната загружена
		undefined: 'undefined'
	}
	
	$scope.roomState = ROOM_STATE.loading;
	
	var roomId = $routeParams.roomId;
	
	//var room = UserRoom.room;
	
	$scope.roomInfo; //данные комнаты
	
	$scope.input =
	{
		username: null,
		password: null,
		roomPassword: null
	}
	
	$scope.needRoomPassword; //необходим пароль от комнаты
	
	$scope.needAuth; //необходима авторизация
	
	$scope.redirectToRoom = function(id)
	{
		$location.path('number'+id);
	};
	$scope.logIn = logIn;
	
	if(!roomId)
	{
		if(!UserRoom.roomId)
			$scope.roomState = ROOM_STATE.undefined;
		else
			enterRoom(null);
	}
	else
		enterRoom(roomId);
	
	function enterRoom(roomId)
	{
		UserRoom.getRoom(roomId,
		function(data)
		{
			$scope.roomId = data.room_id;
			$scope.roomInfo = data;
			$scope.roomState = ROOM_STATE.loaded;
			console.log('комната найдена');
			accessCheck();
		},
		function(error)
		{
			$scope.roomState = ROOM_STATE.notFound;
			console.log('комната не найдена');
		}
		);
	}
	
	/*
	Следующие две функции следует переписать!
	*/
	function accessCheck() //проверка уровня доступа к комнате
	{
		var token = auth.getUserToken();
		$scope.needRoomPassword = $scope.roomInfo.need_pass;
		logIn();
		/*if(token && !$scope.roomInfo.need_pass)
		{
			$scope.needAuth = false;
			logIn();
		}
		else
		if(!token)
		{
			$scope.needAuth = true;
		}*/
	}
	
	function logIn()
	{	
		var obj =
		{
			access: $scope.roomInfo.access,
			username: $scope.input.username,
			firstName: $scope.input.firstName,
			secondName: $scope.input.secondName,
			password: $scope.input.password,
			roomId: $scope.roomId,
			roomPassword: $scope.input.roomPassword,
			token: auth.getUserToken()
		};
		console.log('token: '+obj.token);
		if(!obj.token && obj.access == 2)
		{
			if(obj.username && obj.password)
			{
				//сначала залогиниться в системе!
				var obj1 = {username: obj.username, password: obj.password};
				auth.logInSystem(obj1,
				function(token)
				{
					//удачно залогинились
					console.log('log in system: '+token);
					obj.token = token;
					logInRoom(obj);
				},
				function(error)
				{
					console.log('login system error');
					//ошибка авторизации
					$scope.needAuth = true;
				});
			}
			else
				$scope.needAuth = true;
		}
		else
		if(!(obj.token || $scope.input.firstName) && obj.access == 1)
		{
			$scope.needAuth = true;
		}
		else
			logInRoom(obj);
		
		function logInRoom(obj)
		{
			auth.logInRoom(obj,
			function()
			{
				$scope.needAuth = false;
				$scope.needRoomPassword = false;
				console.log('login room!!!');
				$location.path('wait');
				//redirect to #/wait
			},
			function(error)
			{
				//ловим ошибки...
				if(error.error_code == 202)
				{
					console.log('ошибка пароля комнаты'); //неправильный пароль комнаты
					$scope.needRoomPassword = true;
				}
				else
				if(error.error_code == 8) //токен входа оказался неверным!
				{
					console.log('получен неверный токен доступа');
					$scope.needAuth = true;
				}
				else
				if(error.error_code == 4)
				{
					console.log('не введено имя пользователя');
					$scope.needAuth = true;
				}
			}
			);
		}
		
	}
}
]);