﻿angular.module('IQApp.authView',['ngRoute', 'userAuthService', 'profileService']).
config(['$routeProvider', function($routeProvider)
{
	$routeProvider.when('/auth',
	{
		templateUrl: 'auth/authView.html',
		controller: 'authViewCtrl'
	});
}]).
controller('authViewCtrl',['$scope', '$location', '$interval', 'UserAuthService', 'Profile', 
		function($scope, $location, $interval, UserAuthService, Profile)
		{
			var auth = UserAuthService;
			if(auth.getUserToken() && !auth.isTemporary())
			{
				
				$location.path('#/n').replace();
				return;
			}
			$scope.authAlert = null;
			$scope.input =
			{
				username: null,
				password: null,
			}
			$scope.login = false;
			$scope.loadingProfile = false;
			$scope.logIn = function()
			{
				if($scope.login || $scope.logged)
					return;
				$scope.login = true;
				auth.logInSystem({type: 2, username: $scope.input.username, password: $scope.input.password},
				function()
				{
					$scope.login = false;
					$scope.logged = true;
					$scope.authAlert = null;
					$scope.loadingProfile = true;
					var tmp = function()
					{
						Profile.load(
						function()
						{
							$location.path('#/n').replace();
							$scope.loadingProfile = false;
						},
						function()
						{
							$scope.loadingProfile = false;
						});
					}
					$interval(tmp, 1500, 1);
				},
				function(error)
				{
					$scope.login = false;
					$scope.logged = false;
					if(error.error_code)
						$scope.authAlert = {msg: 'Ошибка авторизации: неверный логин/пароль'};
					else
						$scope.authAlert = {msg: 'Ошибка сети'};
				});
			}
		}
	]);