'use strict'
angular.module('IQApp.welcomeView',['ngRoute','userAuthService','helpers']).
config(['$routeProvider', function($routeProvider)
{
    $routeProvider.when('/welcome',
        {
            templateUrl: 'welcome/welcomeView.html',
            controller: 'welcomeViewCtrl',
            reloadOnSearch: false
        });
}
]).
controller('welcomeViewCtrl',['$scope', '$rootScope', '$location', '$modal', '$routeParams' , 'UserAuthService','Tutorial',
   function($scope, $rootScope, $location, $modal, $routeParams, UserAuthService, Tutorial)
{
    if(!UserAuthService.getUserToken())
    {
        $location.path('/auth');
        return;
    }
    Tutorial.go('welcomeMessage',
        function()
        {
            Tutorial.go('basicConcept', function()
            {
                $location.path('/lessons');
            });
        },
        function()
        {
            $location.path('#/lessons');
        }
    );
}]).
controller('tutorialViewModalCtrl',['$scope','$location', '$modalInstance', 'page', 'Tutorial', function($scope, $location, $modalInstance, page, Tutorial)
{
   $scope.currentPage = page;
    $scope.tryCancelTutorial = function()
    {
        $scope.tryCancel = true;
    };
    $scope.cancelTutorial = function()
    {
        Tutorial.cancel();
        $modalInstance.dismiss();
    };
    $scope.cancelCancel = function()
    {
      $scope.tryCancel = false;  
    };
    $scope.nextPage = function()
    {
        $modalInstance.close();
    }
}]);
// .
// controller('welcomeViewModalCtrl',['$scope', '$rootScope', '$location', '$modalInstance', '$routeParams' , 'UserAuthService', function($scope, $rootScope, $location, $modalInstance, $routeParams, UserAuthService)
// {
//     var pages = [
//         'welcomeMessage',
//         'basicConcept',
//         'lessonCreation',
//         'lessonCreated',
//         'startTest',
//         'testCreation',
//     ];
//     $scope.page = 0;
//     $scope.currentPage = pages[$scope.page];
//     $scope.close = function()
//     {
//         $modalInstance.close();
//     }
//     $scope.nextPage = function()
//     {
//         if($scope.page+1 < pages.length)
//             $scope.page++;
//         $scope.currentPage = pages[$scope.page];
//     }
//     $scope.prevPage = function()
//     {
//         if($scope.page-1 >= 0)
//             $scope.page--;
//         $scope.currentPage = pages[$scope.page];
//     }
// }]);