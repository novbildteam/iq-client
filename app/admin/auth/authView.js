﻿'use strict'
angular.module('IQApp.authView',['ngRoute', 'userAuthService', 'profileService']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/auth',
		{
			templateUrl: 'auth/authView.html',
			controller: 'authViewCtrl'
		}).
		when('/forgot',
		{
			templateUrl: 'auth/forgotView.html',
			controller: 'forgotViewCtrl'
		});
	}
	]).
	controller('authViewCtrl',['$scope', '$routeParams', '$location', '$interval', '$window', 'UserAuthService', 'Profile',
		function($scope, $routeParams, $location, $interval, $window, UserAuthService, Profile)
		{
			if(UserAuthService.getUserToken())
			{
				$location.path('/lessons');
				return;
			}
			$window.ga('set', 'page', '/authView.html');
			$window.ga('send', 'pageview');

			/* Ссылки на авторизации через соц сети*/
			$scope.social =
			{
				vk: UserAuthService.socialSignInUrl('vk',1),
				fb: UserAuthService.socialSignInUrl('fb',1),
				google: UserAuthService.socialSignInUrl('google',1),
			};

			$scope.input =
			{
				username: null,
				password: null,
			};
			$scope.login = false;
			$scope.loadingProfile = false;
			var auth = UserAuthService;
			$scope.logIn = function()
			{
				if($scope.login || $scope.logged)
					return;
				//$scope.authAlert = null;
				$scope.login = true;
				auth.logInSystem({type: 2, username: $scope.input.username, password: $scope.input.password,
					token: $routeParams.token, user_id: $routeParams.user_id},
				function()
				{
					$scope.login = false;
					$scope.logged = true;
					$scope.loadingProfile = true;
					var tmp = function()
					{
						Profile.load(
						function()
						{
							$location.path('#/lessons').replace();
							$scope.loadingProfile = false;
						},
						function()
						{
							$scope.loadingProfile = false;
						});
					}
					$interval(tmp, 1000, 1);
					
				},
				function(error)
				{
					$scope.login = false;
					$scope.logged = false;
					if(error.error_code)
						$scope.authAlert = {msg: 'Ошибка авторизации: неверный логин/пароль', type: 'danger'};
					else
						$scope.authAlert = {msg: 'Ошибка сети', type: 'danger'};
				});
			}
			if($routeParams.token && $routeParams.user_id)
			{
				$scope.tokenAuth = true;
				$scope.logIn();
			}
			if($routeParams.error == 104)
				$scope.authAlert = {msg: 'Ваш аккаунт не зарегистрирован', type: 'danger'};
		}
	]).
	controller('forgotViewCtrl',['$scope', '$window', 'UserRegService',
		function($scope, $window, UserRegService)
		{
			$window.ga('set', 'page', '/forgotView.html');
			$window.ga('send', 'pageview');

			$scope.input = {username: null};
			$scope.reseted = false;
			$scope.reset = function()
			{
				$scope.reseting = true;
				UserRegService.resetPassword($scope.input.username,
					function()
					{
						$scope.reseted = true;
					},
					function(error)
					{
						$scope.reseted = false;
						$scope.reseting = false;
						$scope.authAlert = {msg: 'Ошибка: Такого e-mail не существует', type: 'danger'};
					}
				);
			}
			
		}
	]);