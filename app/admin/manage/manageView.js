'use strict'
angular.module('IQApp.manageView',['ngRoute','fsManager', 'tests.adminTest', 'room.adminRoom', 'userAuthService','helpers']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/manage/:folder',
		{
			templateUrl: 'manage/manageView.html',
			controller: 'manageViewCtrl'
		}).
		when('/manage',
		{
			templateUrl: 'manage/manageView.html',
			controller: 'manageViewCtrl'
		})
	}
	]).
	controller('manageViewCtrl',['$scope', '$routeParams', '$location', '$interval', '$window', 'FsManager', 'Profile', '$modal', 'AdminTest', 'UserAuthService', 'AdminRoom', 'Tutorial',
	function($scope, $routeParams, $location, $interval, $window, FsManager, Profile, $modal, AdminTest, UserAuthService, AdminRoom, Tutorial)
	{
		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
			return;
		}

		$window.ga('set', 'page', '/libraryView.html');
		$window.ga('send', 'pageview');

		$scope.folder = FsManager.folder;
		$scope.manage = FsManager;
		$scope.selectedItem = null; //выбранный элемент
		$scope.removingItem = null; //удаляемый элемент
		$scope.notifications = []; //уведомления о действиях пользователя (список объектов {msg:null,} )
		$scope.$watch('selectedItem',function(oldVal, newVal)
		{
			$scope.removingItem = null;
		});


		var currentFolder;
		$scope.state = 'loading';
		if(!$routeParams.folder)
			FsManager.open(Profile.profile.rootId,
			function()
			{
				console.log('root opened');
				$scope.state = 'loaded';
				currentFolder = Profile.profile.rootId;
				$scope.selectedItem = null;
			},
			function(error)
			{
				console.log(error);
				$scope.state = 'failed';
			});
		else
			FsManager.open($routeParams.folder,
			function()
			{
				$scope.state = 'loaded';
				currentFolder = $scope.folder.path[$scope.folder.path.length-1].folder_id
				$scope.selectedItem = null;
			},
			function()
			{
				$scope.state = 'failed';
				console.log('something wrong...');
			});

		if(!AdminRoom.connected)
		{
			AdminRoom.init(
				function()
				{
				},
				function()
				{
				});
		}
		if(Tutorial.current)
			Tutorial.go('fs');


		$scope.open = function(item)
		{
			if(item.item_type == 1 || item.item_type == 2)
			{
				$location.url('/test?id='+item.test_id);
			}
			else
			if(item.item_type == 0)
			{
				if(item.folder_id)
					$location.path('/manage/'+item.folder_id);
			}
			else
			if(item.folder_id) //навигация по дереву
				$location.path('/manage/'+item.folder_id);
		}
		$scope.openTest = function(testId)
		{
			$location.url('/test');
		}
		$scope.makeDir = function()
		{
			var modalInstance = $modal.open(
			{
				animation: $scope.animationsEnabled,
				templateUrl: 'makeDirModal.html',
				controller: 'makeDirModalCtrl',
				resolve: {item: function(){return null}}
			});
			modalInstance.result.then(function (name)
			{
				FsManager.makeDir(name, currentFolder,
				function()
				{
					console.log('folder made');
				},
				function()
				{
					console.log('err while make folder');
				});
			},
			function ()
			{				  
			});
		}
		$scope.createTest = function(type)
		{
			var modalInstance = $modal.open(
			{
				animation: $scope.animationsEnabled,
				templateUrl: 'addTestModal.html',
				controller: 'addTestModalCtrl',
				resolve: {type: function(){return type;}}
			});
			modalInstance.result.then(function(result)
			{
				var testInfo =
				{
					name: result.name,
					test_type: type,
					timer: 0,
					random: false,
					description: '',
				};
				FsManager.addTest(currentFolder, testInfo,
				function(data)
				{
					$location.url('/test?id='+data.test_id);
				},
				function(error)
				{
					console.log('err while adding test');
					console.log(error);
				});
			},
			function()
			{});
		};
		$scope.rename = function(item)
		{
			var modalInstance = $modal.open(
			{
				animation: $scope.animationsEnabled,
				templateUrl: 'renameModal.html',
				controller: 'renameModalCtrl',
				resolve: {item: function(){return item}},
			});
			modalInstance.result.then(function (name)
			{
				FsManager.rename(item.item_id, name,
				function()
				{
					console.log('folder renamed');
				},
				function()
				{
					console.log('err while renaming');
				});
			},
			function ()
			{				  
			});
		}
		$scope.tryToRemove = function(item)
		{
			$scope.removingItem = item;
		}
		$scope.remove = function(item)
		{
			FsManager.delete(item.item_id,
			function()
			{
				console.log('deleted');
				if($scope.selectedItem == $scope.removingItem)
					$scope.selectedItem = null;
				$scope.removingItem = null;
			},
			function()
			{
				console.log('err while removing');
			});
		};
		$scope.replace = function(item)
		{
			var modalInstance = $modal.open(
			{
				animation: true,
				templateUrl: 'replaceModal.html',
				scope: $scope,
				controller: 'replaceModalCtrl',
				resolve: {item: function(){return item}},
			});
			modalInstance.result.then(function(dirDestId)
			{
				if(!dirDestId)
					return;
				FsManager.move(item.item_id, dirDestId,
				function ()
				{
					console.log('folder replaced');
					$location.path('/manage/'+dirDestId);
				},
				function ()
				{
					console.log('err while replacing');
					$location.path('/manage/'+dirDestId);
				});
			},
			function()
			{
				FsManager.open(currentFolder);
			})
		};
		$scope.endReplace = function(dirDestId)
		{
			if(!dirDestId)
				$scope.replacingItem = null;
			FsManager.move(item.item_id, dirDestId,
				function()
				{
					console.log('folder replaced');
				},
				function()
				{
					console.log('err while replacing');
				});
		}
		$scope.startTest = function(test)
		{
			//if(AdminRoom.connected && AdminRoom.config.status)
			if(AdminRoom.lessonState != 'active')
			{
				AdminRoom.startLesson({});
			}
			if (AdminRoom.testState == 'active')
					AdminRoom.cancelTest();
			var tmp =
			{
				test_id: test.test_id,
				owner_id: test.owner_id,
				name: test.name,
				type: test.test_type, //самый главный косяк!
				questions: test.questions,
				timer: test.timer,
				random: test.random,
				creation_time: test.creation_time,
				editing_time: test.editing_time,
				description: test.description,
				branch_id: test.branch_id
			};
			// AdminRoom.resume(1, {showResults: true}, test.test_id);
			var modalInstance = $modal.open(
				{
					animation: true,
					templateUrl: 'room/resumeModal.html',
					controller: 'resumeCtrl',
					scope: $scope,
					size: 'lg',
					resolve:
					{
						showResults: function(){return AdminRoom.currentTest.showResults;},
						test: function() {return tmp;},
					}
				});
			modalInstance.result.then(
				function (result)
				{
					AdminRoom.resume(result.paceMode, result.options, test.test_id);
					$location.path('/room');
				},
				function ()
				{
				}
			);
		};
		$scope.addTest = function(test)
		{
			if(AdminRoom.connected && AdminRoom.config.status)
			{
				var tmp =
				{
					test_id: test.test_id,
					owner_id: test.owner_id,
					name: test.name,
					type: test.test_type,
					questions: test.questions,
					timer: test.timer,
					random: test.random,
					creation_time: test.creation_time,
					editing_time: test.editing_time,
					description: test.description,
					branch_id: test.branch_id
				};
				AdminRoom.putIntoRoom(tmp, AdminRoom.queue.list.length+1);
				showNotification('addedToQueue', test);
			}
			else
				showNotification('roomDisabled')
		};
		function showNotification(type, obj)
		{
			switch(type)
			{
				case 'addedToQueue':
					var message;
					if(obj.test_type == 0)
						message = 'тест "'+obj.name+'" добавлен в очередь';
					else
					if(obj.test_type == 1)
						message = 'опрос "'+obj.name+'" добавлен в очередь';
					$scope.notifications.push({msg: message, type: type});
					hideNotificationAfter(2500);
					break;
				case 'testStarted':
					var message;
					if(obj.test_type == 0)
						message = 'тест "'+obj.name+'" запущен в комнате';
					else
					if(obj.test_type == 1)
						message = 'опрос "'+obj.name+'" запущен в комнате';
					$scope.notifications.push({msg: message, type:type});
					hideNotificationAfter(5000);
					break;
				case 'roomDisabled':
					$scope.notifications.push({msg: 'Комната сейчас деактивирована', type:type});
					hideNotificationAfter(5000);
					break;
			}
		}
		function hideNotificationAfter(timer)
		{
			$interval(function(){$scope.notifications = $scope.notifications.slice(2)},timer,1);
		}
		/*
		 * Очередь, кнопка "добавить"
		 */
		angular.element(document).ready(function()
		{
			queueResize();
			window.onresize = queueResize;
		});

		function queueResize()
		{
			try
			{
				var queue = document.getElementById('article')
					,queue_add = document.getElementById('aside')
					,queue_bar = document.getElementById('left-bar')

				var queue_height = queue_bar.offsetHeight - queue_add.offsetHeight - queue.offsetTop - 60;
				queue.style['max-height'] = queue_height+'px';
				$scope.$apply();
			}
			catch(e){}
		}

	}
	]).
	controller('makeDirModalCtrl', ['$scope', 'item', '$modalInstance', function($scope, item, $modalInstance)
	{
		if(item)
			$scope.name = item.name;
		else
			$scope.name = '';
		$scope.ok = function()
		{
			$modalInstance.close($scope.name);
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		};
	}]).
	controller('replaceModalCtrl', ['$scope', 'item', 'FsManager', 'Profile', '$modalInstance', function($scope, item, FsManager, Profile, $modalInstance)
	{
		$scope.openFolder = function(folderId)
		{
			FsManager.open(folderId,
			function()
			{
				$scope.fsState = 'loaded';
				$scope.currentFolder = folderId;
			},
			function(error)
			{
				console.log(error);
				$scope.fsState = 'failed';
			});
		};
		$scope.move = function(folderId)
		{
			$modalInstance.close(folderId);
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		};

		$scope.fsState = 'loading';
		$scope.explorer = FsManager;
		$scope.currentFolder = Profile.profile.rootId
		$scope.openFolder($scope.currentFolder);

	}]).
	controller('renameModalCtrl', ['$scope', 'item', '$modalInstance', function($scope, item, $modalInstance)
	{
		if(item)
			$scope.name = item.name;
			$scope.item_type = item.item_type;
		$scope.ok = function()
		{
			$modalInstance.close($scope.name);
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		};
	}]).
	controller('addTestModalCtrl', ['$scope', '$modalInstance', 'type', function($scope, $modalInstance, type)
	{
		$scope.type = type;
		$scope.ok = function()
		{
			$modalInstance.close({name: $scope.name, type: $scope.type});
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		}
	}]);