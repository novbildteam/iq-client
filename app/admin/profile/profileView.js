'use strict'
angular.module('IQApp.profileView',['ngRoute', 'profileService', 'userAuthService', 'settings']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/profile',
		{
			templateUrl: 'profile/profileView.html',
			controller: 'profileViewCtrl'
		});
		$routeProvider.when('/pass',
		{
			templateUrl: 'profile/passView.html',
			controller: 'passViewCtrl',
		});
	}
	]).
	controller('profileViewCtrl', ['$scope', '$location', '$window', 'Profile', 'UserAuthService', 'Settings', function($scope, $location, $window, Profile, UserAuthService, Settings)
	{
		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
			return;
		}

		$window.ga('set', 'page', '/profileView.html');
		$window.ga('send', 'pageview');

		/*
		Ошибки формы профиля
			empty - пустое поле
			wrong - недопустимые символы в поле
		*/
		$scope.profileErrorObject = 
		{
			firstName: null,
			secondName: null,
		};
		//состояния сохранения профиля
		$scope.profileSaving = false;
		$scope.profileSaved = true;
		
		$scope.profile =
		{
			firstName: Profile.profile.firstName,
			secondName: Profile.profile.secondName,
			avaImg: null,
		};
		$scope.saveProfileInfo = function()
		{
			if($scope.profileSaved || $scope.profileSaving)
				return;
			if(!$scope.profile.firstName)
			{
				$scope.profileErrorObject.firstName = 'empty';
				return;
			}
			$scope.profileSaving = true;
			$scope.profileSaved = false;
			Profile.saveProfileInfo($scope.profile,
			function()
			{
				//сброс ошибок форм
				$scope.profileErrorObject.firstName = null;
				$scope.profileErrorObject.secondName = null;
				$scope.profileSaving = false;
				$scope.profileSaved = true;
				
				console.log('yep');
			},
			function()
			{
				//error
				console.log('error while updating profile info');
				$scope.profileSaving = false;
				$scope.profileSaved = false;
			});
		}
		
	}]).
	controller('passViewCtrl',['$scope', '$location', '$window', 'Profile', 'UserAuthService', 'Settings', function($scope, $location, $window, Profile, UserAuthService, Settings)
	{
		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
			return;
		}

		$window.ga('set', 'page', '/passView.html');
		$window.ga('send', 'pageview');

		$scope.profile = 
		{
			currentPassword: null,
			newPassword: null
		};
		/*
		Ошибки формы смены пароля
			empty - пустое поле
			wrong - недопустимые символы в поле
		*/
		$scope.passwordErrorObject =
		{
			currentPassword: null,
			newPassword: null,
		};
		//состояние смены пароля
		$scope.passwordChanging = false;
		$scope.passwordChanged = false;
		
		$scope.changePassword = function()
		{
			if($scope.passwordChanged || $scope.passwordChanging)
				return;
			if(!$scope.profile.currentPassword || !$scope.profile.newPassword)
			{
				if(!$scope.profile.currentPassword) $scope.passwordErrorObject.currentPassword = 'empty';
				if(!$scope.profile.newPassword) $scope.passwordErrorObject.newPassword = 'empty';
				return;
			}
			$scope.passwordChanged = false;
			$scope.passwordChanging = true;
			Profile.changePassword($scope.profile.currentPassword, $scope.profile.newPassword,
				function()
				{
					//сброс ошибок форм
					$scope.passwordChanging = false;
					$scope.passwordChanged = true;
					$scope.passwordErrorObject.currentPassword = null;
					$scope.passwordErrorObject.newPassword = null;
				},
				function(error)
				{
					if(error.error_code == Settings.ERRCODE.ACC.WRONG_PASS)
						$scope.passwordErrorObject.currentPassword = 'wrong';
					$scope.passwordChanging = false;
					$scope.passwordChanged = false;
				}
			);
		}
	}]);