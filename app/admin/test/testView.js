'use strict'
angular.module('IQApp.testView',['ngRoute','tests.adminTest','userAuthService', 'mediaService', 'helpers']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/test',
		{
			templateUrl: 'test/testView.html',
			controller: 'testViewCtrl',
			reloadOnSearch: false
		});
	}
	]).
	controller('testViewCtrl',['$scope', '$rootScope', '$location', '$timeout', '$interval', '$modal', '$routeParams' , '$window', 'AdminTest', 'UserAuthService', 'Tutorial',
	function($scope, $rootScope, $location, $timeout, $interval, $modal, $routeParams, $window, AdminTest, UserAuthService, Tutorial)
	{
		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
			return;
		}

		$window.ga('set', 'page', '/testView.html');
		$window.ga('send', 'pageview');

		$scope.saving = null; //процесс сохранения теста на сервер
		$scope.saveFailed = null; //сохранение проведено неуспешно
		$scope.list = null;
		$scope.test = null;
		$scope.selectedGroup = null; //выбранный вопрос
		//некорректные элементы теста (см. ответы метод save)
		$scope.error =
		{
			group: null,
			question: null,
			answer: null,
		}
		$scope.state = 'loading';
		var test = AdminTest;
		//флаг автосохранения
		var autoSave = true;
		//задержка сохранения после очередного изменения
		var autoSaveTime = 5000;
		//таймер сохранения
		var autoSaveTimer  = null;

		if($routeParams.id)
			test.initTest($routeParams.id, successInit, initFailed);
		else
			test.initTest(null,successInit, initFailed);
		
		function successInit()
		{
			console.log('successInit');
			test.openTest(
				function()
				{
					$scope.state = 'loaded';
					$scope.test = test;
					$scope.list = test.list;
					var groups = $scope.list.groups
					var question_id = $location.hash().substr(1);
					var selectedGroup;
					if(question_id)
						for(var i in groups)
							if(question_id == groups[i].group_id)
							{
								selectedGroup = groups[i];
								break;
							}
					if(!selectedGroup)
						$scope.changeGroup(groups[0]);
					else
						$scope.changeGroup(selectedGroup);
					initInterface();
					if(Tutorial.current)
						Tutorial.go('testCreation');
					//просмотр изменений состояния сохранения
					$scope.$watch('test.saved',function(saved)
					{
						if(!saved)
							if(autoSave && !autoSaveTimer)
								autoSaveTimer = $interval(
									function()
									{
										if(!AdminTest.checkError())
										{
											$scope.save(true);
											$interval.cancel(autoSaveTimer);
											autoSaveTimer = null;
										}
									}, autoSaveTime);
					});
					$scope.$on('$locationChangeStart', function(e, newUrl)
					{
						//осуществляется переход на другую страницу (а не на текущую с дргуим вопросом)
						if(!AdminTest.saved)
							if(newUrl.indexOf('/test') == -1)
								if(!confirm('Изменения не сохранятся. Все равно перейти?'))
									e.preventDefault();
					});
					$scope.$on('$locationChangeSuccess', function()
					{
						//если страница другая, то удалить таймер автообновления
						if($location.path() != '/test')
							if(autoSave && autoSaveTimer)
							{
								$interval.cancel(autoSaveTimer);
								autoSaveTimer = null;
							}
					});
				},
				function()
				{
					$scope.state = 'failed';
					$location.path('/manage');
				});
		}
		function initFailed()
		{
			$scope.state = 'failed';
			$location.path('/manage');
		}
		
		function initInterface()
		{
			/*
			* Очередь, кнопка "добавить"
			*/
			angular.element(document).ready(function()
			{
				queueResize();
				window.onresize = queueResize;
			});
				

		}
		function queueResize()
		{
			try
			{
				var queue = document.getElementById('groups-list')
					,queue_add = document.getElementById('group-add')
					,queue_bar = document.getElementById('left-bar')

				var queue_height = queue_bar.offsetHeight - queue_add.offsetHeight-120;
				queue.style['max-height'] = queue_height+'px';
			}
			catch(e){};
		}
		$scope.changeGroup = function(group)
		{
			if(!group)
				return;
			if($scope.selectedGroup != group)
				$scope.selectedGroup = group;
			$location.hash('q'+group.group_id);
			$location.replace();
		};
		$scope.addQuestion = function(groupId)
		{
			test.createQuestion(groupId);
		};
		$scope.copyQuestion = function(groupId, questionId)
		{
			test.copyQuestion(groupId, questionId);
		}
		$scope.delQuestion = function(id)
		{
			test.deleteQuestion(id);
		}
		$scope.addGroup = function()
		{
			var group = test.createGroup();
			if(group) $scope.changeGroup(group);
			queueResize();
		};
		$scope.copyGroup = function(groupId)
		{
			var group = test.copyGroup(groupId);
			$scope.changeGroup(group);
		};
		$scope.delGroup = function(id)
		{
			test.deleteGroup(id);
			if($scope.selectedGroup.group_id == id)
				if($scope.list.groups[0])
					$scope.changeGroup($scope.list.groups[0]);
				else
					$scope.selectedGroup = null;
		};
		$scope.addAnswer = function(questionId)
		{
			test.createAnswer(questionId);
		}
		$scope.delAnswer = function(id)
		{
			test.deleteAnswer(id);
		};
		$scope.moveGroupUp = function(group)
		{
			if(group.position - 1 > 0)
			{
				test.moveGroupTo(group.group_id,  group.position-1);
				//$location.hash('gid'+group.group_id);
			}
		};
		$scope.moveGroupDown = function(group)
		{
			if(group.position + 1 <= test.list.groups.length)
			{
				test.moveGroupTo(group.group_id, group.position + 1);
				//$location.hash('gid'+group.group_id);
			}
		};
		$scope.replace = function(groupId, position)
		{
			test.moveGroupTo(groupId, parseInt(position));
		};
		$scope.dataChanged = function(item, id, type)
		{
			if(item == 'answer')
			{
				test.answerChanged(id, type);
				if(id == $scope.error.answerId)
					$scope.error.answerId = null;
			}
			else
			if(item == 'question')
			{
				test.questionChanged(id, type);
				/* if(id == $scope.questionId)
					$scope.error.questionId = null; */
			}

		};

		$scope.editQuestionText = function(question)
		{
			var modalInstance = $modal.open(
			{
				animation: true,
				templateUrl: 'test/modals/editQuestionTextModal.html',
				controller: 'editQuestionTextCtrl',
				backdrop: 'static',
				size: 'lg',
				resolve: {text: function(){return getQuestionText(question.text)}}
			});
			modalInstance.result.then(
			function(htmlText)
			{
				question.text = getQuestionImage(question.text)+htmlText;
				$scope.dataChanged("question",question.question_id,"text");
			},
			function()
			{
				
			});
		};
		$scope.save = function()
		{
			$scope.saving = true;
			$scope.saveFailed = null;
			test.save(
			function()
			{
				console.log('saved');
				$scope.saving = false;
				$scope.error.answer = null;
				$scope.error.question = null;
				$scope.error.group = null;
				if($routeParams.id != test.test.test_id)
					$location.search('id',test.test.test_id).replace();
				if(Tutorial.current == 'testCreation')
					Tutorial.go('finishMessage', function()
					{
						Tutorial.finish();
					});
			},
			function(error)
			{
				$scope.saving = false;
				console.log('err',error.error_code)
				switch(error.error_code)
				{
					case test.ERR_CODE.NO_QUESTIONS:
						$scope.error.group = error.data.group;
						$scope.saveFailed = 'Нет вариантов ответов в вопросе';
					break;
					case test.ERR_CODE.NO_ANSWERS:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.saveFailed = 'Нет ответов для вопроса';
					break;
					case test.ERR_CODE.NOT_ENOUGH_ANSWERS:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.saveFailed = 'Должно быть не менее двух ответов';
					break;
					case test.ERR_CODE.MORE_THAN_ONE_CORRECT:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.saveFailed = 'Должен быть только один правильный ответ';
					break;
					case test.ERR_CODE.NOT_ENOUGH_CORRECT_ANSWERS:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.saveFailed = 'Должно быть больше одного правильного ответа';
					break;
					case test.ERR_CODE.NO_ANSWER_TEXT:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.error.answer = error.data.answer;
						$scope.saveFailed = 'Нет текста ответа';
					break;
					case test.ERR_CODE.NO_QUESTION_TEXT:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.saveFailed = 'Нет текста вопроса';
					break;
					case test.ERR_CODE.NO_CORRECT_ANSWERS:
						$scope.error.group = error.data.group;
						$scope.error.question = error.data.question;
						$scope.error.answer = error.data.answer;
						$scope.saveFailed = 'Нет ни одного корректного ответа в вопросе';
					break;
					case test.ERR_CODE.NO_GROUPS:
						$scope.saveFailed = 'Отсутствуют вопросы';
					break;
					default:
						$scope.saveFailed = 'Ошибка сохранения';
				}
				if($scope.error.group)
					$scope.changeGroup($scope.error.group);
			}
			);
		};
		$scope.renameTest = function()
		{
			
		};
		$scope.onDropComplete = function (index, obj, evt) {
			if(obj)
				$scope.replace(obj.group_id, index+1);
		};
		$scope.closeAlert = function()
		{
			$scope.saveFailed = false;
		}
		//обработка перехода к другой странице: если тест/опрос не сохранен, то предупреждать
		/* $scope.$on('$locationChangeStart',
		function(e)
		{
			if(!$scope.test.saved)
				if(!confirm('Изменения не сохранены. Продолжить?'))
				{
					e.preventDefault();
				}
		}); */
	}
	]).
	controller('editQuestionTextCtrl', ['$scope', 'text', '$modalInstance', function($scope, text, $modalInstance)
	{
		$scope.htmlText = text;
		$scope.isEmpty = function()
		{
			return $scope.htmlText.charCodeAt(0) == 10 && $scope.htmlText.charCodeAt(1) == 9 && $scope.htmlText.length == 2;
		}
		$scope.ok = function()
		{
			if($scope.isEmpty())
				$scope.htmlText = '';
			$modalInstance.close($scope.htmlText);
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		};
		//обработчик перехода между страницами
		$scope.$on('$locationChangeStart',
		function(e)
		{
			e.preventDefault();
			$scope.cancel();
		});
		
	}])