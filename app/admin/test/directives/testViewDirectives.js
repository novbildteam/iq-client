(function(){

angular.module('IQApp.testViewDirectives',['mediaService']).
directive("fileread", [function () {
    return {
        scope: {
			onChanged: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
				if(scope.onChanged)
					scope.onChanged(changeEvent.target.files[0]);
            });
        }
    }
}]).
directive("dropzone", [function () {
	return {
		scope:
		{
			onDrop: "=",
		},
		restrict: 'A',
		link: function (scope, element)
		{
			element.bind("drop", function(changeEvent)
			{
				
				changeEvent.stopPropagation();
				changeEvent.preventDefault();

				if(scope.onDrop)
					scope.onDrop(changeEvent.dataTransfer.files[0]);
				element.css({
					'border-style': 'none',
				});
			});
			element.bind('dragover', function (e) {
				e.stopPropagation();
				e.preventDefault();
				element.css({
					'border-style': 'dotted',
				});
			});
			element.bind('dragenter', function(e) {
				e.stopPropagation();
				e.preventDefault();

			});
			element.bind('dragleave', function(e) {
				e.stopPropagation();
				e.preventDefault();
				element.css({
					'border-style': 'none',
				});
			});
		}
	}
}]).
directive("questionContent", ['$modal', 'MediaService', function($modal, MediaService){
	return {
		templateUrl: 'test/directives/question-content.html',
		restrict: 'E',
		scope:
		{
			question: '=question', //объект вопроса
			dataChanged: '=onDataChanged', //функция, вызываемая после изменения содержимого вопроса (текста или изображения)
		},
		link: function(scope, element, attributes)
		{
			scope.text = getQuestionText(scope.question.text); //текст из scope.question в html
			scope.image = getQuestionImage(scope.question.text); //изображение из scope.question
			scope.$watch('text', function(htmlText)
			{
				if(htmlText == getQuestionText(scope.question.text))
					return;
				if(scope.image)
					scope.question.text = htmlText+'<img src="'+scope.image+'"></img>';
				else
					scope.question.text = htmlText;
				scope.dataChanged("question",scope.question.question_id,"text");
			});
			scope.insertImage = function(method)
			{
				var modalInstance = $modal.open(
				{
					animation: true,
					templateUrl: 'test/modals/insertImageModal.html',
					controller: 'insertImageCtrl',
					backdrop: 'static',
					size: 'lg',
					resolve:
					{
						imageURL: function(){return scope.image},
						method: function(){return method},
					}
				});
				modalInstance.result.then(
				function(url)
				{
					if(!url)
						return;
					scope.image = url;
					if(!scope.text)
						scope.question.text = '<img src="'+url+'">';
					else
						scope.question.text = scope.text + '<img src="'+url+'">';
					scope.dataChanged("question",scope.question.question_id,"text");
				},
				function()
				{
				});
			};
			scope.deleteImage = function()
			{
				scope.image = null;
				scope.question.text = scope.text;
				scope.dataChanged("question",scope.question.question_id,"text");
			};
			scope.uploadImage = function(image)
			{
				if(!image)
					return;
				scope.loadingImage = true;
				MediaService.uploadImage(image,
					function(data)
					{
						scope.loadingImage = false;
						scope.imageLoaded = 'successful';
						console.log(data);

						scope.image = data.image_url;
						if(!scope.text)
							scope.question.text = '<img src="'+scope.image+'">';
						else
							scope.question.text = scope.text + '<img src="'+scope.image+'">';
						scope.dataChanged("question",scope.question.question_id,"text");
					},
					function(error)
					{
						scope.loadingImage = false;
						scope.imageLoaded = 'failed';
						console.log('error while loading image');
						//ошибка при загрузке
					});
			};
			
		}
	}
}]).
directive("groupContent", ['$modal', function($modal){
	return {
		templateUrl: 'test/directives/group-content.html',
		restrict: 'E',
		scope:
		{
			group: '=group',
			selectedGroup: '=selectedGroup',
			error: '=error',
			delGroup: '=onDelete',
			copyGroup: '=onCopy',
			moveGroupUp: '=onMoveUp',
			moveGroupDown: '=onMoveDown'
		},
		link: function(scope,element,attributes)
		{
			scope.$watch('group.questions[0]', function(val)
			{
				scope.image = getQuestionImage(scope.group.questions[0].text);
				scope.text = getQuestionText(scope.group.questions[0].text, true);
			},true);
			
		}
	};
}]).
controller('insertImageCtrl', ['$scope', '$modalInstance', 'MediaService', 'imageURL', 'method', function($scope, $modalInstance, MediaService, imageURL, method)
{
	/*
	method - 'url' - загрузка по ссылке
			'lib' - загрузка из библиотеки
	 */
	$scope.image = {imageFile: null, imageURL: imageURL}; //загружаемое изображение
	$scope.method = method;
	if($scope.method == 'url')
		$scope.image.imageURL = null
	//URL изображения (загруженного или выбранного из библиотеки)
	$scope.uploadImage = function(image)
	{
		if(!image)
			return;
		$scope.loadingImage = true;
		MediaService.uploadImage(image,
		function(data)
		{
			$scope.loadingImage = false;
			$scope.imageLoaded = 'successful';
			console.log(data);
			$scope.image.imageURL = data.image_url;
		},
		function(error)
		{
			$scope.loadingImage = false;
			$scope.imageLoaded = 'failed';
			console.log('error while loading image');
			//ошибка при загрузке
		});
	};

	$scope.ok = function()
	{
		$scope.image.imageFile = null;
		$modalInstance.close($scope.image.imageURL);
	};
	$scope.getImagesList = function()
	{
		
			$scope.loadingList = true;
			MediaService.getImagesList(
			function(data)
			{
				$scope.imagesList = data;
				$scope.loadingList = false;
				$scope.listLoaded = 'successful';
			},
			function()
			{
				$scope.listLoaded = 'failed'
				$scope.loadingList = false;
			});
		
	};
	$scope.cancel = function()
	{
		$scope.image.imageFile = null;
		$modalInstance.dismiss('cancel');
	};
	//обработчик перехода между страницами
	$scope.$on('$locationChangeStart',
	function(e)
	{
		e.preventDefault();
		$scope.cancel();
	});
	
}]);

function getQuestionText(htmlText, onlyString)
{
	var tmp = document.createElement('tmp');
	var tmp2 = document.createElement('tmp2');
	tmp.innerHTML = htmlText;
	var start = 0;
	var end;
	var tmpHtmlText = htmlText;
	while(true)
	{
		var lowHtmlText = tmpHtmlText.toLowerCase()
		start = lowHtmlText.indexOf('<img');
		if(start == -1)
			break;
		end = lowHtmlText.indexOf('>',start);
		tmpHtmlText = tmpHtmlText.substr(0,start)+tmpHtmlText.substr(end+1);
	}
	tmp2.innerHTML = tmpHtmlText;
	if(!onlyString)
		return tmp2.innerHTML;
	else
		return tmp2.textContent;
	// getText(tmp, tmp2);
	// if(!onlyString) {
	// 	return tmp2.innerHTML;
	// }
	// else {
	// 	return tmp2.textContent;
	// }
	// function getText(m, images)
	// {
	// 	while(m.children.length)
	// 	{
	// 		if(m.children[0].nodeName != 'IMG')
	// 			images.appendChild(m.children[0]);
	// 		else
	// 			m.removeChild(m.children[0]);
	// 		if(m.children[0])
	// 			getText(m.children[0], images);
	// 	}
	// }
}

function getQuestionImage(htmlText)
{
	var tmp = document.createElement('tmp');
	var tmp2 = document.createElement('tmp2');
	tmp.innerHTML = htmlText;
	getImage(tmp,tmp2);
	//return tmp2.innerHTML;
	if(tmp2.firstChild)
		return tmp2.firstChild.src;
	else
		return null;
	
	/* function getImage(m, images)
	{
		for(var m = n.firstChild; m != null; m = m.nextSibling)
		{
			if(m.children[0].nodeName == 'IMG')
				images.appendChild(m.children[0]);
			else
				images.removeChild(m.children[0]);
			getImage(m, images);
		}
	} */
	function getImage(m, images)
	{
		while(m.children.length)
		{
			if(m.children[0].nodeName == 'IMG')
				images.appendChild(m.children[0]);
			else
				m.removeChild(m.children[0]);
			if(m.children[0])
				getImage(m.children[0], images);
		}
	}		
}




})()