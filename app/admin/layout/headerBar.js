angular.module('IQApp.headerBar',['userAuthService', 'profileService','room.adminRoom', 'streamsReports', 'lessonsReports', 'helpers']).
controller('HeaderBarCtrl', ['$scope', '$location', 'UserAuthService', 'Profile', 'AdminRoom', 'StreamsReports', 'LessonsReports', 'Tutorial',
	function($scope, $location, UserAuthService, Profile, AdminRoom, StreamsReports, LessonsReports, Tutorial)
{
	$scope.profile = Profile.profile;
	$scope.adminRoom = AdminRoom;
	//connect();

	$scope.logOut = function()
	{
		UserAuthService.logOutSystem(UserAuthService.getUserToken(),
		function()
		{
			StreamsReports.clearCart(); //очистка "корзины" отчетов
			LessonsReports.clear();
			Profile.reset();
			AdminRoom.reset();
			Tutorial.cancel();
			$location.path('/auth');
		},
		function()
		{
			console.log('error while logOut');
		});
	}
	// function connect()
	// {
	// 	if(!AdminRoom.connected)
	// 	{
	// 		AdminRoom.init(
	// 			function()
	// 			{
    //
	// 			},
	// 			function()
	// 			{
	// 				$scope.state = 'failed';
	// 			});
	// 	}
	// }
}]);