angular.module('IQApp.tutorialBar',['userAuthService', 'helpers']).
controller('TutorialBarCtrl', ['$scope', '$location', 'UserAuthService', 'Tutorial', function($scope, $location, UserAuthService, Tutorial)
{
    $scope.tutorial = Tutorial;
    $scope.showTutorial = function()
    {
      Tutorial.repeat();
    };
}]);
