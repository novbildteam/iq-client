'use strict'
angular.module('IQApp.roomView', ['ngRoute', 'room.adminRoom', 'fsExplorer', 'profileService', 'userAuthService', 'helpers']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/room',
		{
			//templateUrl: IQHelpers.isMobile() ? 'room/roomViewMobile.html' : 'room/roomView.html',
			templateUrl: 'room/roomView.html',
			controller: 'roomViewCtrl'
		});
	}
	]).
	controller('roomViewCtrl', ['$scope', '$document', '$interval', '$timeout', '$location', '$routeParams', '$modal', '$window', 'AdminRoom', 'AdminRoomResults', 'FsExplorer', 'Profile', 'UserAuthService', 'Tutorial',
	function($scope, $document, $interval, $timeout, $location, $routeParams, $modal, $window, AdminRoom, AdminRoomResults, FsExplorer, Profile, UserAuthService, Tutorial)
	{
		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
			return;
		}

		$window.ga('set', 'page', '/roomView.html');
		$window.ga('send', 'pageview');

		var LS = window.localStorage;
		
		$scope.adminRoom = AdminRoom;
		$scope.adminRoomResults = AdminRoomResults;
		$scope.results = AdminRoomResults.results;
		$scope.currentTest = AdminRoom.currentTest;
		$scope.interval = $interval;
		//флаг для отображения ождидания запуска теста
		$scope.waitForTestActivated;
		//флаг для отображения ожидания окончания теста
		$scope.waitForTestEnded;
		//флаг ожидания окончания урока
		$scope.waitForLessonFinished;
		//флаг ожидания активации следующего вопроса (для pace_mode = 2)
		$scope.waitForQuestionEnabled;
		//флаг ожидания окончания текущего вопроса (для pace_mode = 2)
		$scope.waitForQuestionFinished;


		//тип визуализации
		$scope.visType = 'chart'; //'list', 'users'
		//состояние файловой системы
		$scope.fsState = null;
		//режим презентации
		

		/* $scope.numberOfQuestions = function()
		{
			if(AdminRoom.currentTest.test != null)
				return new Array(AdminRoom.currentTest.test.questions);
			else
				return [];
		} */
		$scope.connect = connect; //функция подключения к комнате	
		$scope.quizResult = []; //для визуализации результатов опросов (question.type = 1)
		
		//UI
		initUI();

		/* $scope.getWidth = function()
		{
			return window.innerWidth;
		};
		$scope.$watch($scope.getWidth, function(newValue, oldValue)
			{
				if (newValue < mobileView)
					$scope.showQueue = false;
				else
					$scope.showQueue = true;
			}); */
		//END-UI
		
		var queue = AdminRoom.queue;
		
		var tryReconnectTimerInterval; //счетчик обратного отсчета переподключения
		connect();
		
		function connect()
		{
			if(!AdminRoom.connected)
			{
				$scope.state = 'initializing';
				AdminRoom.init(
				function()
				{
					init();
				},
				function()
				{
					$scope.state = 'failed';
				});
			}
			else
				init();
		}
		
		function init()
		{
		
			
			
			/*
			* Очередь, кнопка "добавить"
			*/
			angular.element(document).ready(function()
			{
				queueResize();
				window.onresize = queueResize;
			});
			
				function queueResize()
				{
					try
					{
						var queue = document.getElementById('queue-list')
							,queue_add = document.getElementById('queue-add')
							,queue_bar = document.getElementById('left-bar')
							,userList = document.getElementById('user-list');
						
						var queue_height = queue_bar.offsetHeight - queue_add.offsetHeight - queue.offsetTop - 60;
						var userListHeight = queue_bar.offsetHeight - userList.offsetTop - 60;
						queue.style['max-height'] = queue_height+'px';
						console.log('userListHeight',userListHeight);
						userList.style['max-height'] = userListHeight+'px';
						// if (window.innerWidth < $scope.mobileView)
						// 	$scope.showQueue = false;
						$scope.$apply();
					}
					catch(e){}
				} 

			console.log('init successfully');

			//режим туториала?
			if(Tutorial.current == 'lessonCreation')
			{
				Tutorial.go('lessonCreated');
			}

			//подсчет кол-ва активных пользователей в комнате
			$scope.$watch('adminRoom.users',function(val)
			{
				var count = 0;
				for(var i in $scope.adminRoom.users)
					if($scope.adminRoom.users[i].del_reason == null)
						count++;
				$scope.countOfOnlineUsers = count;
			}, true);
			if(AdminRoom.config.status)
				$scope.state = 'initialized';
			else
				$scope.state = 'off';
			$scope.explorer = new FsExplorer();
			$scope.openFolder = function(folderId)
			{
				$scope.fsState = 'loading';
				$scope.explorer.open(folderId,
				function()
				{
					console.log('folder opened');
					$scope.fsState = 'loaded';
				},
				function()
				{
					$scope.fsState = 'failed';
					console.log('err while opening folder');
				});
			};
			$scope.reconnect = function()
			{
				$interval.cancel(tryReconnectTimerInterval);
				$scope.reconnectState='reconnecting';
				$interval(function()
					{
						if(!AdminRoom.connected)
							AdminRoom.init(
							function()
							{
								$scope.reconnectState = null;
								$scope.state = 'initialized';
							},
							function()
							{
								$scope.reconnectState = 'failed';
								$scope.tryReconnect();
							});
					},1000,1);
			};
			
			$scope.tryReconnect = function()
			{
				$scope.tryReconnectTimer = 30; //в секундах
				var timer = $scope.tryReconnectTimer;
				tryReconnectTimerInterval = $interval(function()
				{
					$scope.tryReconnectTimer--;
					if($scope.tryReconnectTimer == 0)
						$scope.reconnect();
				}, 1000,timer);
			};
			
			$scope.refresh = function()
			{
				if($scope.reconnectState == 'reconnecting')
					return;
				$scope.reconnectState='reconnecting';
				AdminRoom.init(
					function()
					{
						$scope.reconnectState = null;
						$scope.state = 'initialized';
					},
					function()
					{
						$scope.reconnectState = 'failed';
					});
			};
			$scope.addTest = function(test, pos, success, fail)
			{
				//test - элемент из списка explorer-а, поэтому его необходимо привести к виду объекта теста
				//(см. api/tests/getInfo) + test_id
				var tmp =
				{
					test_id: test.test_id,
					owner_id: test.owner_id,
					name: test.name,
					type: test.test_type,
					questions: test.questions,
					timer: test.timer,
					random: test.random,
					creation_time: test.creation_time,
					editing_time: test.editing_time,
					description: test.description,
					branch_id: test.branch_id
				};
				if(pos == undefined || pos == null)
					pos = AdminRoom.queue.list.length+1;
				// AdminRoom.putIntoRoom(tmp, AdminRoom.queue.list.length+1, null, success, fail);
				AdminRoom.putIntoRoom(tmp, pos, null, success, fail);
			};
			$scope.deleteTest = function(position, testId, success, fail)
			{
				console.log('preparing to delete '+position);
				AdminRoom.removeFromRoom(position, testId, null, success, fail);
			};
			$scope.resume = function(pos)
			{
				if(AdminRoom.testState == 'active')
					$scope.cancelTest();

				var modalInstance = $modal.open(
				 {
					animation: true,
					templateUrl: 'room/resumeModal.html',
					controller: 'resumeCtrl',
					scope: $scope,
				 	size: 'lg',
					resolve:
					{
						showResults: function(){return AdminRoom.currentTest.showResults;},
						test: function()
						{
							if(!pos)
								return AdminRoom.queue.list[0];
							return AdminRoom.queue.list[pos-1];
						},
					}
				});
				modalInstance.result.then(function (result)
				{
					if(pos > 1)
					{
						AdminRoom.resume(result.paceMode, result.options, AdminRoom.queue.list[pos - 1].test_id);
						$scope.deleteTest(pos, AdminRoom.queue.list[pos-1].test_id);
					}
					else
						AdminRoom.resume(result.paceMode, result.options);
					$scope.waitForTestActivated = true;
				},
				function ()
				{				  
				}
				);
			};
			$scope.startLesson = function(modal)
			{
				if(!modal)
				{
					AdminRoom.startLesson({});
					return;
				}
				//AdminRoom.startLesson({title: 'test'+Math.random()});
				var modalInstance = $modal.open(
					{
						animation: true,
						templateUrl: 'lessons/createLessonModal.html',
						controller: 'createLessonCtrl',
						scope: $scope,
					});
				modalInstance.result.then(
					function(result)
					{
						AdminRoom.startLesson({title: result.title,id: result.id, description: result.description});
						$location.path('/room');
					},
					function(){}
				);
			};
			$scope.endLesson = function()
			{
				AdminRoom.endLesson();
				$scope.waitForLessonFinished = true;
			};
			$scope.restartLesson = function()
			{
				$scope.waitForLessonFinished = true;
				if(AdminRoom.lessonState == 'active')
				{
					AdminRoom.endLesson(function ()
						{
							AdminRoom.startLesson({});
						},
						function ()
						{
							console.log('err while restarting lesson');
						})
				}
				else
				{
					AdminRoom.startLesson({});
				}
			};

			$scope.finishTest = function()
			{
				AdminRoom.finishTest();
				$scope.waitForTestEnded = true;
			};
			$scope.cancelTest = function()
			{
				AdminRoom.cancelTest();
				$scope.waitForTestEnded = true;
			};
			$scope.override = function(obj, position)
			{
				AdminRoom.overrideTest(obj, position);
			};
			$scope.changeStatus = function(val)
			{
				AdminRoom.changeStatus(val);
			};
			AdminRoom.events.room.onChangedStatus = function()
			{
				// if(AdminRoom.config.status == true)
				// 	$scope.refresh();
				// else
				// 	$scope.state = null;
			};
			AdminRoom.events.room.connectionLost = function()
			{
				$scope.reconnect();
			};
			AdminRoom.events.users.onResult = function()
			{
				if(Tutorial.current == 'activeTest')
					Tutorial.go('answeredQuestion', null, 1000);
			};
			AdminRoom.events.queue.onAdded = function()
			{
			};
			AdminRoom.events.queue.onRemove = function()
			{
			};
			$scope.killStudent = function(id)
			{
				AdminRoom.killStudent(id);
			};
			$scope.addActivity = function()
			{
				if(AdminRoom.lessonState != 'active')
					return;
				exitFullScreen();
				$scope.explorer.open(Profile.profile.rootId,
					function()
					{
						console.log('root dir opened');
						$scope.fsState = 'loaded';
					},
					function(error)
					{
						console.log('error while opening root dir');
						$scope.fsState = 'failed';
					}
				);
				var modalInstance = $modal.open({
					animation: true,
					templateUrl: 'room/addActivityModal.html',
					controller: 'activityCtrl',
					scope: $scope,
					resolve:
					{
						superCtrl: function () {return $scope;}
					}
				});
				modalInstance.result.then(function(activity)
				{
					if(AdminRoom.testState == 'active')
						$scope.cancelTest();

					if(activity)
					{
						var answers = [];
						if (activity.answers)
						{
							for (var i = 0; i < activity.answers.length; i++)
								if (activity.answers[i].text)
									answers.push(activity.answers[i].text);
						}
						var activityItem = {
							text: activity.text,
							answers: answers
						};
						AdminRoom.createActivity(activity.type, activityItem,
						function success(testId)
						{
							// начать activity
							// пока что presenter-paced
							AdminRoom.resume(1, {showResults: true}, testId);
						},
						function fail(error)
						{
							console.log('error while creating activity, error_code');
						});
					}
					else
					{
						$scope.resume();
					}
				});
			};
			$scope.openExplorer = function(size)
			{
				$scope.fsState = 'loading';
				$scope.showQueue = true;
				$scope.explorer.open(Profile.profile.rootId,
					function()
					{
						console.log('root dir opened');
						$scope.fsState = 'loaded';
					},
					function(error)
					{
						console.log('error while opening root dir');
						$scope.fsState = 'failed';
					}
				);
				 var modalInstance = $modal.open(
				 {
					 animation: true,
					 templateUrl: 'explorerModal.html',
					 controller: 'explorerCtrl',
					 size: size,
					 scope: $scope,
					 resolve:
					 {
						superCtrl: function () {return $scope;}
					 }
				});
				modalInstance.result.then(result, result);
				function result()
				{
					if(Tutorial.current == 'lessonCreated' && $scope.adminRoom.queue.list.length > 0)
					{
						Tutorial.go('startTest');
					}
				}

			};
			$scope.editAccessSettings = function(size)
			{
				exitFullScreen();
				var modalInstance = $modal.open(
				{
					 animation: true,
					 templateUrl: 'accessSettingsModal.html',
					 controller: 'accessSettingsCtrl',
					 size: size,
					 scope: $scope,
					
				});
			};
			$scope.explorerClick = function(item, $event, scope)
			{
				if(item.item_type == 0)
					$scope.openFolder(item.folder_id);
				else
				if(item.item_type == 1 || item.item_type == 2)
				{
					$scope.addTest(item);
					// if($event)
					// 	angular.element($event.currentTarget).addClass('active');
					// console.log(scope);
				}
			};
			$scope.editTest = function(test, pos)
			{
				$scope.selectedTest = test;
				var modalInstance = $modal.open(
				 {
					animation: true,
					templateUrl: 'testModal.html',
					controller: 'testCtrl',
					scope: $scope,
					resolve:
					{
						test: function(){return test;},
					}
				});
				modalInstance.result.then(function (override)
				{
					console.log(override);
					if(override && (override.timer != null || override.random != null))
					{
						if(!test.override)
							test.override = {};
						if(override.timer != null)
							test.override.timer = override.timer;
						if(override.random != null)
							test.override.random = override.random;
						$scope.override(override, pos);
					}
				},
				function ()
				{				  
				}
				);
			};
			$scope.showQuestion = function(result, user)
			{
				if(!result.question_id)
					return;
				var modalInstance = $modal.open(
				{
					animation: true,
					templateUrl: 'viewQuestionModal.html',
					controller: 'viewQuestionCtrl1',
					scope: $scope,
					size: 'lg',
					resolve:
					{
						testType: function(){return AdminRoom.currentTest.test.type;},
						result: function(){return result;},
						user: function(){return user},
					}
				});
			};
			$scope.showOnlineUsers = function()
			{
				var modalInstance = $modal.open(
				{
					animation: true,
					templateUrl: 'viewOnlineUsersModal.html',
					controller: 'viewOnlineUsersCtrl',
					scope: $scope,
				});
			};
			$scope.finishQuestion = function()
			{
				$scope.waitForQuestionFinished = true;
				AdminRoom.finishQuestion();
			};
			$scope.enableNextQuestion = function()
			{
				$scope.waitForQuestionEnabled = true;
				AdminRoom.enableNextQuestion();
			};
			$scope.$on('$locationChangeSuccess',function()
			{
				$interval.cancel(tryReconnectTimerInterval);
			});
			AdminRoom.events.test.onActivated = function()
			{
				$scope.waitForTestActivated = false;
				if(Tutorial.current)
					setTimeout(function(){
						Tutorial.go('activeTest');
					},1000);
			};
			AdminRoom.events.test.onCancelled = function()
			{
				$scope.waitForTestEnded = false;
			};
			AdminRoom.events.test.onFinished = function()
			{
				$scope.waitForTestEnded = false;
			};
			AdminRoom.events.lesson.onFinished = function()
			{
				$scope.waitForLessonFinished = false;
			};
			AdminRoom.events.question.onFinished = function()
			{
				$scope.waitForQuestionFinished = false;
			};
			AdminRoom.events.question.onEnabled = function()
			{
				$scope.waitForQuestionEnabled = false;
			};
			AdminRoom.events.queue.onAdded = function(test)
			{

			};
			AdminRoom.events.queue.onRemoved = function(test)
			{

			}
			/*AdminRoom.events.room.changeStatus = function()
			{
				if(AdminRoom.config.status)
					$scope.state = 
			};*/
		}
		function initUI()
		{
			$scope.toggleQueue = function()
			{
				$scope.showQueue = !$scope.showQueue;
				LS['UI.showQueue'] = $scope.showQueue;
			};
			$scope.$watch('presentationMode', function()
			{
				LS['UI.presentationMode'] = $scope.presentationMode;
				var el;
				if($scope.presentationMode)
				{
					//$scope.showQueue = false;
					el = document.getElementById('present');
					fullscreen(el);
				}
				else
				{
					exitFullScreen();
				}
			});

			//проверка выхода из режима презентации стандартным способом (по кнопке ESC или F11)
			//if(document.mozRequestFullScreen)
				angular.element(document).on('mozfullscreenchange', function()
				{
					$scope.presentationMode = document.mozFullScreenElement != null;
					$scope.$apply();
				});
			//else
			//if(document.webkitRequestFullscreen)
				angular.element(document).on('webkitfullscreenchange', function()
				{
					$scope.presentationMode = document.webkitFullscreenElement != null;
					$scope.$apply();
				});

			//UI
			// if(LS['UI.showQueue'] != undefined)
			// 	$scope.showQueue = 'true' === LS['UI.showQueue'];
			// else
			$scope.showQueue = false;
			$scope.presentationMode = false;
			$scope.mobileView = 992;

			$scope.popoverContent = 'popoverContent.html';

		}
		function fullscreen(element) {
			if(element.requestFullScreen) {
				element.requestFullScreen();
			} else if(element.mozRequestFullScreen) {
				element.mozRequestFullScreen();
			} else if(element.webkitRequestFullScreen) {
				element.webkitRequestFullScreen();
			}
		}
		function exitFullScreen()
		{
			if (document.exitFullscreen)
				document.exitFullscreen();
			else if (document.msExitFullscreen)
				document.msExitFullscreen();
			else if (document.mozCancelFullScreen)
				document.mozCancelFullScreen();
			else if (document.webkitExitFullscreen)
				document.webkitExitFullscreen();
		}
		//$scope.words = {'1':1,'2':1,'3':1,'4':1,'5':1,'6':1,'7':1,'8':1,'9':1,'10':1};
		//$scope.words = [{text:'1',count:10},{text:'2',count:1},{text:'2',count:1},{text:'4',count:1}];
	}
	]).
	controller('accessSettingsCtrl', ['$scope', 'AdminRoom', '$modalInstance', function($scope, AdminRoom, $modalInstance)
	{
		$scope.settings = {};
		$scope.settings.access = AdminRoom.config.access;
		$scope.settings.password = AdminRoom.config.password;
		$scope.ok = function ()
		{
			console.log($scope.settings)
			AdminRoom.changeAccess($scope.settings)
			$modalInstance.close();
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		}
	}
	]).
	controller('activityCtrl', ['$scope', '$modalInstance', 'superCtrl', function($scope, $modalInstance, superCtrl)
	{
		// тип активности
		// binary - true/false, poll - выбор ответа, openAnswer - свободный ответ
		// library - выбор из библиотеки тестов/опросов
		$scope.activityType = 'binary';
		// вопрос
		$scope.activityQuestion = {text: ''};
		//список вопросов на голосование (activity type=poll)
		$scope.pollAnswers = [{text:''}, {text:''}, {text:''}];
		// выбранный элемент библиотеки
		$scope.selectedItem = null;
		// сообщение об ошибке
		$scope.error = null;
		$scope.addActivity = function()
		{
			var activity = {};
			switch($scope.activityType)
			{
				case 'binary':
					activity.type = 2;
					activity.text = $scope.activityQuestion.text;
					break;
				case 'poll':
					activity.type = 3;
					activity.text = $scope.activityQuestion.text;
					for(var i=0; i<$scope.pollAnswers.length; i++)
					{
						if(!$scope.pollAnswers[i].text)
						{
							$scope.error = {type: 'no answer'};
							return;
						}
					}
					activity.answers = $scope.pollAnswers;
					break;
				case 'openAnswer':
					activity.type = 4;
					activity.text = $scope.activityQuestion.text;
					break;
				default:
					return;
			}
			$modalInstance.close(activity);
		};
		$scope.removePollAnswer = function(num)
		{
			$scope.pollAnswers.splice(num,1);
		};
		$scope.addPollAnswer = function()
		{
			$scope.pollAnswers.push({text:''});
		};
		$scope.explorerClick = function(item)
		{
			if(item.item_type == 0)
				$scope.openFolder(item.folder_id);
			else
			if(item.item_type == 1 || item.item_type == 2)
			{
				$scope.selectedItem = item;
				$scope.initTest(item);
			}
		};
		$scope.initTest = function(item)
		{
			$scope.waitForQueue = item;
			$scope.addTest(item, 1, function()
			{
				$modalInstance.close();
				$scope.waitForQueue = null;
			},
			function(error)
			{
				$scope.waitForQueue = null;
			});
		}
		$scope.openFolder = function(folderId)
		{
			superCtrl.fsState = 'loading';
			console.log(superCtrl.explorer);
			superCtrl.explorer.open(folderId,
				function()
				{
					console.log('folder opened');
					superCtrl.fsState = 'loaded';
				},
				function()
				{
					superCtrl.fsState = 'failed';
					console.log('err while opening folder');
				});
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss();
		};
	}]).
	controller('explorerCtrl', ['$scope', '$modalInstance', 'superCtrl', function($scope, $modalInstance, superCtrl)
	{
		$scope.waitForQueue = null;
		$scope.addedTests = {};
		var queue = superCtrl.adminRoom.queue.list;
		for(var i in queue)
		{
			var test = queue[i];
			$scope.addedTests[test.test_id] = test;
		}
		$scope.ok = function ()
		{
			$modalInstance.close();
		};
		$scope.cancel = function()
		{
			$modalInstance.close();
		};
		$scope.explorerClick = function(item, $event, scope)
		{
			if($scope.waitForQueue)
				return;
			var queue = superCtrl.adminRoom.queue.list;
			//если тест уже есть в очереди
			if($scope.addedTests[item.test_id])
			{
				$scope.waitForQueue = item;
				//ищем позицию
				var position;
				for(var i=0; i < queue.length; i++)
				{
					if(queue[i].test_id == item.test_id)
					{
						position = i+1;
					}
				}
				//удаляем по выбранной позиции
				superCtrl.deleteTest(position, item.test_id,
					function()
					{
						$scope.waitForQueue = null;
						delete $scope.addedTests[item.test_id];
					},
					function()
					{
						$scope.waitForQueue = null;
					}
				);

				return;
			}
			if(item.item_type == 0)
				superCtrl.openFolder(item.folder_id);
			else
			if(item.item_type == 1 || item.item_type == 2)
			{
				$scope.waitForQueue = item;
				superCtrl.addTest(item,
					function()
					{
						$scope.waitForQueue = null;
						$scope.addedTests[item.test_id] = item;
					},
					function()
					{
						$scope.waitForQueue = null;
					}
				);

				// if($event)
				// 	angular.element($event.currentTarget).addClass('active');
				// console.log(scope);
			}
		};
		$scope.openFolder = function(folderId)
		{
			superCtrl.fsState = 'loading';
			superCtrl.explorer.open(folderId,
				function()
				{
					console.log('folder opened');
					superCtrl.fsState = 'loaded';
				},
				function()
				{
					superCtrl.fsState = 'failed';
					console.log('err while opening folder');
				});
		};
		$scope.addTest = function(test)
		{
			//test - элемент из списка explorer-а, поэтому его необходимо привести к виду объекта теста
			//(см. api/tests/getInfo) + test_id
			var tmp =
			{
				test_id: test.test_id,
				owner_id: test.owner_id,
				name: test.name,
				type: test.test_type,
				questions: test.questions,
				timer: test.timer,
				random: test.random,
				creation_time: test.creation_time,
				editing_time: test.editing_time,
				description: test.description,
				branch_id: test.branch_id
			};
			superCtrl.adminRoom.putIntoRoom(tmp, AdminRoom.queue.list.length+1);
		};
	}
	]).
	controller('testCtrl', ['$scope', '$modalInstance', 'test', function($scope, $modalInstance, test)
	{
		$scope.test = test;
		$scope.timer = test.timer;
		$scope.random = test.random;
		$scope.overrideTimer = $scope.timer;
		$scope.overrideRandom = $scope.random;
		if(test.override && test.override.timer != null)
		{
			$scope.overrideTimer = test.override.timer;
		}
		if(test.override && test.override.random != null)
		{
			$scope.overrideRandom = test.override.random;
		}
		var initTimer = $scope.overrideTimer;
		var initRandom = $scope.overrideRandom;
		$scope.ok = function()
		{
			var override = {};
			if($scope.overrideTimer != initTimer)
				override.timer = $scope.overrideTimer;
			if($scope.overrideRandom != initRandom)
				override.random = $scope.overrideRandom;
			$modalInstance.close(override);
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		}
	}]).
	controller('resumeCtrl', ['$scope', '$modalInstance', 'test', 'showResults', function($scope, $modalInstance, test, showResults)
	{
		$scope.test = test;
		$scope.paceMode = 1; //по-умолчанию режим прохождения audience-paced
		$scope.timer = test.timer;
		$scope.random = test.random;
		$scope.override = {};
		$scope.override.overrideTimer = $scope.timer;
		$scope.override.overrideRandom = $scope.random;
		$scope.options = {showResults: showResults === true};
		if(test.override && test.override.timer != null)
		{
			$scope.overrideTimer = test.override.timer;
		}
		if(test.override && test.override.random != null)
		{
			$scope.overrideRandom = test.override.random;
		}
		var initTimer = $scope.override.overrideTimer;
		var initRandom = $scope.override.overrideRandom;
		$scope.ok = function()
		{
			var options = {};
			if($scope.paceMode === 1)
			{
				var timer = Number($scope.override.overrideTimer);
				var random = $scope.override.overrideRandom;
				if(timer != initTimer)
					options.timer = timer;
				if(random != initRandom)
					options.random = random;
			}
			options.showResults = $scope.options.showResults;
			$modalInstance.close({paceMode: $scope.paceMode, options: options});
		};
		$scope.cancel = function()
		{
			$modalInstance.dismiss('cancel');
		}
	}]).
	controller('viewQuestionCtrl1', ['$scope', '$modalInstance', 'result', 'testType', 'user', function($scope, $modalInstance, result, testType, user)
	{
		var question;
		$scope.result;
		$scope.question;
		$scope.testType = testType;
		$scope.user = user;
		showQuestion(result.question_id);
		
		function showQuestion(questionId)
		{
			$scope.adminRoomResults.getQuestions(result.question_id,
			function(q)
			{
				$scope.question = q[0];
				question = q[0];
				console.log(q)
				var res = {answers: []};
				var ans;
				if(question.type == 1 || question.type == 0)
				{
					for(var i in result.answers)
					{
						ans = result.answers[i];
						if(ans.answer_id == null || ans.answer_id == undefined)
							break;
						console.log(ans);
						for(var j in question.answers)
						{
							if(ans.answer_id == question.answers[j].answer_id)
							{
								res.answers.push(question.answers[j]);
								break;
							}
						}
					}
				}
				else
				{
					for(var i in result.answers)
					{
						ans = result.answers[i];
						res.answers.push(ans);
						for(var j in question.answers)
						{
							if(question.answers[j].text == ans.text)
								{
									ans.correct = true;
									break;
								}
						}
					}
				}
				console.log(question);
				console.log(res);
				$scope.result = res;
			},
			function(error)
			{
				console.log(error);
			});
		}
		$scope.ok = function()
		{
			$modalInstance.close();
		}
		
	}]).
	controller('viewOnlineUsersCtrl', ['$scope','$modalInstance', function($scope, $modalInstance)
	{
		console.log($scope);
		$scope.users = $scope.$parent.adminRoom.users;
		$scope.killStudent = $scope.$parent.killStudent;
		$scope.ok = function()
		{
			$modalInstance.close();
		}
	}]);

