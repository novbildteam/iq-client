(function(){
angular.module('IQApp.roomViewDirectives',['nvd3']).
	directive('studentResults', function()
	{
		return {
			templateUrl: 'room/directives/student-results.html',
			restrict: 'E',
			scope:
			{
				questions: '=questions',
				users: '=users',
				testResults: '=testResults',
				testType: '=testType',
				onResultClick: '=onResultClick',
				onKillStudent: '=onKillStudent',
			},
			link: function(scope, element, attrs)
			{
				//объект с результатами по пользователям для отображения в табличной форме
				scope.results = {};
				/*
					тип фильтров для userFilter:
						first_name - имя
						rate - рейтинг (кол-во звезд)
				*/
				scope.userFilter = {type: null, reverse: false};
				/*
					тип фильтров для groupsFilter:
						correct_rate.value - процент правильных ответов
						position - позиция в тесте
				*/
				scope.groupsFilter = {type: 'position', reverse: false};
				
				scope.settings =
				{
					showNames: true,
					showResults: true,
				};
				
								
				scope.$watch('questions', function(value)
				{
					if(scope.questions != null)
						scope.numberOfQuestions = new Array(scope.questions);
					else
						scope.numberOfQuestions = [];
					for(var i=0; i<scope.questions; i++)
					{
						scope.numberOfQuestions[i] = {position: i+1, correct_rate:{value:0}};
					}
				},true);
				/*scope.$watchGroup(['testResults', 'users'], function(x,y,z)
				{
					updateResult(scope.users, scope.testResults, scope.numberOfQuestions);
				});*/
				//Узкое место, т.к. watch срабатывает дважды!
				scope.$watch('testResults', function(value)
				{
					updateResult(scope.users, scope.testResults, scope.numberOfQuestions);
				}, true);
				scope.$watch('users', function(value)
				{
					updateResult(scope.users, scope.testResults, scope.numberOfQuestions);
				},true);
				scope.orderUsersBy = function(filter)
				{
					if(scope.userFilter.type === filter)
						scope.userFilter.reverse = !scope.userFilter.reverse;
					else
					{
						scope.userFilter.type = filter;
						scope.userFilter.reverse = false;
					}
				};
				scope.orderGroupsBy = function(filter)
				{
					if(scope.groupsFilter.type === filter)
						scope.groupsFilter.reverse = !scope.groupsFilter.reverse;
					else
					{
						scope.groupsFilter.type = filter;
						scope.groupsFilter.reverse = false;
					}
				};
				function updateResult(users, testResults, numberOfQuestions)
				{
					var arr = new Array(numberOfQuestions.length);
					//объект с рейтингом вопроса (по позиции вопроса)
					//инициализация нулями
					var correct_rate = {};
					for(var pos=0;pos<numberOfQuestions.length;pos++)
						correct_rate[pos] = {value: 0};
					var notNull = false; //флаг пустой таблицы
					for(var i in users)
					{
						users[i].rate = 0;
						if(testResults[i])
						{
							//создать строку
							if(!scope.results[i] || scope.results[i].length < numberOfQuestions.length)
							{
								scope.results[i] = new Array(numberOfQuestions.length);
								for (var k = 0; k < numberOfQuestions.length; k++)
									scope.results[i][k] = {position: k + 1, correct_rate:correct_rate[k]};

							}
							for (var j = 0; j < testResults[i].length; j++)
							{
								notNull = true; //таблица не пустая
								var position = testResults[i][j].position - 1; //определяем позицию в списке ответов
								//дублируем информацию из результатов testResults
								scope.results[i][position] = testResults[i][j];
								if (!arr[position]) arr[position] = {correct: 0, all_correct: 0};
								if (testResults[i][j].correct_answers === testResults[i][j].all_correct_answers && testResults[i][j].answers.length === testResults[i][j].all_correct_answers) {
									users[i].rate++;
									arr[position].correct++;
								}
								//else
								// if(testResults[i][j].correct_answers != 0 && testResults[i][j].correct_answers < testResults[i][j].all_correct_answers)
								// 	users[i].rate += .5;
								arr[position].all_correct++;
								if (!correct_rate[position]) //testResults[i][j].group_id
									correct_rate[position] = {value: arr[position].correct / arr[position].all_correct};
								else
									correct_rate[position].value = arr[position].correct / arr[position].all_correct;
								numberOfQuestions[position].correct_rate = correct_rate[position];
								scope.results[i][position].correct_rate = correct_rate[position];
							}
						}
						users[i].rate = users[i].rate/numberOfQuestions.length;
					}
					if(!notNull) //если таблица пустая, очищаем результат
					{
						scope.results = {};
						for (var i = 0; i < numberOfQuestions.length; i++)
							delete numberOfQuestions[i].correct_rate;
					}
				};
			},
		};
	}).
	directive('quizResults', function()
	{
		return {
			templateUrl: 'room/directives/quiz-results.html',
			restrict: 'E',
			scope:
			{
				results: '=results',
			},
			
		};
	}).
	directive('questionsResults', ['$timeout', function($timeout)
	{
		return {
			templateUrl: 'room/directives/questions-results.html',
			restrict: 'E',
			scope: 
			{
				test: '=test',
				results: '=results',
				testType: '=testType',
				testState: '=testState',
				question: '=question',
				questions: '=questions',
				questionState: '=questionState',
				usersOnline: '=usersOnline',
				visType: '=visType',
			},
			link: function(scope, elements, attrs)
			{
				/*scope.currentResults - массив, в котором содержатся ответы (по порядку, в соответствии с номерами позиций)
				*/
				$timeout(function(){$timeout(function(){resizeContent();},0)},0);
				scope.currentQuestion = 1;
				scope.currentResults = [];
				//текущий вариант вопроса
				scope.data = {};
				//showResults - флаг демонстрации результатов на диаграмме
				//config.showResults - флаг демонстрации результатов, управляемый компонентом
				scope.$watch('questionState', function(value)
				{
					if(value == 'enabled')
						scope.config.showResults = false;
					else
					if(value == 'finished' || !value)
						scope.config.showResults = true;
				});
				/*
				 тип фильтров для groupsFilter:
				 correct_rate.value - процент правильных ответов
				 position - позиция в тесте
				 */
				scope.groupsFilter = {type: 'position', reverse: false};
				/* фильтр для groupsFilter (см. выше) */
				scope.orderGroupsBy = function (filter) {
					if (scope.groupsFilter.type === filter)
						scope.groupsFilter.reverse = !scope.groupsFilter.reverse;
					else {
						scope.groupsFilter.type = filter;
						scope.groupsFilter.reverse = false;
					}
				};
				scope.$watch('test', function(value)
				{
					scope.currentQuestion = 1;
				});
				scope.$watch('config.showResults',function(value)
				{
					//scope.options.chart.showValues = value;
					scope.options.chart.showXAxis = value;
					if(!value)
						scope.options.chart.color = function(d){return 0};
					else
						scope.options.chart.color = function(d,i)
						{
							if(d.correct)
								return '#aad406';
							return '#ed861d';
						};
				},true);
				
				scope.$watch('question', function(value)
				{
					// scope.varQuestion = null;
					if(scope.question != null)
						scope.currentQuestion = scope.question;
					else
						scope.currentQuestion = 1;
				});
				
				scope.$watch('currentQuestion', function(value)
				{
					//вариант вопроса
					// if(scope.currentResults[scope.currentQuestion]) {
					// 	var questions = scope.currentResults[scope.currentQuestion].questions;
					// 	scope.varQuestion = Object.keys(questions)[0];
					// }
					// else
					// {
					// 	scope.varQuestion = null;
					// }
					scope.varQuestion = 0;
					initBar();
				});

				scope.$watch('results', function()
				{
					initResults();
				}, true);
				
				scope.getText = getQuestionText;
				scope.getImage = function(text)
				{
					if(text)
					{
						resizeContent();
						return getQuestionImage(text);
					}
				};
				scope.changeVar = function(q_id) {
					scope.varQuestion = q_id;
				};
				
				//настройки диаграммы
				scope.config = 
				{
					showResults: true,
					refreshDataOnly: true,
				};
				scope.options = {
					chart: {
						type: 'discreteBarChart',//'multiBarHorizontalChart',//'discreteBarChart',
						height: 400,
						 margin : {
							top: 20,
							right: 0,
							bottom: 50,
							left: 0
						}, 
						x: function(d){return d.label;},
						y: function(d){return d.value;},
						showValues: true,
						valueFormat: function(d){
							return d3.format('.0f')(d)+'%';
						},
						duration: 500,
						showYAxis: false,
						wrapLabels: true,
						noData: 'Ответов нет',
						color: function(d,i)
						{
							if(d.correct)
								return '#aad406';
							return '#ed861d';
						},
						/* tooltip:
						{
							enabled: true,
							distance: 100,
							keyFormatter: function (d, i) {
								return xAxis.tickFormat()(d, i);
							},
							valueFormatter: function (d, i) {
								return yAxis.tickFormat()(d, i);
							},
							headerFormatter: function (d) {
								return d;
							},
							position: function () {
								return {
									left: d3.event !== null ? d3.event.clientX : 0,
									top: d3.event !== null ? d3.event.clientY : 0
								};
							}
						} */
						/*xAxis: {
							axisLabel: 'X Axis'
						},
						yAxis: {
							axisLabel: 'Y Axis',
							axisLabelDistance: -10
						}*/
					}
				};

				scope.nextQuestion = function(){if(scope.currentQuestion+1 > scope.questions) return; scope.currentQuestion++;};
				scope.prevQuestion = function(){if(scope.currentQuestion-1 < 1) return; scope.currentQuestion--;};

				angular.element(window).on('resize', function(){resizeContent();});

				//события от em-height-source
				//костыль, позволяющий масштабировать содержимое с вопросом
				scope.$on( 'heightchange', function(e, h)
				{
					resizeContent();
				});


				function resizeContent() {
					try
					{
						var docHeight = getViewportHeight();
						var docWidth = getViewportWidth();
						console.log('doc height = '+docHeight);
						var text = document.getElementById("question-text-element");
						var question = document.getElementById("question-element");
						var bar = document.getElementById("question-bar-element");
						var image = document.getElementById('question-image-element');
						text.style['font-size'] = '100%';
						if(image)
						{
							var imageWidth = image.naturalWidth;
							var imageHeight = image.naturalHeight;
							var kImage = imageWidth/imageHeight;
							while(imageWidth > docWidth || imageHeight > 0.5*docHeight)
							{
								imageWidth--;
								imageHeight =  kImage*imageWidth;
								image.style['height'] = imageWidth+'px';
								image.style['width'] = imageHeight+'px';
							}
						}
						while (text.offsetHeight / docHeight > 0.4) {
							//text.style['font-size'] = parseInt(text.style['font-size'])/2 + '%';
							text.style['font-size'] = parseInt(text.style['font-size'])-1 + '%';
						}
						scope.options.chart.height = docHeight*(1-question.offsetHeight/docHeight-0.15);
						document.getElementById("open-answer-list").style['height'] = docHeight*(1-question.offsetHeight/docHeight-0.2);
					}
					catch(e){console.log(e);}
				}

				function getViewportHeight() {
					var ua = navigator.userAgent.toLowerCase();
					var isOpera = (ua.indexOf('opera') > -1);
					var isIE = (!isOpera && ua.indexOf('msie') > -1);
					return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
				}
				function getViewportWidth()
				{
					var ua = navigator.userAgent.toLowerCase();
					var isOpera = (ua.indexOf('opera') > -1);
					var isIE = (!isOpera && ua.indexOf('msie') > -1);
					return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth : (document.parentWindow || document.defaultView).innerWidth;
				}

				function initResults()
				{
					var num = 0;
					for(var i in scope.results) //преобразование объекта в массив
					{
						if(scope.results.hasOwnProperty(i)) //проверяем, не является ли элемент пустым
							num++;
						scope.currentResults[scope.results[i].position] = scope.results[i];
						var count_questions = Object.keys(scope.currentResults[scope.results[i].position].questions).length;
						scope.currentResults[scope.results[i].position].vars = count_questions;
					}
					if(num == 0) //если объект ответов пустой
					{
						scope.currentResults = [];
						//scope.currentQuestion = 1;
					}
					initBar();
					if(scope.testType == 0)
						scope.config.showResults = false;
					else
					if(scope.testType == 1)
						scope.config.showResults = true;
				}
				
				function initBar()
				{
					if(!scope.currentResults[scope.currentQuestion])
						return;
					scope.data = {};
					for(var question in scope.currentResults[scope.currentQuestion].questions)
					{
						scope.data[question] =
							[
								{
									key: "data",
									values: []
								}
							];
						for(var a in scope.currentResults[scope.currentQuestion].questions[question].answers)
						{
							var answer = scope.currentResults[scope.currentQuestion].questions[question].answers[a];
							scope.data[question][0].values.push({'label':answer.text, 'value': answer.rate*100, 'count': answer.count, correct: answer.correct});
						}
					}
				}
				
			}
		};
	}]).
	filter('orderObjectBy', function()
	{
	  return function(items, field, reverse) {
		var filtered = [];
		angular.forEach(items, function(item) {
		  filtered.push(item);
		});
		filtered.sort(function (a, b) {
		  return (a[field] > b[field] ? 1 : -1);
		});
		if(reverse) filtered.reverse();
		return filtered;
	  };
	})
	.directive( 'emHeightSource', ['$timeout', function($timeout) {

		return {
			link: function( scope, elem, attrs ) {
				var h = elem[0].offsetHeight;
					$timeout(
						function()
						{
							$timeout(
								function()
								{
									scope.$emit( 'heightchange', h );
								},0
							)
						},0);
			}
		}

	}]);

	function updateResult(users, testResults, numberOfQuestions)
				{
					var arr = Array(numberOfQuestions.length);
					var correct_rate = {};
					var notNull = false; //флаг пустой таблицы
					for(var i in users)
					{
						users[i].rate = 0;
						if(testResults[i])
						for(var j=0;j<testResults[i].length;j++)
						{
							notNull = true; //таблица не пустая
							//if(!testResults[i][j])
							//	continue;
							var position = testResults[i][j].position-1;
							if(!arr[position]) arr[position] = {correct: 0, all_correct: 0};
							if(testResults[i][j].correct_answers === testResults[i][j].all_correct_answers && testResults[i][j].answers.length === testResults[i][j].all_correct_answers)
							{
								users[i].rate++;
								arr[position].correct++;
							}
							//else
							// if(testResults[i][j].correct_answers != 0 && testResults[i][j].correct_answers < testResults[i][j].all_correct_answers)
							// 	users[i].rate += .5;
							arr[position].all_correct++;
							if(!correct_rate[testResults[i][j].group_id])
								correct_rate[testResults[i][j].group_id] = {value: arr[position].correct/arr[position].all_correct};
							else
								correct_rate[testResults[i][j].group_id].value = arr[position].correct/arr[position].all_correct;
							numberOfQuestions[position].correct_rate = correct_rate[testResults[i][j].group_id];
							testResults[i][j].correct_rate = correct_rate[testResults[i][j].group_id];
						}
						users[i].rate = users[i].rate/numberOfQuestions.length;
					}
					if(!notNull) //если таблица пустая, очищаем результат
						for(var i=0; i<numberOfQuestions.length; i++)
							delete numberOfQuestions[i].correct_rate;
				};
function getQuestionText(htmlText, onlyString)
{
	var tmp = document.createElement('tmp');
	var tmp2 = document.createElement('tmp2');
	tmp.innerHTML = htmlText;
	var start = 0;
	var end;
	var tmpHtmlText = htmlText;
	if(tmpHtmlText)
		while(true)
		{
			var lowHtmlText = tmpHtmlText.toLowerCase()
			start = lowHtmlText.indexOf('<img');
			if(start == -1)
				break;
			end = lowHtmlText.indexOf('>',start);
			tmpHtmlText = tmpHtmlText.substr(0,start)+tmpHtmlText.substr(end+1);
		}
	tmp2.innerHTML = tmpHtmlText;
	if(!onlyString)
		return tmp2.innerHTML;
	else
		return tmp2.textContent;
	// var tmp = document.createElement('tmp');
	// var tmp2 = document.createElement('tmp2');
	// tmp.innerHTML = htmlText;
	// getText(tmp, tmp2);
	// if(!onlyString)
	// 	return tmp2.innerHTML;
	// else
	// 	return tmp2.textContent;
	// function getText(m, images)
	// {
	// 	while(m.children.length)
	// 	{
	// 		if(m.children[0].nodeName != 'IMG')
	// 			images.appendChild(m.children[0]);
	// 		else
	// 			m.removeChild(m.children[0]);
	// 		if(m.children[0])
	// 			getText(m.children[0], images);
	// 	}
	// }
}

function getQuestionImage(htmlText)
{
	var tmp = document.createElement('tmp');
	var tmp2 = document.createElement('tmp2');
	tmp.innerHTML = htmlText;
	getImage(tmp,tmp2);
	//return tmp2.innerHTML;
	if(tmp2.firstChild)
		return tmp2.firstChild.src;
	else
		return null;
	
	/* function getImage(m, images)
	{
		for(var m = n.firstChild; m != null; m = m.nextSibling)
		{
			if(m.children[0].nodeName == 'IMG')
				images.appendChild(m.children[0]);
			else
				images.removeChild(m.children[0]);
			getImage(m, images);
		}
	} */
	function getImage(m, images)
	{
		while(m.children.length)
		{
			if(m.children[0].nodeName == 'IMG')
				images.appendChild(m.children[0]);
			else
				m.removeChild(m.children[0]);
			if(m.children[0])
				getImage(m.children[0], images);
		}
	}		
}

		
}
)();