'use strict'
angular.module('IQApp.lessonsView', ['ngRoute', 'room.adminRoom', 'fsExplorer', 'profileService', 'lessonsReports', 'userAuthService', 'helpers']).
config(['$routeProvider', function($routeProvider)
{
    $routeProvider.when('/lessons',
        {
            templateUrl: 'lessons/lessonsView.html',
            controller: 'lessonsViewCtrl',
        });
}
]).
controller('lessonsViewCtrl', ['$scope', '$interval', '$timeout', '$routeParams', '$location', '$modal', '$window', 'AdminRoom', 'Profile', 'LessonsReports', 'UserAuthService', 'Tutorial',
    function($scope, $interval, $timeout, $routeParams, $location, $modal, $window, AdminRoom, Profile, LessonsReports, UserAuthService, Tutorial)
    {
        if(!UserAuthService.getUserToken())
        {
            $location.path('/auth');
            return;
        }

        $window.ga('set', 'page', '/eventsView.html');
        $window.ga('send', 'pageview');

        $scope.adminRoom = AdminRoom;
        $scope.lessonsReports = LessonsReports;
        //количество уроков ВСЕГО
        $scope.numOfLessons = LessonsReports.countOfLessons;
        connect();
        function connect()
        {
            if(!AdminRoom.connected)
            {
                $scope.state = 'initializing';
                AdminRoom.init(
                    function()
                    {
                        init();
                    },
                    function()
                    {
                        $scope.state = 'failed';
                    });
            }
            else
                init();
        }
        function init()
        {
            $scope.state = 'initialized';
            if(Tutorial.current == 'basicConcept')
            {
                Tutorial.go('lessonCreation');
            }
            if(Tutorial.current == 'answeredQuestion')
                Tutorial.go('lessonResults1');
            //флаг загрузки списка уроков
            $scope.loadingLessons;
            $scope.getLastLessons = function(){
                if($scope.numOfLessons == $scope.lessonsReports.lastLessons.length)
                    return;
                $scope.loadingLessons = true;
                LessonsReports.getLastLessons(20,
                    function(data)
                    {
                        $scope.numOfLessons = LessonsReports.countOfLessons;
                        $scope.loadingLessons = false;
                    },
                    function()
                    {
                        $scope.loadingLessons = false;
                    }
                );
            };
            var timerInt;
            if(!LessonsReports.lastLessons.length)
                $scope.getLastLessons();
            $scope.startLesson = function()
            {
                $location.path('/room');
                AdminRoom.startLesson({}, function()
                {
                },
                function(error)
                {
                    console.log('err while start new lesson')
                });
                // var modalInstance = $modal.open(
                //     {
                //         animation: true,
                //         templateUrl: 'lessons/createLessonModal.html',
                //         controller: 'createLessonCtrl',
                //         scope: $scope,
                //     });
                // modalInstance.result.then(
                //     function(result)
                //     {
                //         AdminRoom.startLesson({title: result.title,id: result.id, description: result.description});
                //         $location.path('/room');
                //     },
                //     function(){}
                // );
            };
            $scope.endLesson = function()
            {
                $scope.endingLesson = true;
                AdminRoom.endLesson(
                    function()
                    {
                        $scope.endingLesson = false;
                    });
                $interval.cancel(timerInt);
            };
            //подсчет кол-ва активных пользователей в комнате
            $scope.$watch('adminRoom.users',function(val)
            {
                var count = 0;
                for(var i in $scope.adminRoom.users)
                    if($scope.adminRoom.users[i].del_reason == null)
                        count++;
                $scope.countOfOnlineUsers = count;
            }, true);
            if(AdminRoom.lessonState == 'active')
                timerInt = $interval(function()
                {
                    $scope.timer = Date.now() - new Date(AdminRoom.currentLesson.start_time*1000);
                },1000);
            $scope.goToRoom = function()
            {
                $location.path('/room');
            }

        }
    }
]).
controller('createLessonCtrl',['$scope','$modalInstance',
    function($scope, $modalInstance)
    {
        $scope.cancel = function()
        {
            $modalInstance.dismiss('cancel');
        }
        $scope.ok = function()
        {
            $modalInstance.close({id: $scope.id, title:$scope.title, description: $scope.description});
        }
    }
]);