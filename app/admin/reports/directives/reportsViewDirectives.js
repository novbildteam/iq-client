(function() {

	angular.module('IQApp.reportsViewDirectives', ['room.adminRoom', 'restApi.tests', 'userAuthService']).directive('streamReport', function () {
		return (
		{
			templateUrl: 'reports/directives/report-results.html',
			restrict: 'E',
			scope: {
				users: '=users',
				test: '=test',
				testContent: '=testContent',
				onResultClick: '=onResultClick',
			},
			link: function(scope,element,attrs)
			{
				scope.visType = 'table';
			}
		});

	}).directive('streamQuestionResults', ['RestApiTests', 'UserAuthService', function (RestApiTests, UserAuthService) {
		return (
		{
			templateUrl: 'reports/directives/stream-question-results.html',
			restrict: 'E',
			scope: {
				users: '=users',
				test: '=test',
				testContent: '=testContent'
			},
			controller: ['$scope', function ($scope) {
			}],
			link: function (scope, element, attrs) {
				scope.groupsFilter = {type: 'position', reverse: false};
				scope.results = {};
				scope.orderGroupsBy = function (filter) {
					if (scope.groupsFilter.type === filter)
						scope.groupsFilter.reverse = !scope.groupsFilter.reverse;
					else {
						scope.groupsFilter.type = filter;
						scope.groupsFilter.reverse = false;
					}
				};
				scope.$watch('testContent', function()
				{
					if(scope.testContent)
						initTestContent(scope.testContent.questions,scope.results);
					init(scope.users, scope.results);
				});

				//init(scope.users, scope.results);
			},
		});
		function init(data, results)
		{
			for (var i in data)
				for (var j in data[i].questions)
					checkQuestion(data[i].questions[j], results);
		}
		function initTestContent(data, results)
		{
			// for(var i in data)
			// 	for(var j in data[i].questions)
			// 	{
			// 		data[i].questions[j].group_id = data[i].group_id; // костылек :)
			// 		initQuestion(data[i].questions[j], results)
			// 	}
			for(var i in data)
				initQuestion(data[i], results);
		}

		function checkQuestion(questionData, results)
		{
			var result = null; //если группы нет в результатах, то вернуть ее номер в качестве результата функции
			var question = questionData;
			if(!questionData.question_id)
				return;
			if (!results[question.group_id]) {
				//если группы нет в результатах, то добавить новую
				results[question.group_id] = {
					group_id: question.group_id, questions: {}, position: question.position,
					count: 0, correct_answers: 0, correct_rate: 0
				};
				result = question.group_id;
			}
			if (!results[question.group_id].questions[question.question_id]) //если нет вопроса в списке, то добавить новый
			{
				results[question.group_id].questions[question.question_id] =
				{
					question_id: question.question_id,
					position: question.position,
					type: question.type,
					count: 0, correct_answers: 0, correct_rate: 0,
					answers: {},
				}
			}
			var questionObj = results[question.group_id].questions[question.question_id];//объект вопроса
			questionObj.count++;
			results[question.group_id].count++;
			if (question.correct_answers === question.all_correct_answers) //проверка правильности ответа на вопрос пользователем
			{
				questionObj.correct_answers++;
				results[question.group_id].correct_answers++;
			}
			questionObj.correct_rate = questionObj.correct_answers / questionObj.count; //вычисление долЯ правильных ответов на вопрос
			results[question.group_id].correct_rate = results[question.group_id].correct_answers / results[question.group_id].count; //вычисление доли правильных вопросов в группе
			for (var k in question.answers) {
				var answer = question.answers[k];
				var answerObj;
				if (question.type != 2)//если вопросы закрытого типа, то поместить их id в качестве ключа таблицы
				{
					if (!questionObj.answers[answer.answer_id])
						questionObj.answers[answer.answer_id] =
						{
							answer_id: answer.answer_id,
							text: answer.text,
							rate: 0, count: 0,
						}
					answerObj = questionObj.answers[answer.answer_id];
				}
				if (question.type == 2) //если вопросы открытого типа, то text в качестве ключа таблицы
				{
					if (!questionObj.answers[answer.text])
						questionObj.answers[answer.text] =
						{
							text: answer.text,
							rate: 0, count: 0,
						};
					answerObj = questionObj.answers[answer.text];
				}
				answerObj.count++;
			}
			for (var k in questionObj.answers) {
				var answerObj = questionObj.answers[k];
				answerObj.rate = answerObj.count / questionObj.count;
			}
			return result;
		}
		function initQuestion(questionData, results)
		{
			var result = null; //если группы нет в результатах, то вернуть ее номер в качестве результата функции
			var question = questionData;
			if (!results[question.group_id]) {
				//если группы нет в результатах, то добавить новую
				results[question.group_id] = {
					group_id: question.group_id, questions: {}, position: question.position,
					count: 0, correct_answers: 0, correct_rate: 0
				};
				result = question.group_id;
			}
			if (!results[question.group_id].questions[question.question_id]) //если нет вопроса в списке, то добавить новый
			{
				results[question.group_id].questions[question.question_id] =
				{
					question_id: question.question_id,
					text: question.text,
					position: question.position,
					type: question.type,
					count: 0, correct_answers: 0, correct_rate: 0,
					answers: {},
				}
			}
			results[question.group_id].questions[question.question_id].text = question.text;
			var questionObj = results[question.group_id].questions[question.question_id];//объект вопроса
			for (var k in question.answers) {
				var answer = question.answers[k];
				var answerObj;
				if (question.type != 2)//если вопросы закрытого типа, то поместить их id в качестве ключа таблицы
				{
					if (!questionObj.answers[answer.answer_id])
						questionObj.answers[answer.answer_id] =
						{
							answer_id: answer.answer_id,
							rate: 0, count: 0,
						}
					answerObj = questionObj.answers[answer.answer_id];
				}
				if (question.type == 2) //если вопросы открытого типа, то text в качестве ключа таблицы
				{
					if (!questionObj.answers[answer.text])
						questionObj.answers[answer.text] =
						{
							text: answer.text,
							rate: 0, count: 0,
						};
					answerObj = questionObj.answers[answer.text];
				}
				answerObj.text = answer.text;
				answerObj.correct = answer.correct;
			}
		}


	}]).directive('streamQuestionResult', function () {
		return (
		{
			//require: '^^streamQuestionResults',
			templateUrl: 'reports/directives/stream-question-result.html',
			restrict: 'E',
			scope: {
				result: '=result',
				hide: '=hide',
				type: '=type',
			},
			link: function (scope, element, attrs, controller) {
				scope.hideQuestion = scope.hide;
				scope.$watch('hide', function () {
					scope.hideQuestion = scope.hide;
				});

				scope.numberOfVars = 0; //количество вариантов вопроса
				scope.previewQuestion; //превью вопроса
				scope.previewImage; //изображение для превью варианта вопроса. Изображение берется для первого варианта вопроса 
				for (var i in scope.result.questions) {
					scope.previewQuestion = scope.result.questions[i];
					scope.numberOfVars++
				}
				if(scope.numberOfVars == 1)
					scope.previewImage = getQuestionImage(scope.result.questions[i].text);
				scope.getQuestionText = function(text)
				{
					if(!text) return '';
					return getQuestionText(text,false);
				};
				scope.getQuestionImage = function(text) {return getQuestionImage(text)};
				scope.countOfQuestions = 0;
			}
		});
		// function init(testId, data, results)
		// {
		// 	var groups_id = [];
		// 	getFullTest(testId);
		// 	for (var i in data) {
		// 		for (var j in data[i].questions) {
		// 			var q = checkQuestion(data[i].questions[j], results);
		// 			if (q)
		// 				groups_id.push(q); //добавить в список длЯ загрузки из кэша
		// 		}
		// 	}
		// 	if (groups_id.length > 0) //загрузка содержимого вопросов и ответов
		// 	{
		// 		//this.loadGroups(groups_id);
		// 	}
		// }
		// function getFullTest(testId, success, fail)
		// {
		// 	var apiTests = RestApiTests;
		// 	var auth = UserAuthService;
		// 	apiTests.getFullTest(auth.getUserToken(), testId).$promise.then(
		// 		function(data)
		// 		{
		// 			if(!data.error_code)
		// 			{
		// 				if(success) success(data);
		// 			}
		// 			else
		// 			if(fail) fail(data);
		// 		},
		// 		function(error)
		// 		{
		// 			if(fail) fail(error);
		// 		}
		// 	);
		// }
		// function checkQuestion(questionData, results)
		// {
		// 	var result = null; //если группы нет в результатах, то вернуть ее номер в качестве результата функции
		// 	var question = questionData;
		// 	if (!results[question.group_id]) {
		// 		//если группы нет в результатах, то добавить новую
		// 		results[question.group_id] = {
		// 			group_id: question.group_id, questions: {}, position: question.position,
		// 			count: 0, correct_answers: 0, correct_rate: 0
		// 		};
		// 		result = question.group_id;
		// 	}
		// 	if (!results[question.group_id].questions[question.question_id]) //если нет вопроса в списке, то добавить новый
		// 	{
		// 		results[question.group_id].questions[question.question_id] =
		// 		{
		// 			question_id: question.question_id,
		// 			text: question.text,
		// 			position: question.position,
		// 			type: question.type,
		// 			count: 0, correct_answers: 0, correct_rate: 0,
		// 			answers: {},
		// 		}
		// 	}
		// 	var questionObj = results[question.group_id].questions[question.question_id];//объект вопроса
		// 	questionObj.count++;
		// 	results[question.group_id].count++;
		// 	if (question.correct_answers === question.all_correct_answers) //проверка правильности ответа на вопрос пользователем
		// 	{
		// 		questionObj.correct_answers++;
		// 		results[question.group_id].correct_answers++;
		// 	}
		// 	questionObj.correct_rate = questionObj.correct_answers / questionObj.count; //вычисление долЯ правильных ответов на вопрос
		// 	results[question.group_id].correct_rate = results[question.group_id].correct_answers / results[question.group_id].count; //вычисление доли правильных вопросов в группе
		// 	for (var k in question.answers) {
		// 		var answer = question.answers[k];
		// 		var answerObj;
		// 		if (question.type != 2)//если вопросы закрытого типа, то поместить их id в качестве ключа таблицы
		// 		{
		// 			if (!questionObj.answers[answer.answer_id])
		// 				questionObj.answers[answer.answer_id] =
		// 				{
		// 					answer_id: answer.answer_id,
		// 					//text: answer.text,
		// 					rate: 0, count: 0,
		// 				}
		// 			answerObj = questionObj.answers[answer.answer_id];
		// 		}
		// 		if (question.type == 2) //если вопросы открытого типа, то text в качестве ключа таблицы
		// 		{
		// 			if (!questionObj.answers[answer.text])
		// 				questionObj.answers[answer.text] =
		// 				{
		// 					text: answer.text,
		// 					rate: 0, count: 0,
		// 				};
		// 			answerObj = questionObj.answers[answer.text];
		// 		}
		// 		answerObj.count++;
		// 	}
		// 	for (var k in questionObj.answers) {
		// 		var answerObj = questionObj.answers[k];
		// 		answerObj.rate = answerObj.count / questionObj.count;
		// 	}
		// 	return result;
		// }
	}).directive('tableResults', ['AdminRoomResults', function(AdminRoomResults)
	{
		return({
			//require: '^^reportResults',
			templateUrl: 'reports/directives/table-results.html',
			restrict: 'E',
			scope:
			{
				test: '=test',
				users: '=users',
				testContent: '=testContent',
			},
			controller:
			['$scope','$modal',function($scope, $modal) {
				$scope.showQuestion = function( results, user)
				{
					// отображаемые результаты списком
					results = [].concat(results);
					// словарь содержимого вопросов тестов, необходимого для передваваемых результатов results
					var questions = {};
					var count_of_noexists = 0;
					for (var i=0; i<results.length; i++)
					{
						var res = results[i];
						//если ответ сущесвтует, добавить содержимое вопроса в словарь
						if(res.question_id)
							questions[res.question_id] = $scope.testContent.questions[res.question_id];
						else
							count_of_noexists += 1;
					}
					var modalInstance = $modal.open(
						{
							animation: true,
							templateUrl: 'reports/directives/viewQuestionModal.html',
							controller: 'viewQuestionCtrl',
							scope: $scope,
							size: 'lg',
							resolve:
							{
								//если ответов нет
								noAnswers: function() { return results.length == count_of_noexists},
								testType: function(){return $scope.test.type;},
								results: function(){return results;},
								user: function(){return user;},
								questions: function(){return questions;}
							}
						});
				};
			}],
			link: function (scope, element, attrs)
			{
				/*
				 тип фильтров для userFilter:
				 first_name - имя
				 rate - рейтинг (кол-во звезд)
				 del_reason - причина удаления
				 time - время прохождения теста
				 */
				scope.userFilter = {type: null, reverse: false};
				/*
				 тип фильтров для groupsFilter:
				 correct_rate.value - процент правильных ответов
				 position - позиция в тесте
				 */
				scope.groupsFilter = {type: 'position', reverse: false};
				scope.$watch(attrs.test, function (value) {
					scope.numberOfQuestions = new Array(scope.test.questions);
					for (var i = 0; i < scope.test.questions; i++) {
						scope.numberOfQuestions[i] = {position: i + 1};
					}
				});
				scope.$watch(attrs.users, function (value) {
					scope.integralResult = integralResult(scope.users, scope.test, scope.numberOfQuestions);
				});
				scope.orderUsersBy = function (filter) {
					if (scope.userFilter.type === filter)
						scope.userFilter.reverse = !scope.userFilter.reverse;
					else {
						scope.userFilter.type = filter;
						scope.userFilter.reverse = false;
					}
				};
				scope.orderGroupsBy = function (filter) {
					if (scope.groupsFilter.type === filter)
						scope.groupsFilter.reverse = !scope.groupsFilter.reverse;
					else {
						scope.groupsFilter.type = filter;
						scope.groupsFilter.reverse = false;
					}
				};
				/*scope.numberOfQuestions = function()
				 {
				 if(scope.test.questions != null)
				 return new Array(scope.test.questions);
				 else
				 return [];
				 };*/
				/*scope.integralResult = function()
				 {
				 var arr = Array(scope.test.questions);
				 for(var i in scope.users)
				 {
				 for(var j=0;j<scope.users[i].questions.length;j++)
				 {
				 if(!arr[j])	arr[j] = {correct: 0, all_correct: 0};
				 if(scope.users[i].questions[j].correct_answers === scope.users[i].questions[j].all_correct_answers)
				 arr[j].correct++;
				 arr[j].all_correct++;
				 }
				 }
				 //console.log(arr);
				 return arr;
				 };*/
			},
		
		})
	}])
	.directive('userResults', function()
	{
		return({


		templateUrl: 'reports/directives/user-results.html',
			restrict: 'E',
		scope:
		{
			test: '=test',
			users: '=users',
		},
		});
	})
	//контроллер модального окна просмотра результатов (для табличной версии)
	.controller('viewQuestionCtrl', ['$scope', '$modalInstance', 'noAnswers', 'results', 'questions', 'testType', 'user', 'AdminRoomResults', function($scope, $modalInstance, noAnswers, results, questions, testType, user, AdminRoomResults)
	{
		$scope.questions = questions;
		$scope.testType = testType;
		$scope.user = user;
		$scope.results = results;
		$scope.noAnswers = noAnswers;

		//непонятно зачем нужна эта функция :)
		function showQuestion(results, questions)
		{
			var results_res = [];
			for(var result_index=0; result_index < results.length; result_index++)
			{
				var result = results[result_index];
				var question = questions[result.question_id];
				var res = {
					question_id: result.question_id,
					type: result.type,
					answers: []
				};
				var ans;
				if (question.type == 1 || question.type == 0)
				{
					for (var i in result.answers) {
						ans = result.answers[i];
						if (ans.answer_id == null || ans.answer_id == undefined)
							break;
						for (var j in question.answers)
						{
							if (ans.answer_id == question.answers[j].answer_id)
							{
								res.answers.push(question.answers[j]);
								break;
							}
						}
					}
				}
				else
				{
					for (var i in result.answers)
					{
						ans = result.answers[i];
						res.answers.push(ans);
						for (var j in question.answers)
						{
							if (question.answers[j].text == ans.text)
							{
								ans.correct = true;
								break;
							}
						}
					}
				}
				results_res.push(res);
			}
			return results_res;
		}
		$scope.ok = function()
		{
			$modalInstance.close();
		}

	}]);
	
	
	function getQuestionText(htmlText, onlyString)
	{
		var tmp = document.createElement('tmp');
		var tmp2 = document.createElement('tmp2');
		tmp.innerHTML = htmlText;
		var start = 0;
		var end;
		var tmpHtmlText = htmlText;
		while(true)
		{
			var lowHtmlText = tmpHtmlText.toLowerCase()
			start = lowHtmlText.indexOf('<img');
			if(start == -1)
				break;
			end = lowHtmlText.indexOf('>',start);
			tmpHtmlText = tmpHtmlText.substr(0,start)+tmpHtmlText.substr(end+1);
		}
		tmp2.innerHTML = tmpHtmlText;
		if(!onlyString)
			return tmp2.innerHTML;
		else
			return tmp2.textContent;
		// tmp.innerHTML = htmlText;
		// getText(tmp, tmp2);
		// if(!onlyString)
		// 	return tmp2.innerHTML;
		// else
		// 	return tmp2.textContent;
		// function getText(m, images)
		// {
		// 	while(m.children.length)
		// 	{
		// 		if(m.children[0].nodeName != 'IMG')
		// 			images.appendChild(m.children[0]);
		// 		else
		// 			m.removeChild(m.children[0]);
		// 		if(m.children[0])
		// 			getText(m.children[0], images);
		// 	}
		// }
	}

	function getQuestionImage(htmlText)
	{
		var tmp = document.createElement('tmp');
		var tmp2 = document.createElement('tmp2');
		tmp.innerHTML = htmlText;
		getImage(tmp,tmp2);
		//return tmp2.innerHTML;
		if(tmp2.firstChild)
			return tmp2.firstChild.src;
		else
			return null;

		/* function getImage(m, images)
		 {
		 for(var m = n.firstChild; m != null; m = m.nextSibling)
		 {
		 if(m.children[0].nodeName == 'IMG')
		 images.appendChild(m.children[0]);
		 else
		 images.removeChild(m.children[0]);
		 getImage(m, images);
		 }
		 } */
		function getImage(m, images)
		{
			while(m.children.length)
			{
				if(m.children[0].nodeName == 'IMG')
					images.appendChild(m.children[0]);
				else
					m.removeChild(m.children[0]);
				if(m.children[0])
					getImage(m.children[0], images);
			}
		}
	}
	function integralResult(users, test, numberOfQuestions)
	{
		var arr = Array(test.questions);
		var correct_rate = {};
		for (var i in users)
		{
			users[i].rate = 0;
			//массив номеров вопрос, на которые пользователь дал ответ
			var actualPosition = [];
			for (var j = 0; j < users[i].questions.length; j++)
			{
				var position = users[i].questions[j].position - 1;
				actualPosition.push(position);
				if (!arr[position]) arr[position] = {correct: 0, all_correct: 0};
				if (users[i].questions[j].correct_answers === users[i].questions[j].all_correct_answers && users[i].questions[j].answers.length === users[i].questions[j].all_correct_answers) {
					users[i].rate++;
					arr[position].correct++;
				}
				// else if (users[i].questions[j].correct_answers != 0 && users[i].questions[j].correct_answers < users[i].questions[j].all_correct_answers)
				// 	users[i].rate += .5;
				arr[position].all_correct++;
				if (!correct_rate[users[i].questions[j].group_id])
					correct_rate[users[i].questions[j].group_id] = {value: arr[position].correct / arr[position].all_correct};
				else
					correct_rate[users[i].questions[j].group_id].value = arr[position].correct / arr[position].all_correct;
				numberOfQuestions[position].correct_rate = correct_rate[users[i].questions[j].group_id];
				users[i].questions[j].correct_rate = correct_rate[users[i].questions[j].group_id];
			}
			users[i].rate = users[i].rate/numberOfQuestions.length;
			//заполнение недостающими ответами (те, на которые пользователь не дал ответ)
			//это необходимо для корректной сортировки по correct_rate
			if(test.questions - actualPosition.length > 0)
			{
				if(actualPosition.length == 0)
					users[i].questions = [];
				var tmpArray = {};
				for(var k=0;k<test.questions;k++)
					tmpArray[k] = true;
				for(k in actualPosition)
					delete tmpArray[actualPosition[k]];
				for(k in tmpArray)
					users[i].questions.push({position:k,correct_rate:{value:0}});
			}
		}
		//заполнение недостающих
		for(k=0;k<numberOfQuestions.length;k++)
			if(!numberOfQuestions[k].correct_rate)
				numberOfQuestions[k].correct_rate = {value:0};
		// actualPosition = [];
		// for(k=0;k<numberOfQuestions.length;k++)
		// 	actualPosition.push(numberOfQuestions[k].position);
		// tmpArray = {};
		// for(k=0;k<numberOfQuestions;k++)
		// 	tmpArray[k] = true;
		// for(k in actualPosition)
		// 	delete tmpArray[actualPosition[k]];
		// for(k in tmpArray)
		// 	numberOfQuestions.push({position:k,correct_rate:{value:0}});
		// console.log(numberOfQuestions);
	}
})();