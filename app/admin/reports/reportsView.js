'use strict'
angular.module('IQApp.reportsView',['ngRoute','streamsReports', 'lessonsReports', 'userAuthService', 'room.adminRoom', 'helpers']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/reports/streams/:page',
		{
			templateUrl: 'reports/reportsView.html',
			controller: 'streamsReportsViewCtrl'
		});
		$routeProvider.when('/reports/streams',
		{
			templateUrl: 'reports/reportsView.html',
			controller: 'streamsReportsViewCtrl'
		});
		$routeProvider.when('/report/view/:type/:streamId',
		{
			templateUrl: 'reports/reportView.html',
			controller: 'streamReportViewCtrl'
		});
		$routeProvider.when('/report/view/:type',
		{
			templateUrl: 'reports/reportView.html',
			controller: 'streamReportViewCtrl'
		});
	$routeProvider.when('/reports/lessons',
		{
			templateUrl: 'reports/lessonsView.html',
			controller: 'lessonsReportViewCtrl',
		});
		$routeProvider.when('/reports/lessons/:page',
		{
			templateUrl: 'reports/lessonsView.html',
			controller: 'lessonsReportViewCtrl',
		});
		$routeProvider.when('/reports/lesson/:id',
		{
			templateUrl: 'reports/lessonView.html',
			controller: 'lessonReportViewCtrl',
			reloadOnSearch: false
		});
	}
	]).
	controller('streamsReportsViewCtrl',['$scope', '$routeParams', '$location', 'StreamsReports', function($scope, $routeParams, $location, StreamsReports)
	{
		$scope.list = [];
		$scope.streams = StreamsReports;
		$scope.maxOnPage = 20;
		$scope.maxPages = 5;
		$scope.page = Number($routeParams.page);
		if(!$scope.page)
			$scope.page = 1;
		$scope.checkedStreams = {};
		$scope.changePage = function(page)
		{
			if($scope.page != $routeParams.page)
				$location.path('/reports/streams/'+$scope.page);
		};
		$scope.getLastStreams = function()
		{
			if($scope.streams.numOfReports == $scope.list.length)
				return;
			$scope.loadingStreams = true;
			StreamsReports.getLastStreams(($scope.page-1)*$scope.maxOnPage, $scope.maxOnPage, testFilter,
				function(data)
				{
					$scope.loadingStreams = false;
					$scope.page++;
					$scope.list = $scope.list.concat(data);
				},
				function()
				{
					$scope.loadingReports = false;
				});
		};

		$scope.showPreview = function(stream)
		{
			StreamsReports.current = stream;
		};
		$scope.showResults = function()
		{
			if($scope.streams.cartLength > 0)
				$location.path('/report/view/cart');
		};
		$scope.addToCart = function(stream)
		{
			$scope.checkedStreams[stream.stream_id] = true;
			StreamsReports.addToCart(stream);
		};
		$scope.removeFromCart = function(stream)
		{
			$scope.checkedStreams[stream.stream_id] = false;
			StreamsReports.removeFromCart(stream.stream_id);
		};
		$scope.changeToCart = function(stream)
		{
			if($scope.inCart(stream.stream_id))
				$scope.removeFromCart(stream);
			else
				$scope.addToCart(stream);
		}
		$scope.showStreamResult = function(stream)
		{
			StreamsReports.current = {stream: stream};
			$location.path('/report/view/stream/'+stream.stream_id);
		};
		$scope.inCart = function(streamId)
		{
			return streamId in $scope.streams.cart;
		};
		$scope.clearCart = function()
		{
			for(var i in $scope.checkedStreams)
				$scope.checkedStreams[i] = false;
			StreamsReports.clearCart();
		};
		$scope.removeAll = function()
		{
			for(var stream in $scope.streams.list)
				$scope.removeFromCart($scope.streams.list[stream].stream_id)
		};
		$scope.addAllToCart = function()
		{
			for(var stream in $scope.streams.list)
				$scope.addToCart($scope.streams.list[stream]);
		};
		for(var i in StreamsReports.cart)
			$scope.checkedStreams[i] = true;
		if($routeParams.test_id)
			var testFilter = {"test_id": $routeParams.test_id};
		//if($scope.list.length)
			$scope.getLastStreams();
		
	}]).
	controller('streamReportViewCtrl',['$scope', '$modal','$routeParams', '$location', 'StreamsReports', function($scope, $modal, $routeParams, $location, StreamsReports)
	{
		if($routeParams.type == 'stream')
		{
			if($routeParams.streamId != null && (!StreamsReports.current || StreamsReports.current.stream_id !== $routeParams.streamId))
				StreamsReports.current = {stream_id: Number($routeParams.streamId)};
			else
			if(!StreamsReports.current && !$routeParams.streamId)
			{
				console.log('no current stream')
				return;
			}
			StreamsReports.loadCurrent(
			function()
			{
				$scope.list = [StreamsReports.current];
				console.log('successfully');
				$scope.getFullTest(StreamsReports.current);
			});
		}
		if($routeParams.type == 'cart')
		{
			if(StreamsReports.cartLength == 0)
				$scope.list = [];
			else
			{
				StreamsReports.loadCart(function()
				{
					$scope.list = [];
					for(var i in StreamsReports.cart)
						$scope.list.push(StreamsReports.cart[i]);
				},
				function()
				{
					console.log('err while loading cart');
				});
			}
		}
		// $scope.showQuestion = function(type, result, user)
		// {
		// 	console.log(type, result, user)
		// 	var modalInstance = $modal.open(
		// 	{
		// 		animation: $scope.animationsEnabled,
		// 		templateUrl: 'viewQuestionModal.html',
		// 		controller: 'viewQuestionCtrl',
		// 		scope: $scope,
		// 		size: 'lg',
		// 		resolve:
		// 		{
		// 			testType: function(){return type;},
		// 			result: function(){return result;},
		// 			user: function(){return user;},
		// 		}
		// 	});
		// };
		$scope.fullTest = {}; //хэш-таблица содержимого тестов (по ключу test_id)
		$scope.removeFromList = function(report)
		{
			var count
			for(var i=0;i<$scope.list.length;i++)
				if($scope.list[i] == report)
				{
					$scope.list = $scope.list.slice(0,i).concat($scope.list.slice(i+1));
					break;
				}
			StreamsReports.removeFromCart(report.stream.stream_id);
		}
		//получение содержимого теста
		$scope.getFullTest = function(stream)
		{
			if(!$scope.fullTest[stream.test.test_id])
				StreamsReports.getFullTest(stream, function(data)
				{
					$scope.fullTest[stream.test.test_id] = {raw: data};
					var fullTest = $scope.fullTest[stream.test.test_id];
					//преобразование в хэш-таблицы групп, ответов и вопросов
					makeHashTable(fullTest, data);
				});
		}

		function makeHashTable(fullTest, data)
		{
			fullTest.groups = {};
			fullTest.questions = {};
			fullTest.answers = {};
			for(var i in data.groups)
			{
				var g = data.groups[i];
				var group = {group_id: g.group_id, position: g.position, questions: {}};
				fullTest.groups[g.group_id] = group;
				for(var j in g.questions)
				{
					var q = g.questions[j];
					var question =
					{
						question_id: q.question_id, group_id: group.group_id,
						text: q.text, type: q.type, position: group.position,
						answers: {}
					}
					fullTest.questions[q.question_id] = question;
					group.questions[q.question_id] = question;
					for(var k in q.answers)
					{
						var a = q.answers[k];
						var answer =
						{
							answer_id: a.answer_id, question_id: question.question_id,
							text: a.text,
						}
						fullTest.answers[a.answer_id] = answer;
						if(question.type == 0 || question.type == 1)
						{
							answer.correct = a.correct;
							question.answers[a.answer_id] = answer;
						}
						else
						if(question.type == 2)
						{
							answer.correct = true;
							question.answers[a.text] = answer;
						}
					}
				}
			}
		}

	}]).
	controller('lessonsReportViewCtrl', ['$scope','$location', '$routeParams', '$window', 'LessonsReports', 'UserAuthService', function($scope, $location, $routeParams, $window, LessonsReports, UserAuthService)
	{

		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
		}

		$window.ga('set', 'page', '/reports/events.html');
		$window.ga('send', 'pageview');

		$scope.page = $routeParams.page;
		if(!$scope.page)
			$scope.page = 1;
		$scope.maxOnPage = 20;

		$scope.changePage = function(page)
		{
			if(page != $routeParams.page)
				$location.path('/reports/lessons/'+page);
		};

		LessonsReports.getLastLessons(($scope.page-1)*$scope.maxOnPage, $scope.maxOnPage,
			function(data)
			{
				$scope.lessons = data;
			},
			function()
			{
				console.error('err while loading last lessons');
			}			
		);
	}]).
	controller('lessonReportViewCtrl',['$scope','$location','$routeParams', '$window', 'LessonsReports', 'StreamsReports','Tutorial', 'UserAuthService', function($scope, $location, $routeParams, $window, LessonsReports, StreamsReports, Tutorial, UserAuthService)
	{
		var lessonId = Number($routeParams.id);
		var streams = {}; //хэш-таблица стримов по stream_id

		if(!UserAuthService.getUserToken())
		{
			$location.path('/auth');
		}

		$window.ga('set', 'page', '/reports/event.html');
		$window.ga('send', 'pageview');

		getLesson(lessonId);	
		function getLesson(lessonId)
		{
			LessonsReports.getLessons(lessonId, {offset: 0, limit: 1},
				function(data)
				{
					$scope.lesson = data[0];
					getLessonsStreams(lessonId);
					if(Tutorial.current)
						Tutorial.go('lessonResults2', function()
						{
							Tutorial.go('gotoFs', null, 20000);
						});
					//getLessonsUsers(lessonId);
				},
				function(error)	{console.error('error while loading lesson '+lessonId);	}
			);
		}
		function getLessonsStreams(lessonId)
		{
			LessonsReports.getLessonsStreams(lessonId, {offset:0, limit: 20},
				function(data)
				{
					$scope.streams = data;
					//заполнение хэш-таблицы стримов
					for(var i=0; i<data.length; i++)
						streams[data[i].stream_id] = data[i];
				},
				function(error)
				{
					console.error('error while loading lesson streams');
				}
			);
		}
		function getLessonsUsers(lessonId)
		{
			LessonsReports.getLessonsUsers(lessonId, {offset:0, limit: 100},
				function(data)
				{
					$scope.users = data;
				},
				function(error)
				{
					console.error('error while loading lesson users');
				}
			);
		}

		$scope.fullTest = {}; //хэш-таблица содержимого тестов (по ключу test_id)
		$scope.streamResults = {}; //хэш-таблица содержимого стрима
		$scope.removeFromList = function(report)
		{
			for(var i=0;i<$scope.list.length;i++)
				if($scope.list[i] == report)
				{
					$scope.list = $scope.list.slice(0,i).concat($scope.list.slice(i+1));
					break;
				}
			StreamsReports.removeFromCart(report.stream.stream_id);
		};
		//получение содержимого теста
		$scope.loadStreamResults = function(stream)
		{
			if(!$scope.streamResults[stream.stream_id])
				StreamsReports.loadStreamResult(stream.stream_id,
					function(data)
					{
						$scope.streamResults[stream.stream_id] = data;
						if(!$scope.fullTest[stream.test.test_id])
							StreamsReports.getFullTest(stream, function(testData)
							{
								$scope.fullTest[stream.test.test_id] = {raw: testData};
								var fullTest = $scope.fullTest[stream.test.test_id];
								//преобразование в хэш-таблицы групп, ответов и вопросов
								makeHashTable(fullTest, testData);
							});
					},
					function(error)
					{
						console.error('err while loading stream result: '+error.error_code);
					}
				);
			
		};
		// TODO: Не работает передача данных как arraybuffer.
		$scope.downloadStream = function(streamId, format, event)
		{
			StreamsReports.downloadStream(streamId, format, function(data)
			{
				var file;
				if(!format || format=='xlsx')
				{
					file = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
				}
				if(!file)
					return;
				var fileURL = window.URL.createObjectURL(file);
				window.open(fileURL, '_self');
				// window.open('data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'+data, 'Download')
				window.URL.revokeObjectURL(file);
			})
		};
		$scope.getDownloadStreamLink = function(streamId, format)
		{
			return StreamsReports.getDownloadLink(streamId, format);
		};
		function makeHashTable(fullTest, data)
		{
			fullTest.groups = {};
			fullTest.questions = {};
			fullTest.answers = {};
			for(var i in data.groups)
			{
				var g = data.groups[i];
				var group = {group_id: g.group_id, position: g.position, questions: {}};
				fullTest.groups[g.group_id] = group;
				for(var j in g.questions)
				{
					var q = g.questions[j];
					var question =
					{
						question_id: q.question_id, group_id: group.group_id,
						text: q.text, type: q.type, position: group.position,
						answers: {}
					}
					fullTest.questions[q.question_id] = question;
					group.questions[q.question_id] = question;
					for(var k in q.answers)
					{
						var a = q.answers[k];
						var answer =
						{
							answer_id: a.answer_id, question_id: question.question_id,
							text: a.text,
						}
						fullTest.answers[a.answer_id] = answer;
						if(question.type == 0 || question.type == 1)
						{
							answer.correct = a.correct;
							question.answers[a.answer_id] = answer;
						}
						else
						if(question.type == 2)
						{
							answer.correct = true;
							question.answers[a.text] = answer;
						}
					}
				}
			}
		}
		
	}]);
	// .controller('viewQuestionCtrl', ['$scope', '$modalInstance', 'result', 'testType', 'user', 'AdminRoomResults', function($scope, $modalInstance, result, testType, user, AdminRoomResults)
	// {
	// 	var question;
	// 	$scope.result;
	// 	$scope.question;
	// 	$scope.testType = testType;
	// 	$scope.user = user;
	// 	showQuestion(result.question_id);
	//
	// 	function showQuestion(questionId)
	// 	{
	// 		AdminRoomResults.getQuestions(result.question_id,
	// 		function(q)
	// 		{
	// 			$scope.question = q[0];
	// 			question = q[0];
	// 			console.log(q)
	// 			var res = {answers: []};
	// 			var ans;
	// 			if(question.type == 1 || question.type == 0)
	// 			{
	// 				for(var i in result.answers)
	// 				{
	// 					ans = result.answers[i];
	// 					if(ans.answer_id == null || ans.answer_id == undefined)
	// 						break;
	// 					console.log(ans);
	// 					for(var j in question.answers)
	// 					{
	// 						if(ans.answer_id == question.answers[j].answer_id)
	// 						{
	// 							res.answers.push(question.answers[j]);
	// 							break;
	// 						}
	// 					}
	// 				}
	// 			}
	// 			else
	// 			{
	// 				for(var i in result.answers)
	// 				{
	// 					ans = result.answers[i];
	// 					res.answers.push(ans);
	// 					for(var j in question.answers)
	// 					{
	// 						if(question.answers[j].text == ans.text)
	// 							{
	// 								ans.correct = true;
	// 								break;
	// 							}
	// 					}
	// 				}
	// 			}
	// 			console.log(question);
	// 			console.log(res);
	// 			$scope.result = res;
	// 		},
	// 		function(error)
	// 		{
	// 			console.log(error);
	// 		});
	// 	}
	// 	$scope.ok = function()
	// 	{
	// 		$modalInstance.close();
	// 	}
	//
	// }]);