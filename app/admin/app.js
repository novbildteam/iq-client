'use strict';

// Declare app level module which depends on views, and components
/*angular.module('IQApp', [
  'ngRoute',
  'IQApp.testView',
  'IQApp.manageView',
  'IQApp.authView',
  'IQApp.profileView',
  'restApi.room',
  'restApi.accounts',
  'restApi.tests',
  'tests.adminTest',
  'userAuthService',
  'userRegService',
  'profileService',
  'httpInterceptor',
  'IQApp.version',
  'restApi.fakedata'
]).
config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
	$httpProvider.interceptors.push('HttpInterceptor');
  $routeProvider.when('/login', {templateUrl: '', controller: 'testCtrl'}).
  otherwise({redirectTo: '/main'});
}]).
run(['$rootScope', '$location', 'UserAuthService', 'Profile', function($rootScope, $location, UserAuthService, Profile)
{
	$rootScope.profile = Profile.profile;
	//Profile.set();
	$rootScope.logOut = function()
	{
		UserAuthService.logOutSystem(UserAuthService.getUserToken(),
		function()
		{
			Profile.reset();
			$location.path('/');
		},
		function()
		{
			console.log('error while logOut');
		});
	}
}]);*/
/*
  'restApi.room',
  'restApi.accounts',
  'restApi.tests',
  'tests.adminTest',
  'userRegService',
  'restApi.fakedata',
*/
(function() {
	var profileData = {};
	var noNetAlertPages = //список со страницами, для которых не надо выводить стандартные ошибки (alert) подключения
	{
		'/room': true,
		'/auth': true,
	}
    var myApplication = angular.module('IQApp', [
	'ngRoute',
	'ngAnimate',
	'room.adminRoom',
	'IQApp.testView',
	'IQApp.manageView',
	'IQApp.authView',
	'IQApp.regView',
	'IQApp.profileView',
	'IQApp.roomView',
	'IQApp.lessonsView',
	'IQApp.roomViewDirectives',
	'IQApp.testViewDirectives',
	'IQApp.reportsViewDirectives',
	'IQApp.reportsView',
	'IQApp.version',
	'IQApp.headerBar', 
	// 'IQApp.tutorialBar',
	// 'IQApp.welcomeView',
	// 'IQApp.helpView',
	'userAuthService',
	'profileService',
	'httpInterceptor',
	'restApi.fakedata',
	'errorInterceptor',
	'streamsReports',
	'lessonsReports',
	'settings',
	'helpers',
	'ui.bootstrap',
	'textAngular',
	'ngDraggable',
		'd3',
		'deviceService'
]).
config(['$routeProvider', '$httpProvider', '$resourceProvider', function($routeProvider, $httpProvider, $resourceProvider) {

	$resourceProvider.defaults.stripTrailingSlashes = false;

	$httpProvider.interceptors.push('HttpInterceptor');
	$routeProvider.when('/login', {templateUrl: '', controller: 'testCtrl'}).
  otherwise({redirectTo: '/lessons'});
}]).
run(['$rootScope', '$location', '$anchorScroll', 'UserAuthService', 'Profile', 'ErrorInterceptor', 'Settings', function($rootScope, $location, $anchorScroll, UserAuthService, Profile, ErrorInterceptor, Settings)
{
	if(auth.getUserToken() && profileData.root_id)
	{
		var obj = IQHelpers.toCamelCase(profileData);
		Profile.set(obj);
	}
}]).
controller('AppCtrl', ['$scope', '$location', '$rootScope', 'ErrorInterceptor', 'Settings', 'Profile', 'UserAuthService', 'StreamsReports', function($scope, $location, $rootScope, ErrorInterceptor, Settings, Profile, UserAuthService, StreamsReports)
{
	$scope.errorAlert = null; //объект события ошибки
	//обработчик полученных от сервера или сети
	ErrorInterceptor.onError = function(error)
	{
		if(error.type == ErrorInterceptor.ERR_TYPE.SERVER)
		{
			$scope.errorAlert = {type: 'danger'};
			switch(error.error.error_code)
			{
				case Settings.ERRCODE.TOKEN_OVER:
				case Settings.ERRCODE.WRONG_TOKEN:
				console.log('auth message');
					$scope.errorAlert.msg = 'Ошибка аутентификации. Пожалуйста авторизируйтесь <a href="#/auth">еще раз</a>';
					//$scope.errorAlert.timer = 5000;
					//$injector.get('Profile').reset();
					//$injector.get('UserAuthService').resetAuth();
					StreamsReports.clearCart();
					Profile.reset();
					UserAuthService.resetAuth();
					$location.path('#/auth');
				break;
				// case Settings.ERRCODE.ACC.WRONG_LOGIN:
				// 	$scope.errorAlert.msg = 'Ошибка. Неверные логин/пароль';
				// break;
				default:
					$scope.errorAlert = null;
				break;
			}
		}
		else
		if(error.type == ErrorInterceptor.ERR_TYPE.NET)
		{
			if($location.path() in noNetAlertPages)
				return;
			$scope.errorAlert = {type: 'danger'};
			$scope.errorAlert.msg = 'Потеряна связь с сервером. Проверьте подключение';
			//$scope.errorAlert.timer = 15000;
		}
	};
	$scope.closeErrorAlert = function()
	{
		$scope.errorAlert = null;
	};
	$scope.$on('$routeChangeSuccess',
	function()
	{
		$scope.closeErrorAlert();
	});
	// $scope.$on('$routeChangeStart',
	// function(event, next, current)
	// {
	// 	if(!UserAuthService.getUserToken())
	// 	{
	// 		$location.path('/auth');
	// 	}
	// });
	
}]);
		var authInjector = angular.injector(["userAuthService"]);
		var auth = authInjector.get("UserAuthService");
		if(!auth.isTemporary())
			auth.getProfile(
			function(data)
			{
				profileData = data;
				if(data.type == 2) //для студентов отдельный аккаунт
					auth.resetAuth();
				bootstrapApplication();
			},
			function()
			{
				auth.resetAuth();
				bootstrapApplication();
			});
		else
		{
			auth.resetAuth();
			bootstrapApplication();
		}
		function bootstrapApplication()
		{
			angular.element(document).ready(
			function()
			{
				angular.bootstrap(document, ["IQApp"]);
			});
		}
    
}());