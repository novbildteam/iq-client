'use strict'
angular.module('IQApp.helpView', ['ngRoute', 'userAuthService']).
config(['$routeProvider', function($routeProvider)
{
    $routeProvider.when('/help',
        {
            templateUrl: 'help/helpView.html',
            controller: 'helpViewCtrl',
        });
}]).
controller('helpViewCtrl',['$scope', '$window', 'UserAuthService',
function($scope, $window, UserAuthService)
{
    if(!UserAuthService.getUserToken())
    {
        $location.path('/auth');
        return;
    }

    $window.ga('set', 'page', '/helpView.html');
    $window.ga('send', 'pageview');

}]);
