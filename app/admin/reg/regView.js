﻿'use strict'
angular.module('IQApp.regView',['ngRoute', 'userRegService', 'userAuthService', 'profileService']).
	config(['$routeProvider', function($routeProvider)
	{
		$routeProvider.when('/reg',
		{
			templateUrl: 'reg/regViewNoPromo.html',
			controller: 'regViewCtrlNoPromo'
		});
	}
	]).
	controller('regViewCtrl',['$scope', '$routeParams', '$location', '$interval', 'UserAuthService', 'UserRegService', 'Profile',
		function($scope, $routeParams, $location, $interval, UserAuthService, UserRegService, Profile)
		{
			if(UserAuthService.getUserToken())
			{
				$location.path('/lessons');
				return;
			}
			$scope.input =
			{
				username: null,
				password: null,
				firstName: null,
				secondName: null,
				promo: $routeParams.promo
			}
			$scope.login = false;
			$scope.loadingProfile = false;
			var reg = UserRegService;
			$scope.logIn = function()
			{
				if($scope.login || $scope.logged)
					return;
				//$scope.authAlert = null;
				$scope.login = true;
				reg.registerByPromo({username: $scope.input.username, password: $scope.input.password,
					promo: $scope.input.promo, firstName: $scope.input.firstName, secondName: $scope.input.secondName},
					
				function()
				{
					$scope.login = false;
					$scope.logged = true;
					$scope.loadingProfile = true;
					UserAuthService.logInSystem({type:1, username:$scope.input.username, password: $scope.input.password},
						function(data)
						{
							var tmp = function()
							{
								Profile.load(
									function()
									{
										$location.path('/lessons').replace();
										$scope.loadingProfile = false;
									},
									function()
									{
										$scope.loadingProfile = false;
									});
							}
							$interval(tmp, 1000, 1);
						},
						function(error)
						{
							console.error('ошибка загрузки профиля');
						}
					);
				},
				function(error)
				{
					$scope.login = false;
					$scope.logged = false;
					if(error.error_code != 107)
						$scope.authAlert = {msg: 'Ошибка регистрации: неверный логин/пароль', type: 'danger'};
					else
					if(error.error_code == 107)
						$scope.authAlert = {msg: 'Ошибка регистрации: неверный промокод'};
					else
						$scope.authAlert = {msg: 'Ошибка сети', type: 'danger'};
				});
			};
			$scope.promoChange = function()
			{
				/* Ссылки на авторизации через соц сети*/
				$scope.social =
				{
					vk: UserAuthService.socialSignInUrl('vk',1, {promo: $scope.input.promo}),
					fb: UserAuthService.socialSignInUrl('fb',1, {promo: $scope.input.promo}),
					google: UserAuthService.socialSignInUrl('google',1, {promo: $scope.input.promo}),
				};
			};
			$scope.promoChange();
		}
	]).
	controller('regViewCtrlNoPromo',['$scope', '$routeParams', '$location', '$interval', 'UserAuthService', 'UserRegService', 'Profile',
		function($scope, $routeParams, $location, $interval, UserAuthService, UserRegService, Profile)
		{
			if(UserAuthService.getUserToken())
			{
				$location.path('/lessons');
				return;
			}
			$scope.input =
			{
				username: null,
				password: null,
				firstName: null,
				secondName: null,
				promo: $routeParams.promo
			};
			$scope.login = false;
			$scope.loadingProfile = false;
			var reg = UserRegService;
			$scope.logIn = function()
			{
				if($scope.login || $scope.logged)
					return;
				//$scope.authAlert = null;
				$scope.login = true;
				reg.register({username: $scope.input.username, password: $scope.input.password,
						firstName: $scope.input.firstName, secondName: $scope.input.secondName},

					function()
					{
						$scope.login = false;
						$scope.logged = true;
						$scope.loadingProfile = true;
						UserAuthService.logInSystem({type:1, username:$scope.input.username, password: $scope.input.password},
							function(data)
							{
								var tmp = function()
								{
									Profile.load(
										function()
										{
											$location.path('/lessons').replace();
											$scope.loadingProfile = false;
										},
										function()
										{
											$scope.loadingProfile = false;
										});
								}
								$interval(tmp, 1000, 1);
							},
							function(error)
							{
								console.error('ошибка загрузки профиля');
							}
						);
					},
					function(error)
					{
						$scope.login = false;
						$scope.logged = false;
						if(error.error_code != 107)
							$scope.authAlert = {msg: 'Ошибка регистрации: неверный логин/пароль', type: 'danger'};
						else
						if(error.error_code == 107)
							$scope.authAlert = {msg: 'Ошибка регистрации: неверный промокод'};
						else
							$scope.authAlert = {msg: 'Ошибка сети', type: 'danger'};
					});
			};
			$scope.promoChange = function()
			{
				/* Ссылки на авторизации через соц сети*/
				$scope.social =
				{
					vk: UserAuthService.socialSignInUrl('vk',1, {promo: $scope.input.promo}),
					fb: UserAuthService.socialSignInUrl('fb',1, {promo: $scope.input.promo}),
					google: UserAuthService.socialSignInUrl('google',1, {promo: $scope.input.promo}),
				};
			}
			$scope.promoChange();
		}
	]);