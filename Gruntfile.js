// ������������ ������
module.exports = function(grunt) {
    // ������
    initConfig = {
		buildPath: 'build/',
		test:
		{
			options:
			{
				msg: 'test message',
			}
		},
		bower_concat:
		{
			all:
			{
				dest: '<%= buildPath %>lib/lib.js',
				cssDest: '<%= buildPath %>lib/lib.css',
				mainFiles:
				{
					'html5-boilerplate':
					[
						'js/vendor/modernizr-2.6.2.min.js',
						'css/main.css',
						'css/normalize.css',
					]
				},
				exclude:
				[
					'angular-mocks'
				]
			}
		},
		wiredep:
		{
			task:
			{
				src:
				[
					'app/admin/index-debug.html',
					'app/student/index-debug.html',
				]
			}
		},
        // ���������
       /* concat: {
			admin:
			{
				src:
				[
					'app/js/components/textAngular/textAngular-rangy.min.js',
					'app/js/components/textAngular/textAngular-sanitize.js',
					'app/js/components/textAngular/textAngularSetup.js',
					'app/js/components/textAngular/textAngular.js',
					'app/components/version/version.js',
					'app/components/version/interpolate-filter.js',
					'app/components/version/version-directive.js',
					'app/admin/../js/services/auth/userAuthService.js',
					
					'app/js/services/fs/fsExplorer.js',
					'app/js/services/fs/fsManager.js',
					'app/js/services/httpInterceptor/httpInterceptor.js',
					'app/js/services/longPoller/longPoller.js',
					'app/js/services/profile/profile.js',
					'app/js/services/reg/userRegService.js',
					'app/js/services/restapi/*.js',
					'app/js/services/restapi/fakedata/fakedata_0.js', //��������
					'app/js/services/room/adminRoom.js',
					'app/js/services/structures/Structures.js',
					'app/js/services/tests/adminTest.js',
					'app/js/services/reports/streamsReports.js',
					'app/js/services/errorInterceptor/errorInterceptor.js',
					'app/js/helpers/helpers.js',
					'app/admin/auth/authView.js',
					'app/admin/manage/manageView.js',
					'app/admin/profile/profileView.js',
					'app/admin/test/testView.js',
					'app/admin/room/roomView.js',
					'app/admin/room/directives/roomViewDirectives.js',
					'app/admin/reports/directives/reportsViewDirectives.js',
					'app/admin/reports/reportsView.js',
					'app/admin/layout/headerBar.js',
					'app/admin/app.js',
				],
				dest:	'<%= buildPath %>admin/script.js',
				nonull: true,
			},
			student: {
                src: [
					'app/components/version/version.js',
					'app/components/version/interpolate-filter.js',
					'app/components/version/version-directive.js',
					'app/js/services/auth/userAuthService.js',
					'app/js/services/httpInterceptor/httpInterceptor.js',
					'app/js/services/longPoller/longPoller.js',
					'app/js/services/profile/profile.js',
					'app/js/services/reg/userRegService.js',
					'app/js/services/restapi/accounts.js',
					'app/js/services/restapi/fakedata/fakedata_0.js', //��������
					'app/js/services/restapi/rooms.js',
					'app/js/services/restapi/settings.js',
					'app/js/services/restapi/tests.js',
					'app/js/services/room/userRoom.js',
					'app/js/services/errorInterceptor/errorInterceptor.js',
					'app/js/helpers/helpers.js',
					'app/js/services/structures/Structures.js',
					'app/js/services/tests/userTest.js',
					'app/student/number/roomView.js',
					'app/student/reg/regView.js',
					'app/student/test/testView.js',
					'app/student/wait/waitView.js',
					'app/student/auth/authView.js',
					'app/student/layout/headerBar.js',
					'app/student/app.js',
					],
                    dest: '<%= buildPath %>student/script.js'
            },
			studentCss: 
			{
				src:
				[
					'app/css/app.css',
					'app/css/bootstrap.css',
					
				],
				dest: '<%= buildPath %>lib/student_style.css'
			},
			adminCss:
			{
				src:
				[
					'app/css/app.css',
					'app/css/bootstrap.css',
					'app/css/textAngular.css',
				],
				dest: '<%= buildPath %>lib/admin_style.css'
			}
        },*/
		//�������� ����� � ���������� ������� ������ ������� � �������� ������
		copy:
		{
			main:
			{
				files:
				[
					{expand: true, cwd: 'app/admin/', src: ['**/*.html'], dest: '<%= buildPath %>admin/'},
					{expand: true, cwd: 'app/student/', src: ['**/*.html'], dest: '<%= buildPath %>student/'},
					{expand: true, cwd: 'app/fonts/', src: ['**'], dest: '<%= buildPath %>fonts/'},
					{expand: true, cwd: 'app/media/', src: ['**'], dest: '<%= buildPath %>media/'},
					{expand: true, cwd: 'app/media/', src: ['**'], dest: '<%= buildPath %>admin/media/'},
					{expand: true, cwd: 'app/media/', src: ['**'], dest: '<%= buildPath %>student/media/'},
				],
			},
		},
        // �������
        uglify: {
			options: 
			{
				sourceMap: true,
				mangle: true,
			},
            main: {
                files: {
                    // ��������� ������ concat
					'<%= buildPath %>lib/lib.min.js':'<%= bower_concat.all.dest %>',
					'<%= buildPath %>admin/script.min.js':'<%= dev_concat.admin_js.dest %>',
					'<%= buildPath %>student/script.min.js': '<%= dev_concat.student_js.dest %>',
                }
            }
        },
		dev_concat:
		{
			admin_js: 
			{
				indexFile: 'app/admin/index-debug.html',
				type: 'js',
				dest: '<%= buildPath %>admin/script.js',
			},
			admin_css:
			{
				indexFile: 'app/admin/index-debug.html',
				type: 'css',
				dest: '<%= buildPath %>lib/admin_style.css',
			},
			student_js:
			{
				indexFile: 'app/student/index-debug.html',
				type: 'js',
				dest: '<%= buildPath %>student/script.js',
			},
			student_css:
			{
				indexFile: 'app/student/index-debug.html',
				type: 'css',
				dest: '<%= buildPath %>lib/student_style.css'
			}
		}
    };
	grunt.initConfig(initConfig);
    // �������� ��������, ������������� � ������� npm install
    grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-wiredep');

    grunt.registerTask('gen_index_file', '', function(type,path)
		{
			var index = grunt.file.read(path+'/index-debug.html');
			var cssLib = '';
			if(type == 'student')
				cssLib = 'student_style.css';
			else
				cssLib = 'admin_style.css';
			
			index = replace(index, 'bower_css');
			index = replace(index, 'bower_js');
			index = replace(index, 'dev_css');
			index = replace(index, 'dev_js');
			grunt.file.write(path+'/index.html', index);
			grunt.file.delete(path+'/index-debug.html');

			function replace(index, type)
			{
				var beginTemplate = '';
				var endTemplate = '';
				var replacement = '';
				switch(type)
				{
					case 'bower_css':
						beginTemplate = '<!-- bower:css -->';
						endTemplate = '<!-- endbower -->';
						replacement = '<link rel="stylesheet" href="../lib/lib.css" />';
						break;
					case 'bower_js':
						beginTemplate = '<!-- bower:js -->';
						endTemplate = '<!-- endbower -->';
						replacement = '<script src="../lib/lib.min.js" ></script>';
						break;
					case 'dev_js':
						beginTemplate = '<!-- dev:js -->';
						endTemplate = '<!-- enddev -->';
						replacement = '<script src="script.min.js" ></script>';
						break;
					case 'dev_css':
						beginTemplate = '<!-- dev:css -->';
						endTemplate = '<!-- enddev -->';
						replacement = '<link rel="stylesheet" href="../lib/'+cssLib+'" />';
						break;
				}
				var position1 = index.indexOf(beginTemplate);
				var position2 = index.indexOf(endTemplate, position1)+endTemplate.length;
				return index.substring(0,position1)+replacement+'\n'+index.substring(position2, index.length);
			}
			
		});
	grunt.registerTask('clear', '������� ��������� ������ ������������ ��������', function(path)
	{
		grunt.file.delete(path+'admin/script.js');
		if(grunt.file.exists(path+'admin/script.min.js.map'))
			grunt.file.delete(path+'admin/script.min.js.map');
		grunt.file.delete(path+'lib/lib.js');
		if(grunt.file.exists(path+'lib/lib.min.js.map'))
			grunt.file.delete(path+'lib/lib.min.js.map');
		grunt.file.delete(path+'student/script.js');
		if(grunt.file.exists(path+'student/script.min.js.map'))
			grunt.file.delete(path+'student/script.min.js.map');
	});
	
	grunt.registerMultiTask('dev_concat', '����������� ��������', function()
	{
		
		var list = getListOf(this.data.type, this.data.indexFile);
		var self = this;
		grunt.config('concat.dist.src',list);
		grunt.log.writeln(list);
		grunt.config('concat.dist.dest', this.data.dest);
		grunt.task.run('concat');
		
		function getListOf(type, dir)
		{
			var string = grunt.file.read(dir);
			var pattern;
			var filePattern;
			var item;
			var list = [];
			var prefix = dir.match(new RegExp('.*/'));

			
			if(type == 'css')
			{
				filePattern = /href=("[^"]*"|'[^']*')/igm;
				pattern = new RegExp("<!--( )*dev:css");
			}
			else
			if(type == 'js')
			{
				//filePattern = new RegExp('src=[\"]+([\\w/\._]*)"','igm');
				filePattern = /src=("[^"]*"|'[^']*')/igm;
				pattern = new RegExp("<!--( )*dev:js");
			}
			else
				return null;
			var begin = string.search(pattern);
			if(begin == -1)
				return null;
			var end = string.indexOf('enddev',begin);
			if(end == -1)
				return null;
			var result = string.substring(begin, end);
			while((item = filePattern.exec(result)) != null)
				list.push(prefix+item[1].substring(1,item[1].length-1));
			return list;
		}
		
	});
	
    grunt.registerTask('build', ['bower_concat','dev_concat','copy','uglify',
	'gen_index_file:admin:'+initConfig.buildPath+'admin',
	'gen_index_file:student:'+initConfig.buildPath+'student',
	'clear:'+initConfig.buildPath,
	]);
	grunt.registerTask('debug', 'wiredep');
};